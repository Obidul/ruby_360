@user_profile
Feature: Account activation - Sub user

  @subactivation
  Scenario: Varify and signin to the Subuser account
    Then Subuser account activation and login
    And I wait for 2 seconds
    And I would focus on new window
    Then I fill in "username" with "abdulalim"
    Then I fill in "password" with "123456"
    Then I fill in "confirmPassword" with "123456"
    Then I click on the Login button
    And I wait for 40 seconds

  @adminactivation
  Scenario: Varify and signin to the Admin account
    Then Admin account activation and login
    And I wait for 2 seconds
    And I would focus on new window
    Then I fill in "username" with "robinhoodbs23"
    Then I fill in "password" with "123456"
    And I click on "Login" button
    And I wait for 40 seconds

  #To Create Quick Account in 3 Server, we use this part:
#
#  @prod @account
#  Scenario Outline: Create new account in Production
#    And Production: My email: "<email_address>" and username: "<username>" & password: "<password>" and confirmation at : "<mailinator_email_id>"
#    And I would focus on new window
#    And Logged me to system with new username: "<username_to_login>" & password: "<password_to_login>"
#    And I would focus on new window
#    And I click on the avater button and select "<select_option>"
#    And I fillup the account informations
#    And I click on the Save button
#  Examples:
#    | email_address                  | username        | password          | mailinator_email_id | username_to_login | password_to_login | select_option |
#    | automatedemailtest@mailinator.com | automatedemailtest | test-automation@# | automatedemailtest     | automatedemailtest   | test-automation@#            | Profile       |
#
#  @qa @account
#  Scenario Outline: Create new account in QA
#    And QA: My email: "<email_address>" and username: "<username>" & password: "<password>" and confirmation at : "<mailinator_email_id>"
#    And I would focus on new window
#    And Logged me to system with new username: "<username_to_login>" & password: "<password_to_login>"
#    And I would focus on new window
#    And I click on the avater button and select "<select_option>"
#    And I fillup the account informations
#    And I click on the Save button
##    And I have Purchased Subscription Plan
#  Examples:
#    | email_address                  | username        | password          | mailinator_email_id | username_to_login | password_to_login | select_option |
#    | automatedemailtest@mailinator.com | automatedemailtest | test-automation@# | automatedemailtest     | automatedemailtest   | test-automation@#            | Profile       |
#
#  @pre @account
#  Scenario Outline: Create new account in Preprod
#    And Preprod: My email: "<email_address>" and username: "<username>" & password: "<password>" and confirmation at : "<mailinator_email_id>"
#    And I would focus on new window
#    And Logged me to system with new username: "<username_to_login>" & password: "<password_to_login>"
#    And I would focus on new window
#    And I click on the avater button and select "<select_option>"
#    And I fillup the account informations
#    And I click on the Save button
##    And I have Purchased Subscription Plan
#  Examples:
#    | email_address                  | username        | password          | mailinator_email_id | username_to_login | password_to_login | select_option |
#    | automatedemailtest@mailinator.com | automatedemailtest | test-automation@# | automatedemailtest     | automatedemailtest   | test-automation@#            | Profile       |
#
#
#  @sch
#  Scenario Outline: Facebook Profile (FP) = Text
#    Given Go for Social Post
#    And Select Facebook Profile and Fill "<user_input>"
#    And Scheduling Social Post with Default Time & Date
#    And I click Save Post button
#    And I wait for <secs> seconds
#  Examples:
#    | user_input           | secs |
#    | World wide web - FIRSTBLOCK1 | 2    |
#    | World wide web - FIRSTBLOCK2 | 2    |
#    | World wide web - FIRSTBLOCK3 | 2    |
#    | World wide web - FIRSTBLOCK4 | 2    |
#    | World wide web - FIRSTBLOCK5 | 2    |
#    | World wide web - FIRSTBLOCK6 | 2    |
#    | World wide web - FIRSTBLOCK7 | 2    |
#    | World wide web - FIRSTBLOCK8 | 2    |
#    | World wide web - FIRSTBLOCK9 | 2    |
#    | World wide web - FIRSTBLOCK10 | 2    |
#    | World wide web - FIRSTBLOCK11 | 2    |
#    | World wide web - FIRSTBLOCK12 | 2    |


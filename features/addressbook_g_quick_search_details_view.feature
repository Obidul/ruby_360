@addressbook
Feature: Go for Quick search Test Execution - Details View
  Quick Search with -- click functions
  Quick Search with -- enter functions
  Quick Search without -- company name and contact field and others field empty
  Quick Search with - 15 Field Data Verification
  Quick search results to select checked

#Note: (LV) = List View & (DV) = Details View

  Background: Prerequisites
   Given I want to maximize the windows of browser
   Given As Admin User, Addressbook - Quick Search Activity access to conduct the test execution
   Given Wait until the Application is ready

  @test1
  Scenario: (DV) Quick Search with -- click functions
    And I quick search with "An" -- with click function
    And I select "Anne Andrews, Eabox" from the list of autocomplete -- with click selection
    And I wait for 3 seconds

  @test2
  Scenario: (DV) Quick Search with -- enter functions
    And I quick search with "An" -- with enter function
    And I wait for 3 seconds
    And I should see in Search Results : "4"

  @test3
  Scenario: (DV) Quick Search with -- enter functions (No Company Name & Contact Name & Empty Others Field)
    And I quick search with "sakibqa+38@gmail.com" -- with enter function and no company and contact info not found
    And I should see in Search Results : "1"
    And I wait for 3 seconds

  @test4
  Scenario Outline: (DV) Quick Search with - 15 fields verification
    And I quick search with "<quick_search_field>" -- with enter function
    And I should see in Search Results : "<number>"
    And I wait for <secs> seconds
  Examples:
    | quick_search_field  | number | secs |
    | Eabox               | 2      | 3    |
    | An                  | 4      | 3    |
    | Sr                  | 10     | 3    |
    | Mrs                 | 7      | 3    |
    | sakibqa+22@gmail.com| 1      | 3    |
    | (404) 825-3925      | 1      | 3    |
#    | 49321               | 1      | 3    |
    | (937) 942-8625      | 1      | 3    |
    | (309) 750-0031      | 1      | 3    |
    | Attempted           | 2      | 3    |
    | North Carbon Road   | 1      | 3    |
    | Corben              | 1      | 3    |
    | New York            | 4      | 3    |
    | Michigan            | 1      | 3    |
    | 20503-79950         | 1      | 3    |

  @test5
  Scenario: (DV) Quick Search with -- Partially adding/deleting contact name and notice change in quick search result
    And I quick search with "Wayne" -- In Drop-down Updates Information
    And In quick search I should see "Wayne Spencer, Centizu"
    And I quick search with "Wa" -- In Drop-down Updates Information
    And I wait for 3 seconds
    And In quick search I should see "Doris Watson, Devpulse"
    And In quick search I should see "Wayne Spencer, Centizu"
    And In quick search I should see "Willie Williams, Eabox"
    And In quick search I should see "Dennis Ellis, Eazzy"





@action_items @afterhandle
Feature: Dashboard - Activity Today

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Dashboard access to conduct the test execution
    Given Wait until the Application is ready

  ##STORY BLOCK: Primary Action List
  ##STORY BLOCK: => ACTIONS ITEMS <=

  @t1 @er
  Scenario: As Admin User, Primary Action List -> Today (Default Selected) - Activity Type: Call
    Given I wait for 3 seconds
    And I create an activity with "Demo Activity Task -- Activity Today selected"
    And I select activity type "Call"
    And I searched for "Da" & select "Daniel Baker, Aba" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Activity Today selected" activity has enlisted

  @t2 @er
  Scenario: As Admin User, Primary Action List -> Starred (Task selected) - Activity Type: Meeting
    And I wait for 3 seconds
    Given I select Starred from Primary Right Panel
    And I create an activity with "Demo Activity Task -- Starred selected"
    And I select activity type "Meeting"
    And I searched for "Ga" & select "Gary Cook, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Starred selected" activity has enlisted

  @t3 @er
  Scenario: As Admin User, Primary Action List -> Overdue (Overdue selected) - Today - Activity Type: Meeting
    And I wait for 3 seconds
    Given I select Overdue from Primary Right Panel
    And I create an activity with "Demo Activity Task -- Overdue selected in Today"
    And I select activity type "Meeting"
    And I searched for "Er" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And Scroll to find contents
    And I should see "Demo Activity Task -- Overdue selected in Today" activity has enlisted

  @t4 @er
  Scenario: As Admin User, Primary Action List -> Overdue (Overdue selected) - Previous Date - Activity Type: Meeting
    And I wait for 3 seconds
    Given I select Overdue from Primary Right Panel
    And I create an activity with "Demo Activity Task -- Overdue selected in Previous Date"
    And I select activity type "Meeting"
    And I select Previous Date - Yesterday
    And I searched for "Ant" & select "Anthony Harvey, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Overdue selected in Previous Date" activity has enlisted

  @tasking
  Scenario: As Admin User, Primary Action List -> Today (Default Selected) - Activity Type: Task, Contact: Company.
    And I wait for 3 seconds
    And I create an activity with "Demo Activity Task for Company -- Activity Today selected"
    And I select activity type "Task"
    And I searched for "Phy" & select "Phyllis Reyes, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task for Company -- Activity Today selected" activity has enlisted

  @todo @er
  Scenario: As Admin User, Primary Action List -> Today (Default Selected) - Activity Type: Todo
    And I wait for 3 seconds
    And I create activity of Todo with "Add Todo - Todo Added Example"
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Add Todo - Todo Added Example" Todo has enlisted

  @task @er
  Scenario: As Admin User, Primary Action List -> Today (Default Selected) - Activity Type: Task
    And I wait for 3 seconds
    And I select Today from Primary Right Panel
    And I create an activity with "Add Task - Task Added Example"
    And I searched for "Mat" & select "Matthew Williamson, Buzzdog" from the list
    And I select activity type "Task"
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Add Task - Task Added Example" activity has enlisted

  @switch @er
  Scenario: As Admin User, Primary Action List -> Create Activity (Meeting selected) = Switch to Todo (CA)
#    And I wait for 3 seconds
    And I wait for 5 seconds
    Given I select Today from Primary Right Panel
    And I create an activity with "Demo Activity Task -- Switched to Todo"
    And I select activity type "Meeting"
    And I searched for "Gar" & select "Gary Cook, Abata" from the list
    And I wait for 5 seconds
    And I select activity type "To Do"
    And I wait for 2 seconds
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Switched to Todo" Todo has enlisted

  ##STORY BLOCK: Secondary Action List
  ##STORY BLOCK: => ACTIVITY TAB <=

  @sakib @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> All Activity -> Create
    And I wait for 3 seconds
    Given I select All Activity Under Activity Tab
    And I create an activity with "Demo Activity Task -- All Activity selected"
    And I searched for "Alb" & select "Albert Wilson, Chatterbridge" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- All Activity selected" activity has enlisted

  @sakib @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Create -> Meeting
    And I wait for 3 seconds
    Given I select Meeting Under Activity Tab
    And I create an activity with "Demo Activity Task -- Meeting selected"
    And I searched for "Nic" & select "Nicole Morrison, Cogibox" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Meeting selected" activity has enlisted

  @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Create -> Calls
    And I wait for 3 seconds
    Given I select Calls Under Activity Tab
    And I create an activity with "Demo Activity Task -- Calls selected"
    And I searched for "Ste" & select "Steve Smith, Dynabox" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Calls selected" activity has enlisted

  @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Create -> Tasks
    And I wait for 3 seconds
    Given I select Tasks Under Activity Tab
    And I create an activity with "Demo Activity Task -- Tasks selected"
    And I searched for "Ern" & select "Ernest Hall, Flipstorm" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Demo Activity Task -- Tasks selected" activity has enlisted

  @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Create -> Todo
    And I wait for 3 seconds
    Given I select Todo Under Activity Tab
    And I create activity of Todo with "Demo Todo Task from Activity Tab"
    And I click to Save button
    And I wait for 5 seconds
  #   And Scroll to find contents
    And I should see "Demo Todo Task from Activity Tab" Todo has enlisted

  #We can add more scenario in low level

  @tags @refactor @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Tags -> Create
    And I wait for 3 seconds
    Given I select Tags Under Tags Tab
    And I create an activity with "Demo Activity Task -- Tags selected"
    And I searched for "Ja" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And I select Today from Primary Right Panel
    And I should see "Demo Activity Task -- Tags selected" activity has enlisted

  #Prerequisties -- Filter should be available.

  @filter @tf @er
  Scenario: As Admin User, Secondary Action List -> Activity Tab -> Filter -> Create
    And I wait for 3 seconds
    Given I select Filter Under Filter Tab
    And I create an activity with "Demo Activity Task -- Filter selected"
    And I searched for "De" & select "Debra Shaw, Izio" from the list
    And I click to Save button
    And I wait for 3 seconds
    And I select Today from Primary Right Panel
    And I should see "Demo Activity Task -- Filter selected" activity has enlisted

  #STORY BLOCK: InLine ACTIVITY + COMMENTS => ADDING/ EDITING/ UPDATING - MOUSE HOVER & DROP-DOWN MENU TO

  #PAIR START

  @pair-01 @paired @er
  Scenario: As Admin User, Activity Today - Default Selected
    And I wait for 3 seconds
    And I create an activity with "New - Inline - Entry"
    And I searched for "Chr" & select "Chris Harvey, Jamia" from the list
    And I click to Save button
    And I wait for 3 seconds
    And I should see "New - Inline - Entry" activity has enlisted

  @pair-02 @paired
  Scenario: As Admin User, Activity Today - Mouse Click -- Inline Edit
    And I wait for 3 seconds
    And I want to edit "New - Inline - Entry"
    And I want to edit activities with "Edited Through Click -- Inline - Entry"
    And I searched for "La" & select "Larry Cooper, Kaymbo" from the list
    And I click to Save button
    And I wait for 3 seconds
    And I should see "Edited Through Click -- Inline - Entry" activity has enlisted

  @pair-03 @paired @drop-down-menu
  Scenario: As Admin User, Activity Today -> Drop-down Menu to - Edit Activity
    And I wait for 3 seconds
    And I edit activity of "Edited Through Click -- Inline - Entry" & select "Edit Activity" from drop-down menu and fill in "Edited -- Drop-down to Click -- Inline - Entry"
    And I searched for "And" & select "Andrea Crawford, Kazio" from the list
    And I click to Save button
    And I wait for 3 seconds
    And I should see "Edited -- Drop-down to Click -- Inline - Entry" activity has enlisted

  @pair-04 @paired
  Scenario: As Admin User, Activity Today -> Existing Comments - Inline Comments (Icons to click)
    And I wait for 3 seconds
    And I comments on activity of "Edited -- Drop-down to Click -- Inline - Entry" & clicked on comment icon and fill in "Commented -- Mouse Hover -- Inline - Entry"
    And I click to Save button
    And I wait for 3 seconds
    And I should see "Commented -- Mouse Hover -- Inline - Entry" comment has enlisted

  @pair-05 @paired @drop-down-menu
  Scenario: As Admin User, Activity Today -> Drop-down menu to - Add Comments
    And I wait for 3 seconds
    And I comments on activity of "Edited -- Drop-down to Click -- Inline - Entry" and I have select "Add Comment" from drop-down menu and fill in "Commented -- Drop-down menu -- Inline - Entry"
    And I click to Save button
    And I wait for 3 seconds
    And I should see "Commented -- Drop-down menu -- Inline - Entry" comment has enlisted

  @edit
  Scenario: As Admin User, Activity Today -> Existing Task - Inline Edit
    And I wait for 3 seconds
    Given I select Todo Under Activity Tab
    And I create activity of Todo with "Todo - Inline Edit"
    And I click to Save button
    And I wait for 5 seconds
    And I found Todo: "Todo - Inline Edit" on Grid list and Updated to "Todo - Inline Edited & Updated"
    And I click to Save button
    And I wait for 5 seconds

  @edit
  Scenario: As Admin User, Activity Today -> Drop-down menu to -> Edit Todo
    And I wait for 3 seconds
    Given I select Todo Under Activity Tab
    And I create activity of Todo with "Todo - Drop-down menu to Edit Todo"
    And I click to Save button
    And I wait for 5 seconds
    And I found Todo: "Todo - Drop-down menu to Edit Todo" on Grid list and I have selected "Edit Todo" from drop-down menu and fill in "Todo - Drop-down menu to Edit Todo - Updated"
    And I click to Save button
    And I wait for 5 seconds

  @edit @delete
  Scenario: As Admin User, Activity Today -> Drop-down menu to -> Delete
    And I wait for 3 seconds
    Given I select Todo Under Activity Tab
    And I create activity of Todo with "Todo - Create and then Delete"
    And I click to Save button
    And I wait for 5 seconds
    And I found Todo: "Todo - Create and then Delete" on Grid list and I have selected "Delete" from drop-down menu
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #PAIR END
  #STORY BLOCK: Starred/UnStarred

  @starred @star
  Scenario: As Admin User, Starred An Activity from Grid List
    And I wait for 3 seconds
    And I create an activity with "Star Mark Task in Grid List"
    And I searched for "Jud" & select "Judith Hanson, Kazio" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Star Mark Task in Grid List" and select Star button
    And I wait for 5 seconds

  @un-starred @star
  Scenario: As Admin User, Un-Starred An Activity from Grid List
    And I wait for 3 seconds
    And I create an activity with "Star Mark Task in Grid List"
    And I searched for "Mil" & select "Mildred Perez, Kwideo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Star Mark Task in Grid List" and unselect Star button
    And I wait for 5 seconds

  @back @star
  Scenario: As Admin User, Starred -> Verify -> Details View Contact -- Star Button Active
    And I wait for 3 seconds
    And I create an activity with "Details View Contact - Star"
    And I searched for "Nicho" & select "Nicholas Garza, Riffpath" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Details View Contact - Star" and select Star button
    And I quick search with "Nicholas Garza" -- with enter function
    And I wait for 5 seconds
    And I should see Star button is active
    And I wait for 5 seconds

  #STORY BLOCK: DROP-DOWN MENU ACTIVITIES => Edit Activity, Add Comments, View Contact, Email Contact, Delete

  #Verification Included within steps definitions

  @view_contact @drop-down-menu @ver
  Scenario: As Admin User, Activity Grid List  -> Drop-down menu to - View Contacts - Details View (Load)
    And I wait for 3 seconds
    And I create an activity with "Details View Access - Drop-down Menu - From Grid List"
    And I searched for "Pam" & select "Pamela Phillips, Leexo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Details View Access - Drop-down Menu - From Grid List" and select View Contact from Drop-down menu
    And I wait for 5 seconds

  @email_contact @wip @drop-down-menu @refactor @ver
  Scenario: As Admin User, Activity Grid List -> Drop-down menu to - Email Contact - Email Module (Load)
    And I wait for 3 seconds
    And I want to Email Contact from Grid list
    And Allow to close browser or tab anytime
    And I wait for 3 seconds

  @delete_activity @drop-down-menu @ver
  Scenario: As Admin User, Activity Grid List -> Drop-down menu to - Delete
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I create an activity with "Delete Activity - From Grid List"
    And I searched for "Fr" & select "Fred Coleman, Motherboard" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Delete Activity - From Grid List" and select Delete from Drop-down menu
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: CHECKBOX & RADIO BUTTON => COMPLETE TASK/ VERIFICATION IN HISTORY
  #CREATE & VERIFY & COMPLETED -- START
  #Note: @verification tags is required verify activity on history.

  @mark_completed @verification
  Scenario: As Admin User, Activity Grid List - Checked - As Completed
    And I wait for 3 seconds
    And I create an activity with "Mark Activity as Completed"
    And I searched for "Jen" & select "Jennifer Hunter, Gigazoom" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Mark Activity as Completed" and select Radio button
    And I wait for 5 seconds

  @mark_completed @verification
  Scenario: As Admin User, Activity Grid List - History -- Verification Presence
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I switch to History to see Completed Items
    And I should see "Mark Activity as Completed" activity has enlisted
    And I wait for 3 seconds

  @mark_completed @verification
  Scenario: As Admin User, Activity Grid List - From History - Un-Checked - As Not Completed
    And I wait for 3 seconds
    And I switch to History to see Completed Items
    And I should see "Mark Activity as Completed" activity has enlisted
    And I found activity of "Mark Activity as Completed" and select Radio button
    And I select All Activity Under Activity Tab
    And I wait for 3 seconds
    And I should see "Mark Activity as Completed" activity has enlisted
    And I wait for 3 seconds

  #CREATE & VERIFY & COMPLETED -- END

  #STORY BLOCK: REMINDER CLOCK ACTIVITY

  @reminder @clock
  Scenario: As Admin User, Add Reminder Clock for Reminder
    And I wait for 3 seconds
    And I create an activity with "Set & Get Clock Reminder"
    And I searched for "Cat" & select "Catherine Gilbert, Motherboard" from the list
    And I select Reminder Clock
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Set & Get Clock Reminder" activity has enlisted

  #STORY BLOCK: ID/STATUS => SELECT AND UNSELECT @REFACTOR

  @id_status
  Scenario: As Admin User, Select ID/Status and Give a New Status
    And I wait for 3 seconds
    And I create an activity with "Change Current ID and Status of Activity"
    And I searched for "Ke" & select "Kenneth Tucker, Motherboard" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found activity of "Change Current ID and Status of Activity" and change ID&Status to "Customer"
    And I wait for 5 seconds

  @id_status
  Scenario: As Admin User, Change Current ID&Status and Give a New Status
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I found activity of "Change Current ID and Status of Activity" and change ID&Status to "Contacted"
    And I wait for 5 seconds

  #STORY BLOCK: => Contact Action List <=

  #STORY BLOCK: STATUS

  #Note: Sorting is important to pass scenario in status case. A_Z

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Disqualified
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Disqualified"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Disqualified from Status - while in Action Stream module
    And I create an activity with "Status - Disqualified"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Disqualified Status Counter matched
  
  @status
  Scenario: As Admin User, Contact Action List -> Status -> New
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "New"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select New from Status - while in Action Stream module
    And I create an activity with "Status - New"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected New Status Counter matched

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Attempted
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Attempted"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Attempted from Status - while in Action Stream module
    And I create an activity with "Status - Attempted"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Attempted Status Counter matched

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Contacted
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Contacted"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Contacted from Status - while in Action Stream module
    And I create an activity with "Status - Contacted"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Contacted Status Counter matched

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Hot Prospect
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Hot Prospect"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Client from Status - while in Action Stream module
    And I create an activity with "Status - Hot Prospect"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Hot Prospect Status Counter matched

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Client
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Client"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Client from Status - while in Action Stream module
    And I create an activity with "Status - Client"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Client Status Counter matched

  @status
  Scenario: As Admin User, Contact Action List -> Status -> Disqualified
    And I wait for 3 seconds
    Given Contact of "James Bennett" Status has updated to "Disqualified"
    And I click on Action Items Tab
    And I wait for 5 seconds
    Given I select Disqualified from Status - while in Action Stream module
    And I create an activity with "Status - Disqualified"
    And I searched for "James" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 3 seconds
    And Number of Status Activity and selected Disqualified Status Counter matched

  #WIP: TEAM
#
#  @team
#  Scenario: As Admin User, Contact Action List -> Team -> Account 1
#    And I wait for 3 seconds
#    Given I select Team
#    Given I select "Acc. Manager " Under Team Menu
#    And I create an activity with "Hik Demo Team Task -- Team selected"
#    And I searched for "Ho" & select "Howard Rivera, Photofeed" from the list
#    And I click to Save button
#    And I wait for 3 seconds


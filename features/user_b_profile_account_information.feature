@user_profile
Feature: Account information

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, User Profile access to conduct the test execution
    Given Wait until the Application is ready

  Scenario:As admin user, click on on the avater and navigate to Profile
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 5 seconds

  Scenario:As admin user, click on on the avater and navigate to My Cart
    And I wait for 5 seconds
    Then I click on the avater button and select "My Cart"
    And I wait for 5 seconds

  Scenario:As admin user, click on on the avater and navigate to Purchase History
    And I wait for 5 seconds
    Then I click on the avater button and select "Purchase History"
    And I wait for 5 seconds

  Scenario:As admin user, click on on the avater and navigate to Help
    And I wait for 5 seconds
    Then I click on the avater button and select "Help"
    And I wait for 5 seconds

  Scenario:As admin user, click on on the avater and navigate to Log out
    And I wait for 5 seconds
    Then I click on the avater button and select "Log Out"
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate Profile from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "My Cart"
    And I wait for 2 seconds
    Then I click and select "Profile" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate My Cart from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "My Cart" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate Subscription Plan from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Subscription Plan" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate Purchase History from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Purchase History" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate Manage Users from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, select and navigate Update Password from the profile right panel
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Update Password" from the profile right panel
    And I wait for 5 seconds

  Scenario:As admin user, Fillup and Cancel Account Profile informations
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 5 seconds
    Then I fillup the account informations
    And I wait for 3 seconds
    Then I click on the Cancel button
    And I wait for 3 seconds

  Scenario:As admin user, Fillup and Save Account Profile informations
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 5 seconds
    Then I fillup the account informations
    And I wait for 3 seconds
    Then I click on the Save button
    And I wait for 3 seconds

  Scenario:As admin user, Select Monthly Invoices from the Purchase History page
    And I wait for 5 seconds
    Then I click on the avater button and select "Purchase History"
    And I wait for 5 seconds
    Then I checked the Monthly Invoices
    And I wait for 5 seconds

  Scenario:As admin user, Goto Manage Users, fillup the sub user details and click on the cancel button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the Add New User button
    And I wait for 2 seconds
    Then I fillup the new user account detials
    And I wait for 2 seconds
    Then I click on the Cancel button
    And I wait for 2 seconds

  @subinvitation
  Scenario:As admin user, Goto Manage Users and Add a subuser
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the Add New User button
    And I wait for 2 seconds
    Then I fillup the new user account detials
    And I wait for 2 seconds
    Then I click on the Send Invitaion button
    And I wait for 5 seconds

  Scenario:As admin user, Goto Manage Users and click on the Edit
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the edit of the subuser
    And I wait for 2 seconds

  Scenario:As admin user, Select Block user option and click on Cancel button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the edit of the subuser
    And I wait for 2 seconds
    Then I select Block access
    And I wait for 5 seconds
    Then I click on the Cancel button
    And I wait for 2 seconds

  Scenario:As admin user, Select Block user option and click on Save button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the edit of the subuser
    And I wait for 2 seconds
    Then I select Block access
    And I wait for 5 seconds
    Then I click on the Save button
    And I wait for 2 seconds

  Scenario:As admin user, Select Assign New User to Account option and click on Cancel button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the edit of the subuser
    And I wait for 2 seconds
    Then I select Assign New User to Account
    And I wait for 2 seconds
    Then I fillup the Assign New User detials
    And I wait for 2 seconds
    Then I click on the Cancel button
    And I wait for 2 seconds

  Scenario:As admin user, Select Assign New User to Account option and click on Save button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the edit of the subuser
    And I wait for 2 seconds
    Then I select Assign New User to Account
    Then I fillup the Assign New User detials
    Then I click on the Save button
    And I wait for 2 seconds

  Scenario:As admin user, Goto Manage Users and click on the Delete
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Manage Users" from the profile right panel
    And I wait for 2 seconds
    Then I click on the delete of the subuser
    And I wait for 2 seconds
    Then I accept to proceed remaining scenario
    And I wait for 2 seconds

  Scenario:As admin user, Goto Update Password page, fillup the current & new passowrd and click on the cancel button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Update Password" from the profile right panel
    And I wait for 5 seconds
    Then I fillup the current & new passowrd
    And I wait for 2 seconds
    Then I click on the Cancel button
    And I wait for 5 seconds

  Scenario:As admin user, Goto Update Password page, fillup the current & new passowrd and Save the new passord
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Update Password" from the profile right panel
    And I wait for 5 seconds
    Then I fillup the current & new passowrd
    And I wait for 2 seconds
    Then I click on the Reset Password button
    And I wait for 5 seconds

  Scenario:As admin user, Goto Subscription Plan page, fillup the username & passowrd and click on the Return: Do Not Cancel button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Subscription Plan" from the profile right panel
    And I wait for 5 seconds
    Then I click on the Cancel Account button on the subscription Plan page
    And I wait for 2 seconds
    Then I fillup the username and password
    And I wait for 2 seconds
    Then I click on the Return: Do Not Cancel button
    And I wait for 2 seconds

   #NOTE: This scenario will delete the admin user account. Run this scenario with proper observation.
  Scenario:As admin user, Goto Subscription Plan page, fillup the username & passowrd and click on the Cancel Account button
    And I wait for 5 seconds
    Then I click on the avater button and select "Profile"
    And I wait for 2 seconds
    Then I click and select "Subscription Plan" from the profile right panel
    And I wait for 5 seconds
    Then I click on the Cancel Account button on the subscription Plan page
    And I wait for 2 seconds
    Then I fillup the username and password
    And I wait for 2 seconds
#    Then I click on the Cancel Account button
#    And I wait for 2 seconds
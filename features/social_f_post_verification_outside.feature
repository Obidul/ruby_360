Feature: Social Media Posted Status Verification
  Twitter Post Verification
  Facebook Post Verification
  Linkedin Post Verification

  Scenario Outline: Twitter Post Verification
    And Access Twitter Account - Post Verification
    And I wait for <secs> seconds
    And I should see "<user_public_data>" on Twitter stream
  Examples:
    | secs | user_public_data          |
    | 5    | bright sky and new daydd3 |
    | 5    | tinyurl.com/zuwaeto       |

  Scenario: Linkedin Post Verification
    And Access Linkedin Profile - Post Verification
    And I wait for 5 seconds
    And I should see "Scheduled - Single Email - Auto Post - Linkedin" on Linkedin Profile stream

  Scenario: Facebook Profile Post Verification
    And Access Facebook Profile - Post Verification
    And I wait for 5 seconds
    And I should see "hello world" on Facebook Profile stream

  Scenario: Facebook Fan Page Post Verification
    And Access Facebook Profile - Post Verification
    And I wait for 5 seconds
    And I should see "New test 3.30PM" on Facebook Fan Page stream


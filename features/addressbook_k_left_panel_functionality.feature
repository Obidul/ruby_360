@addressbook
Feature: Addressbook - Left Panel Functionality

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook Left Panel Functionality access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #STORY BLOCK: Addressbook Left Panel Test Coverage

  #STORY BLOCK: ACTIVITY TAB

  Scenario: As Admin, Create Activity -> Meeting
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "Addressbook Demo Test Data - Meeting" in Details View
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As Admin, Create Activity -> Call
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I move to next contact address
    And I create an activity with "Addressbook Demo Test Data - Call" in Details View
    And I select activity type "Call"
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As Admin, Create Activity -> Task
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I move to next contact address
    And I create an activity with "Addressbook Demo Test Data - Task" in Details View
    And I select activity type "Task"
    And I click to Save button
    And I wait for 5 seconds

  @reminder_addressbook
  Scenario: As Admin User, In Activity Tab -> Add Activity -> Set Reminder
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "Set & Get Clock Reminder" in Details View
    And I select activity type "Meeting"
    And I select Reminder Clock
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Set & Get Clock Reminder" activity has enlisted

  Scenario: As Admin User, Addressbook -> In any Contact -> Dropdown menu -> Create Activity
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "In Addressbook - Demo Data" in Details View
    And I click to Save button
    And I wait for 3 seconds
    And I should see "In Addressbook - Demo Data" activity has enlisted

  @dropdown
  Scenario: As Admin User, Addressbook -> In Activity Tab -> Dropdown menu -> Edit Activity
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "In Addressbook - Demo Data - Edit Activity" in Details View
    And I click to Save button
    And I wait for 5 seconds
    And I edit activity of "In Addressbook - Demo Data - Edit Activity" & select "Edit Activity" from drop-down menu and fill in "In Addressbook - Demo Data - Edit Activity - Updated"
    And I click to Save button
    And I wait for 3 seconds
    And I should see "In Addressbook - Demo Data - Edit Activity - Updated" activity has enlisted

  @dropdown
  Scenario: As Admin User, Addressbook -> In Activity Tab -> Dropdown menu -> Add Comment
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "In Addressbook - Demo Data - Add Comment" in Details View
    And I click to Save button
    And I wait for 3 seconds
    And I comments on activity of "In Addressbook - Demo Data - Add Comment" and I have select "Add Comment" from drop-down menu and fill in "In Addressbook - Demo Data - Add Comment Updated"
    And I click to Save button
    And I wait for 3 seconds
    And I should see "In Addressbook - Demo Data - Add Comment Updated" comment has enlisted

  @dropdown
  Scenario: As Admin User, Addressbook -> In Activity Tab -> Dropdown menu -> Email Contact
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "In Addressbook - Demo Data - Email Contact" in Details View
    And I click to Save button
    And I wait for 3 seconds
    And I found activity of "In Addressbook - Demo Data - Email Contact" and I have select "Email Contact" from drop-down menu -- In Addressbook
    And I wait until "#to" is visible
    And I wait for 5 seconds

  @dropdown
  Scenario: As Admin User, Addressbook -> In Activity Tab -> Dropdown menu -> Delete
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create an activity with "In Addressbook - Demo Data - Delete" in Details View
    And I click to Save button
    And I wait for 3 seconds
    And I found activity of "In Addressbook - Demo Data - Delete" and select Delete from Drop-down menu
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: EMAIL TAB

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Compose
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I want to Compose an Email
    And I wait for 5 seconds
    And I wait until "#to" is visible
    And I wait for 5 seconds

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Compose and Send Mail
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I want to Compose an Email
    And I wait for 5 seconds
    And I wait until "#to" is visible
    And In Subject field: I fill in "Demo Email from Addressbook"
    And I click on Send button
    And I should see Email has sent confirmation

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Drop-down Menu -> View Sent Report
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I am on Email Tab
    And I found sent Email Subject of "Demo Email from Addressbook" and I select "View Sent Report"
    And I wait for 5 seconds
    And I wait until "#metrix_email" is visible
    And I wait for 5 seconds

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Drop-down Menu -> View Sent Email
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I am on Email Tab
    And I found sent Email Subject of "Demo Email from Addressbook" and I select "View Sent Email"
    And I wait for 5 seconds
    And I wait until "#sentMailTopHeader" is visible
    And I wait for 5 seconds

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Drop-down Menu -> Delete
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I am on Email Tab
    And I found sent Email Subject of "Demo Email from Addressbook" and I select "Delete"
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  @email
  Scenario: As Admin, Addressbook -> In any Contacts -> Email Tab -> Direct Delete through Icon
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I am on Email Tab
    And I found sent Email Subject of "Demo Email from Addressbook" and I select Delete Icon
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: DEALS TAB

  Scenario: As Admin, Create Deals -> Default
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I Create Deals in Details View with Product "Orange" & Price "$345.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I click to Save button
    And I wait for 3 seconds
    And I found Price: "$345.00" and Product & Company: "Orange - Abata" shows in Details view

  @reminder_deals
  Scenario: As Admin User, In Deals Tab -> Add Deals -> Set Reminder
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$366.00" & Lead Source "Mailing" & Sales Stage "Negotiations"
    And I select Reminder Clock
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$366.00" and Product & Company: "Orange - Abata" shows in Details view
    And I wait for 5 seconds

  Scenario: As Admin, Create Deals -> Default -> Next Contacts
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I move to next contact address
    And I Create Deals in Details View with Product "Orange" & Price "$222.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I click to Save button
    And I wait for 3 seconds

  @dropdown_deals
  Scenario: As Admin User, Addressbook -> In Deals Tab -> Dropdown menu -> Edit Deal
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$367.00" & Lead Source "Tradeshow" & Sales Stage "Negotiations"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price & Product & Company of "$367.00 Orange - Abata" and I select "Edit Deal" from drop-down menu -- In Addressbook
    And I wait for 5 seconds

  @dropdown_deals
  Scenario: As Admin User, Addressbook -> In Deals Tab -> Dropdown menu -> Add Comment
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$332.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price & Product & Company of "$332.00 Orange - Abata" and I select "Add Comment" from drop-down menu -- In Addressbook
    And I comments on deals and fill in "Add Comments in Deals - From Addressbook Dropdown" -- From Dropdown menu
    And I wait for 5 seconds
    And I should see "Add Comments in Deals - From Addressbook Dropdown" comment has enlisted

  @dropdown_deals
  Scenario: As Admin User, Addressbook -> In Deals Tab -> Dropdown menu -> Email Contact
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$398.00" & Lead Source "Email Marketing" & Sales Stage "Needs Analysis"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price & Product & Company of "$398.00 Orange - Abata" and I select "Email Contact" from drop-down menu -- In Addressbook
    And I wait for 5 seconds
    And I wait until "#to" is visible
    And I wait for 5 seconds

  @dropdown_deals @delete
  Scenario: As Admin User, Addressbook -> In Deals Tab -> Dropdown menu -> Delete
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$301.00" & Lead Source "Employee Referral" & Sales Stage "Order/Contract"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price & Product & Company of "$301.00 Orange - Abata" and I select "Delete" from drop-down menu -- In Addressbook
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @dropdown_deals @inline
  Scenario: As Admin User, Addressbook -> In Deals Tab -> Dropdown menu -> Inline Add Comment
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I Create Deals in Details View with Product "Orange" & Price "$500.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price & Product & Company of "$500.00 Orange - Abata" -- In Addressbook
    And I want to comments on deals activities of "Add Comments in Deals - From Addressbook Inline"
    Then I click to Save button
    And I wait for 5 seconds
    And I should see "Add Comments in Deals - From Addressbook Inline" comment has enlisted

  #STORY BLOCK: NOTES

  Scenario: As Admin, Addressbook -> Create Notes
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create a Note with "Demo Notes"
    And I found Notes: "Demo Notes" on Details View Grid list

  Scenario: As Admin, Addressbook -> Create Notes -> In any Contact -> Dropdown menu -> Edit Note
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create a Note with "Demo Notes - Edit Note"
    And I found Notes of "Demo Notes - Edit Note" and I have select "Edit Note" from drop-down menu and fill in "Demo Notes - Edit Note - Updated"
    And I click to Save button
    And I wait for 3 seconds
    And I found Notes: "Demo Notes - Edit Note - Updated" on Details View Grid list

  Scenario: As Admin, Addressbook -> Create Notes -> In any Contact -> Dropdown menu -> Delete
    Given I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I create a Note with "Demo Notes - Delete"
    And I found Notes of "Demo Notes - Delete" and I have select "Delete" from drop-down menu
    And I accept to proceed remaining scenario
    And I wait for 3 seconds


# Deal won / lost added on 20-07-2016

  Scenario: As Admin User, Addressbook -> In Deals Tab -> Create a Deal as Won
    Given I wait for 5 seconds
    Then I click on Contacts Tab
    Then I Create Deals in Details View with Product "Orange" & Price "$1000.00" & Lead Source "Email Marketing" & Sales Stage "Won"
    And I click to Save button
    And I wait for 2 seconds
    Then I found existing Deals Price & Product & Company of "$1,000.00 Orange - Abata" -- In Addressbook

  Scenario: As Admin User, Addressbook -> In Deals Tab -> Create a Deal as Lost
    Given I wait for 5 seconds
    Then I click on Contacts Tab
    Then I Create Deals in Details View with Product "Apple" & Price "$350.00" & Lead Source "Employee Referral" & Sales Stage "Lost"
    And I click to Save button
    And I wait for 2 seconds
    Then I found existing Deals Price & Product & Company of "$350.00 Apple - Abata" -- In Addressbook
    And I wait for 2 seconds

  Scenario: As Admin User, Addressbook -> In Deals Tab -> Marked Deal as Completed (Won)
    Given I wait for 5 seconds
    Then I click on Contacts Tab
    Then I Create Deals in Details View with Product "Diamond" & Price "$450.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I click to Save button
    And I wait for 2 seconds
    Then I found existing Deals : "$450.00 Diamond - Aba" and select Radio button -- In Addressbook
    Then I found existing Deals Price & Product & Company of "$450.00 Diamond - Abata" and Sales Stage: "Won" -- In Addressbook
    And I wait for 2 seconds

  Scenario: As Admin User, Addressbook -> In Deals Tab -> Change Sales Stage From Drop-down to Won
    Given I wait for 5 seconds
    Then I click on Contacts Tab
    Then I Create Deals in Details View with Product "Gold" & Price "$4000.00" & Lead Source "Mailing" & Sales Stage "Prospect"
    And I click to Save button
    And I wait for 5 seconds
    Then I found existing Deals Price & Product & Company of "$4,000.00 Gold - Abata" & Sale Stage "Prospect" & then Change Sales Stage to: "Won"
    And I wait for 5 seconds
    Then I found existing Deals Price & Product & Company of "$4,000.00 Gold - Abata" and Sales Stage: "Won" -- In Addressbook
    And I wait for 2 seconds

  Scenario: As Admin User, Addressbook -> In Deals Tab -> Change Sales Stage From Drop-down to Lost
    Given I wait for 5 seconds
    Then I click on Contacts Tab
    Then I Create Deals in Details View with Product "Motherboard" & Price "$5000.00" & Lead Source "Tradeshow" & Sales Stage "Negotiations"
    And I click to Save button
    And I wait for 5 seconds
    Then I found existing Deals Price & Product & Company of "$5,000.00 Motherboard - Abata" & Sale Stage "Negotiations" & then Change Sales Stage to: "Lost"
    And I wait for 5 seconds
    Then I found existing Deals Price & Product & Company of "$5,000.00 Motherboard - Abata" and Sales Stage: "Lost" -- In Addressbook
    And I wait for 2 seconds
Feature: Do Quick Regression Test on Addressbook

  Background: Pre-reqisties
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Mixed activity access to conduct the test execution
#    Given Local Test User Account
    Given Wait until the Application is ready
    Given I wait for 10 seconds

  @short
  Scenario: Create new contact (Short) in addressbook
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I create a New contact in Short
    And I wait for 5 seconds
    And I accept to proceed remaining scenario
    And I wait for 1 seconds

  @detail
  Scenario: Create new contact (Detail) in Addressbook
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I create a new contact in Detail
    And I wait for 5 seconds
    And I accept to proceed remaining scenario
    And I wait for 1 seconds

  @delete
  Scenario: Delete a Contact from Addressbook
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I Delete a contact
    And I wait for 5 seconds

  @tags
  Scenario: Few Contacts Assign in a Group/Tags
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
    And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
    Then To create Tags I fill in "MyGroupTags" and save it
    And I wait for 5 seconds
    And I switch to Contact List View
    And I select three row from List View
    And I Assign few contacts in "MyGroupTags" Tag
    And I wait for 5 seconds

  @group
  Scenario: Few contacts Un-assign from a Group
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
    And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
    Then To create Tags I fill in "Un-assign" and save it
    And I wait for 5 seconds
    And I switch to Contact List View
    And I select three row from List View
    And I Assign few contacts in "Un-assign" Tag
    And I wait for 5 seconds
    And I select three row from List View
    And I want to Unassign few contacts from newly created Group
    And Save Tags Button clicked
    And I wait for 5 seconds

  @group
  Scenario: Create group from List View Drop-down menu
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
#   And I wait for 5 seconds
#   And I select Tags Under Tags Tab
#   And I wait for 5 seconds
#   And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
#   Then To create Tags I fill in "Tagging Contact" and save it
    And I wait for 5 seconds
    And I switch to Contact List View
    And I select three row from List View
    And I quick create a Tag of "Tag It" and save it
    And I wait for 5 seconds

  @cl
  Scenario: From Manage Columns to Select All Columns
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I switch to Contact List View
    And I select Manage Column options
    And I wait for 3 seconds
    And I select All Columns Checkbox
    And I save the selected Columns
    And I wait until List View Table Load on Screen
    And I wait for 5 seconds

  @c2
  Scenario: From Manage Columns to Un-Select All Columns
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I switch to Contact List View
    And I select Manage Column options
    And I wait for 3 seconds
    And I Unselect All Columns Checkbox, Except One
    And I save the selected Columns
    And I wait until List View Table Load on Screen
    And I wait for 5 seconds

#  @manual
#  Scenario: Export Contacts
#    Given I click on Contacts Tab
#    And I wait for 5 seconds
#    And I switch to Contact List View
#    And I wait until List View Table Load on Screen
#    And I select three row from List View
#    And Export Contacts selected
#    And I wait for 5 seconds

    #STORY BLOCK: CONTACT PRINT

  @report_print
  Scenario: Contact Report - All Contacts - Print
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Print button on the contact report page
    And I wait for 2 seconds
#   Then I want to switch between windows
#   And I wait for 5 seconds
#   Then I accept the popup
#   And I wait for 30 seconds

  @report_print
  Scenario: Contact Report - All Contacts - Cancel
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Cancel button on the contact report page
    And I wait for 2 seconds

  @report_print
  Scenario: Contact Report - All Contacts - Status
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Status button on the contact report page
    And I wait for 30 seconds

  @report_print
  Scenario: Contact Report - All Contacts - Status - All Contacts
    And I wait for 50 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Status button on the contact report page
    And I wait for 2 seconds
    Then I select option "All Contacts"
    And I wait for 2 seconds
    Then I click on the Print button on the contact report page
    And I wait for 5 seconds

  @report_print
  Scenario: Contact Report - All Contacts - Status - Current Lookup
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Status button on the contact report page
    And I wait for 2 seconds
    Then I select option "Current Lookup"
    And I wait for 2 seconds
    Then I click on the Print button on the contact report page
    And I wait for 5 seconds

  @report_print
  Scenario: Contact Report - All Contacts - Status - Current Contact
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the contact right panel Dropdown Icon
    And I wait for 2 seconds
    Then I select "Contact Report" from the dropdown list
    And I wait for 2 seconds
    Then I click on the Status button on the contact report page
    And I wait for 2 seconds
    Then I select option "Current Contact"
    And I wait for 2 seconds
    Then I click on the Print button on the contact report page
    And I wait for 5 seconds

  @iconemail
  Scenario: Contact Report - Right panel option
    And I wait for 10 seconds
    And I click on Contacts Tab
    Then I click on the Email Icon
    And I wait for 5 seconds

    #STORY BLOCK: REPLACE FIELD

  @c3
  Scenario: Replace Field Update - All Contacts Selected to Replace Field Use
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select All Contacts -- while in Addressbook
    And Replace Field Data selected "Prefix" and Replace with "Another Data"
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  @details_view_replace @refactor
  Scenario: Replace Field Update - Select a Tag and Replace Field Use (Details View)
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I create a group name as "Details Group"
    And I switch to Contact List View
    And I select three row from List View
    And I Assign few contacts in "Details Group" Group
    And I wait for 5 seconds
    And I switch to Contact Details View
    And I select All Contacts from - right panel container
    And I Found the Group named as "Details Group"
    And I wait for 3 seconds
    And Replace Field Data selected "Title" and Replace with "Mr."
    And I wait for 5 seconds
    And I accept to proceed remaining scenario
    And I wait until Details View Block Load on Screen
    And I wait for 5 seconds

  @list_view_replace @refactor
  Scenario: Replace Field Update - Select a Group and Replace Field Use (List View)
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I create a group name as "List Group"
    And I switch to Contact List View
    And I select three row from List View
    And I Assign few contacts in "List Group" Group
    And I wait for 5 seconds
    And I select All Contacts from - right panel container
    And I Found the Group named as "List Group"
    And I wait for 3 seconds
    And I select three row from List View
    And Replace Field Data selected "City" and Replace with "Dhaka"
    And I wait for 5 seconds
    And I accept to proceed remaining scenario
    And I wait until List View Table Load on Screen
    And I wait for 5 seconds


  Scenario: My Contacts
    Given I wait for 4 seconds
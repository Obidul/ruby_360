@email
Feature: Email Service to Auto Posting
  Direct Campaign Email
  Scheduled Single Email
  Scheduled Campaign Email
  Campaign Email + Single email

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Email Auto Post access to conduct the test execution
    Given Wait until the Application is ready

  ## STORY BLOCK: DIRECT POST =>  "CAMPAIGN" EMAIL

  @dir-camp-facebook
  Scenario: As Admin, Direct - Campaign Email - Auto Post - Facebook Network
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Direct - Campaign Email - Auto Post - Facebook Network"
    And I click on Send button
    And I wait for 5 seconds
    Then I select Facebook for Social Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  @dir-camp-twitter
  Scenario: As Admin, Direct - Campaign Email - Auto Post - Twitter Network
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Direct - Campaign Email - Auto Post - Twitter Network"
    And I click on Send button
    And I wait for 5 seconds
    Then I select Twitter for Social Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  @dir-camp-linkedin
  Scenario: As Admin, Direct - Campaign Email - Auto Post - Linkedin Network
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Direct - Campaign Email - Auto Post - Linkedin Network"
    And I click on Send button
    And I wait for 5 seconds
    Then I select Linkedin for Social Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  @dir-camp-all_social_networks
  Scenario: As Admin, Direct - Campaign Email - Auto Post - All Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Direct - Campaign Email - Auto Post - All Networks"
    And I click on Send button
    And I wait for 5 seconds
    Then I select All Network for Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  ### STORY BLOCK: SCHEDULED POST => "CAMPAIGN" EMAIL

  @sch-camp-facebook @test5
  Scenario: As Admin, Scheduled - Campaign Email - Auto Post - Facebook
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Scheduled - Campaign Email - Auto Post - Facebook"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Facebook for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Campaign Email - Auto Post - Facebook"
    And Logged out from system

  @sch-camp-twitter @test6
  Scenario: As Admin, Scheduled - Campaign Email - Auto Post - Twitter
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Scheduled - Campaign Email - Auto Post - Twitter"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Twitter for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Campaign Email - Auto Post - Twitter"
    And Logged out from system

  @sch-camp-linkedin @test7
  Scenario: As Admin, Scheduled - Campaign Email - Auto Post - Linkedin
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Scheduled - Campaign Email - Auto Post - Linkedin"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Linkedin for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Campaign Email - Auto Post - Linkedin"
    And Logged out from system

  @sch-camp-all_social_networks
  Scenario: As Admin, Scheduled - Campaign Email - Auto Post - All Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Scheduled - Campaign Email - Auto Post - All Networks"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select All Network for Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Campaign Email - Auto Post - All Networks"
    And Logged out from system

  ## STORY BLOCK: SCHEDULED POST => "SINGLE" EMAIL

  @sch-single-facebook
  Scenario: As Admin, Scheduled - Single Email - Auto Post - Facebook
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed an Email Address "sakibqa@gmail.com"
    And In Subject field: I fill in "Scheduled - Single Email - Auto Post - Facebook"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Facebook for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Single Email - Auto Post - Facebook"
    And Logged out from system

  @sch-single--twitter
  Scenario: As Admin, Scheduled - Single Email - Auto Post - Twitter
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed an Email Address "sakibqa@gmail.com"
    And In Subject field: I fill in "Scheduled - Single Email - Auto Post - Twitter"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Twitter for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Single Email - Auto Post - Twitter"
    And Logged out from system

  @sch-single-linkedin
  Scenario: As Admin, Scheduled - Single Email - Auto Post - Linkedin
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed an Email Address "sakibqa@gmail.com"
    And In Subject field: I fill in "Scheduled - Single Email - Auto Post - Linkedin"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Linkedin for Social Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Single Email - Auto Post - Linkedin"
    And Logged out from system

  @sch-single-all_social_networks
  Scenario: As Admin, Scheduled - Single Email - Auto Post - All Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed an Email Address "sakibqa@gmail.com"
    And In Subject field: I fill in "Scheduled - Single Email - Auto Post - All Networks"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select All Network for Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Single Email - Auto Post - All Networks"
    And Logged out from system

  ## STORY BLOCK: DIRECT & SCHEDULED POST => "CAMPAIGN + SINGLE" EMAIL (COMBINED)
  ## STORY BLOCK: WORK IN PROGRESS -- NOT WORKING.

  @dir-with-single-all-networks
  Scenario: As Admin, Direct Campaign + Single Email to Auto Posting - All Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In field: Typed "Auto" and found "Auto Post Group" campaign group and selected "Auto Post Group" from the list and input "sakibqa@gmail.com"
    And In Subject field: I fill in "Direct Campaign + Single Email to Auto Posting - All Networks"
    And I click on Send button
    And I wait for 5 seconds
    Then I select All Network for Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  @sch-with-single-all-networks
  Scenario: As Admin, Scheduled Campaign + Single Email to Auto Posting - All Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Dha" campaign group and found "Dhaka" and selected "Dhaka" from the list
    And I add contacts of "autopostdhaka@brainstation-23.com"
    And In Subject field: I fill in "Scheduled Campaign + Single Email to Auto Posting - All Networks"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select All Network for Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled Campaign + Single Email to Auto Posting - All Networks"
    And Logged out from system

  ## STORY BLOCK: DIRECT & SCHEDULED POST => COMBINED => "FB + LK, FB + TW, TW + LK."

  #Add More

  @test15
  Scenario: As Admin, Direct - Campaign Email - FB + LK Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Direct - Campaign Email - FB + LK Networks"
    And I click on Send button
    And I wait for 5 seconds
    Then I select Facebook & Linkedin Network for Auto Post & Send Now
    And I should see Email has sent confirmation
    And Logged out from system

  @test16
  Scenario: As Admin, Scheduled - Campaign Email - TW + LK Networks
    And I wait for 10 seconds
    Given Go for Email Service
    And I wait for 5 seconds
    And Go for Email Compose
    And I wait for 10 seconds
    And In To field: I typed "Auto" campaign group and found "Auto Post Group" and selected "Auto Post Group" from the list
    And In Subject field: I fill in "Scheduled - Campaign Email - TW + LK Networks"
    And I click on Schedule button
    And I wait for 5 seconds
    Then I select Twitter & Linkedin Network for Auto Post & Saved for Schedule
    And I wait for 10 seconds
    And Email has scheduled in list with this subject: "Scheduled - Campaign Email - TW + LK Networks"
    And Logged out from system
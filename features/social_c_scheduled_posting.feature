@social
Feature: Scheduled social posting - All combinations
    Text/Link/Image post to all social network with possible all combinations.

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    #    Given As Admin User, Social Module access to conduct the test execution
    Given Social Access
    Given Wait until the Application is ready

  #STORY BLOCK: TC-S1 = TC-S6

  @TC-S1
  Scenario: Facebook Profile (FP) = Text
    Given Go for Social Post
    And Select Facebook Profile and Fill "World wide web - SS1"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S2
  Scenario: Facebook Profile (FP) = Link
    Given Go for Social Post
    And Select Facebook Profile and Fill "http://www.facebook.com/SS2"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S3
  Scenario: Facebook Profile (FP) = Text + Link
    Given Go for Social Post
    And Select Facebook Profile and Fill "World wide web - SS3 + http://www.facebook.com/profile-post - SS3"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S4 @wip
  Scenario: Facebook Profile (FP) = Text + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "World wide web - SS4 + http://www.facebook.com/profile-post - SS4"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S5 @wip
  Scenario: Facebook Profile (FP) = Link + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "World wide web - SS5 http://www.facebook.com/profile-post - SS5"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S6 @wip
  Scenario: Facebook Profile (FP) = Text + Link + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "World wide web - SS6 + http://www.facebook.com/profile-post - SS6"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S7 - TC-S10 -> Switch Hooks

  @TC-S7
  Scenario: Facebook Fan Page (FFP) = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "World wide web - SS7"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S8
  Scenario: Facebook Fan Page (FFP) = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "http://www.facebook.com/SS8"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S9
  Scenario: Facebook Fan Page (FFP) = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Intranet and Internet http://www.facebook.com/profile-post/SS9 - SS9"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S10 @wip
  Scenario: Facebook Fan Page (FFP) = Text + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Today, Intranet and Internet + http://www.facebook.com/profile-post - SS10"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S11 @wip
  Scenario: Facebook Fan Page (FFP) = Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "From Mars + http://www.facebook.com/profile-post - SS11"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S12 @wip
  Scenario: Facebook Fan Page (FFP) = Text + Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Facebook Fan Page (FFP) - Text + http://www.facebook.com/profile-post - SS12"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S13 - TC-S15

  @TC-S13
  Scenario: Linkedin Profile (LP) = Text
    Given Go for Social Post
    And Select Linkedin Profile and Fill "Linkedin Profile (LP) - Text - SS13"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S14
  Scenario: Linkedin Profile (LP) = Link
    Given Go for Social Post
    And Select Linkedin Profile and Fill "http://www.linkedin.com/SS14"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S15
  Scenario: Linkedin Profile (LP) = Text + Link
    Given Go for Social Post
    And Select Linkedin Profile and Fill "http://www.linkedin.com/ & Linkedin Profile (LP) - Text - SS15"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S16 - TC-S18 -> Switch Hooks

  @TC-S13 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - SS16"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S14 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - SS17"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S15 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - SS18"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S19 - TC-S24

  @TC-S19
  Scenario: Twitter (TW) = Text
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text - SS19"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S20
  Scenario: Twitter (TW) = Link
    Given Go for Social Post
    And Select Twitter and Fill "http://www.twitter.com/SS20"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S21
  Scenario: Twitter (TW) = Text + Link
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - SS21"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S22 @wip
  Scenario: Twitter (TW) = Text + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - SS22"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S23 @wip
  Scenario: Twitter (TW) = Link + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - SS23"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S24 @wip
  Scenario: Twitter (TW) = Text + Link + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - SS24"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S25 - TC-S30
  #WIP: WORK IN PROGRESS - FAIL TO EXECUTE

  @TC-S25
  Scenario: Facebook Profile (FP) + TW = Text
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text - SS25"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-S26
  Scenario: Facebook Profile (FP) + TW = Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "http://www.facebooktwitter.com(NEW)/SS26"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-S27
  Scenario: Facebook Profile (FP) + TW = Text + Link
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text + http://www.facebooktwitter00.com/profile-post - SS27"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-S28 @wip
  Scenario: Facebook Profile (FP) + TW = Text + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text + http://www.facebooktwitter.com/profile-post - SS28"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S29 @wip
  Scenario: Facebook Profile (FP) + TW = Link + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW - Text + http://www.facebooktwitter.com/profile-post - SS29"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S30 @wip
  Scenario: Facebook Profile (FP) + TW = Text + Link + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW - Text + http://www.facebooktwitter.com/profile-post - SS30"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S31 - TC-S33

  @TC-S31
  Scenario: Facebook Profile (FP) + LP = Text
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "Facebook Profile (FP) + LP - Text - SS31"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S32
  Scenario: Facebook Profile (FP) + LP = Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "http://www.facebooklinkedin.com/SSD32"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S33
  Scenario: Facebook Profile (FP) + LP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "Facebook Profile (FP) + LP - Text - http://www.facebooklinkedin.com/SSD33 - SS33"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S34 - TC-S36

  @TC-S34 @wip
  Scenario: Facebook Profile (FP) + LCP = Text
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "Facebook Profile (FP) + LCP - Text - SS34"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S35 @wip
  Scenario: Facebook Profile (FP) + LCP = Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "http://www.facebooklinkedincompanypage.com/SSD35"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S36 @wip
  Scenario: Facebook Profile (FP) + LCP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "Facebook Profile (FP) + LCP - Text - http://www.facebooklinkedincompanypage.com/SSD32 - SS36"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S37 - TC-S39

  @TC-S37
  Scenario: Twitter (TW) + LP = Text
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "Twitter (TW) + LP - Text - SS37"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S38
  Scenario: Twitter (TW) + LP = Link
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "http://www.facebooklinkedin.com/SSD38"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S39
  Scenario: Twitter (TW) + LP = Text + Link
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "Twitter (TW) + LP - Text - http://www.facebooklinkedin.com/SSD39 - SS39"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S40 - TC-S42 -> Switch Hook

  @TC-S40 @wip
  Scenario: Twitter (TW) + LCP = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "Twitter (TW) + LCP - Text - SS40"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S41 @wip
  Scenario: Twitter (TW) + LCP = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "http://www.facebooklinkedin.com/SS41"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S42 @wip
  Scenario: Twitter (TW) + LCP = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "Twitter (TW) + LCP - Text - http://www.facebooklinkedin.com/SS42"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S43 - TC-S48

  @TC-S43
  Scenario: Twitter (TW) + FPP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text - SS43"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S44
  Scenario: Twitter (TW) + FPP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "http://www.facebookTwitter.com/SS44"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S45 @wip
  Scenario: Twitter (TW) + FPP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - SS45"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S46 @wip
  Scenario: Twitter (TW) + FPP = Text + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - SS46"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S47 @wip
  Scenario: Twitter (TW) + FPP = Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - SS47"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S48 @wip
  Scenario: Twitter (TW) + FPP = Text + Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - SS48"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S49 - TC-S51

  @TC-S49
  Scenario: Facebook Fan Page (FFP) + LP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "Facebook Fan Page + LP - Text - SS49"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S50
  Scenario: Facebook Fan Page (FFP) + LP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "http://www.facebooklinkedin.com/SS50"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S51
  Scenario: Facebook Fan Page (FFP) + LP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "Facebook Fan Page + LP - Text - http://www.facebooklinkedin.com/SS51"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S52 - TC-S54

  @TC-S52 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "Linkedin Company Page + FFP - Text - SS52"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S53 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "http://www.facebooklinkedin.com/SS53"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S54 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "Linkedin Company Page + FFP - Text - http://www.facebooklinkedin.com/SS54"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S55 - TC-S57

  @TC-S55
  Scenario: Facebook Profile (FP) + TW + LP = Text
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "Facebook Profile TW + LP - Text - SS55"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S56
  Scenario: Facebook Profile (FP) + TW + LP = Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "http://www.facebooklinkedin.com/SS56"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S57
  Scenario: Facebook Profile (FP) + TW + LP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "Facebook Profile TW + LP - Text - http://www.facebooklinkedin.com/SS57"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S58 - TC-S60 -> Switch Hook

  @TC-S52 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Text
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "Facebook Fan Page (FFP) TW + LCP - Text - SS58"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S53 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Link
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "http://www.facebooklinkedin.com/SS59"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @TC-S54 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Text + Link
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "Facebook Fan Page (FFP) TW + LCP - Text - http://www.facebooklinkedin.com/SS60"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S61 - TC-S63
  #WIP: WORK IN PROGRESS

  @TC-S61
  Scenario: Linkedin Profile (LP) + TW + FFP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "Linkedin Profile (LP)  + TW + FFP - Text - SS61"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S62
  Scenario: Linkedin Profile (LP) + TW + FFP = Link
    Given Go for Social Post
    And I wait for 5 seconds
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "http://www.facebooklinkedingoogle.com/SS62"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S63
  Scenario: Linkedin Profile (LP) + TW + FFP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "Linkedin Profile (LP)  + TW + FFP - Text - http://www.facebooklinkedin.com/SS63"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-S64 - TC-S66

  @TC-S64 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "Linkedin Company Profile + TW + FP- Text - SS64"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S65 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "http://www.facebooklinkedin.com/SS65"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-S66 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "Linkedin Company Profile + TW + FP- Text - http://www.facebooklinkedin.com/SS66"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I click Save Post button
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: CUSTOM SCHEDULED POST - DATE/TIME

  @default
  Scenario: Facebook Profile (FP) = Text
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile and Fill "World wide web - SS1"
    Then I wait for 2 seconds
    And Scheduling Social Post with Default Time & Date
    And I wait for 3 seconds
    And I click Save Post button
    Then Clean Browser Session
    And Close all browser windows

  @date
  Scenario: Scheduling Social Post with - Date
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "World wide web in future date 23838"
    And Scheduling Social Post with Date "30"
    And I wait for 3 seconds
    And I click Save Post button
    And I wait for 10 seconds

  @time
  Scenario: Scheduling Social Post with - Time
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "World wide web in future date 484784"
    And Scheduling Social Post with Time "11:00 PM"
    And I wait for 3 seconds
    And I click Save Post button
    And I wait for 10 seconds

  @datetime
  Scenario: Scheduling Social Post with - Date + Time (2 lines)
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "bright sky and new day and okayddfdd3"
    And Scheduling Social Post, Select Date "30" & Time "2:00 PM"
    And I wait for 3 seconds
    And I click Save Post button
    And I wait for 10 seconds
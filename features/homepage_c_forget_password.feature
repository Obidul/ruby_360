@splash_webpage
Feature: Retrieve Forget Password
  As registered user, I would to get my password from splash360.com system
  As anonymous user, I would to get my password from splash360.com system with invalid data

  Scenario:   As registered user, I would to retrieve my password from splash360.com system
    Given I am on the "/Account/ForgotPassword" page
    When I fill in "username" with "isakib"
    And I click through xpath link ".//*[@id='send_btn']/span"
    Then I should see "Password Sent!" on screen
    Then I wait for 2 seconds

  Scenario: As anonymous user, I would to retrive my password from splash360.com system with invalid data
    Given I am on the "/Account/ForgotPassword" page
    When I fill in "username" with "nonononoemailsdadd"
    And I click through xpath link ".//*[@id='send_btn']/span"
    Then I should see "Username is invalid" on screen
    Then I wait for 2 seconds
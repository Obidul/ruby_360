Feature: SBE - Combined Search Query to get results

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Search By Example access to conduct the test execution
    Given Wait until the Application is ready

  @test5
  Scenario: SBE - Multiple Field Search
    And Go for Search By Example
    And Multiple Search with - "#city" & Start with "New York"
    And Multiple Search with - "#country" & Start with "United States"
    And I click to Search Button -- Search By Example
    And I wait for 3 seconds

require_relative '../../lib/cucumber_helper'

# Note:
#Important to wait for 10 more seconds, unless most of the scenario fails -- we have no tweaks or work around here.
#Basic Advanced Search Scenario will be similar like this (added below)
#Modified Date and few other changes requires to be replicated before to expect the outcomes.
#Follup Documents -> http://tinyurl.com/jut7ph4
#Create Date and Modified Data - Expected Value depends on the contact created and contact modified.

Given /^Go for Advanced Search$/ do
  find('.topSearchArrow').click
  expect(page).to have_selector('.splash_panel_listitem', wait: 5)
  find('.splash_panel_listitem', :text => 'Advanced Search', exact: true).click
  # expect(page).to have_selector('#AddressBookSmartGroupContent', visible: true)
end

#Generic Conditions

#note: Eg. Advanced Search contact based on "company" and select conditions "Is equal to" and user's given input "Abata"
Given /^Select "([^"]*)" with the conditions of "([^"]*)" and fill in "([^"]*)"$/ do |contact_field, conditions, user_input|
  find('#listField0__iCombo').click
  page.find('li', :match => :first, :text => contact_field, exact: true).click #added :match => :first -- if any major impacts happens remove this portion.
  find('#listContain0__iCombo').click
  page.find('li', :text => conditions, exact: true).click
  find('#txtField0').click
  find('.right-list').set "#{user_input}"
  find('.promtbtn.btnsave').click
end

#note: Generic Conditions for Is Not Blank and Is Blank
Given /^Select "([^"]*)" with the conditions of "([^"]*)"$/ do |contact_field, conditions|
  find('#listField0__iCombo').click
  page.find('li', :match => :first, :text => contact_field, exact: true).click
  find('#listContain0__iCombo').click
  page.find('li', :text => conditions, exact: true).click
  find('.promtbtn.btnsave').click
end

#Only Status Query in Advanced Search
Given /^Select only Status with the conditions of "([^"]*)" and select "([^"]*)"$/ do |conditions, conditions_secondary|
  find('#listField0__iCombo').click
  find('ul li', :text => /\AStatus\z/, :match => :prefer_exact).click
  find('#listContain0__iCombo').click
  sleep 0.5
  page.find('li', :text => conditions, :match => :prefer_exact).click
  find('#listIdAccManger0__iCombo').click
  sleep 0.5
  page.find('li', :text => conditions_secondary, :match => :prefer_exact).click
  find('.promtbtn.btnsave').click
end

#note: Eg. Advanced Search Email Status
Given /^Select "([^"]*)" with the conditions of "([^"]*)" and select "([^"]*)"$/ do |contact_field, conditions, conditions_secondary|
  find('#listField0__iCombo').click
  page.find('li', :text => contact_field, exact: true).click
  # find('#addressBookDiv #secondaryActionListTab li', :text => /\AStatus\z/, exact: true).click
  find('#listContain0__iCombo').click
  sleep 0.5
  page.find('li', :text => conditions, :match => :prefer_exact).click
  find('#listIdAccManger0__iCombo').click
  sleep 0.5
  page.find('li', :text => conditions_secondary, :match => :prefer_exact).click
  find('.promtbtn.btnsave').click
end

Given /^I click for Search Button -- Advanced Search$/ do
  find('#btnOK').click
end

#Note: Applied in Anniversary & Birthday and Create Date & Modify Date:
#      Eg. Advanced Search "Anniversary & Birthday"" and select conditions "Between" and user's given input "Start MM/DD and End MM/DD"
#      Eg. Advanced Search "Create Date & Modified Date"" and select conditions "Between" and user's given input "Start MM/DD/YYYY and End MM/DD/YYYY"

Given /^Select "([^"]*)" with the conditions of "([^"]*)" and fill in Start Date "([^"]*)" and End Date "([^"]*)"$/ do |contact_field, conditions, start_mm_dd_yy, end_mm_dd_yy|
  find('#listField0__iCombo').click
  page.find('li', :text => contact_field, exact: true, wait: 5).click
  find('#listContain0__iCombo').click
  page.find('li', :text => conditions, :match => :prefer_exact).click
  find('#txtFieldS0').click
  find('.start-list').set "#{start_mm_dd_yy}"
  find('#txtFieldE0').click
  find('.end-list').set "#{end_mm_dd_yy}"
  find('.promtbtn.btnsave').click
end


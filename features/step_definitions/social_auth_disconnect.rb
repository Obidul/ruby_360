require_relative '../../lib/cucumber_helper'

#disable social accounts #

Given /^I want to disable twitter social account$/ do
  find(:xpath, ".//*[@id='divTwitterChangeProfile']").click
  find(".disableButton").click
  sleep 0.2
end

Given /^I want to disable linkenin social account$/ do
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find(".disableButton").click
  sleep 0.2
end

Given /^I want to disable facebook social account$/ do
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find(".disableButton").click
  sleep 0.2
end

Given /^Disable all social network$/ do
  #Twitter Inactive
  find(:xpath, ".//*[@id='divTwitterChangeProfile']").click
  find(".disableButton").click
  sleep 0.2

  if page.driver.class == Capybara::Selenium::Driver
    page.driver.browser.switch_to.alert.accept
  elsif page.driver.class == Capybara::Webkit::Driver
    sleep 1 # prevent test from failing by waiting for popup
    page.driver.browser.accept_js_confirms
  else
    raise "Unsupported driver"
  end

  #Facebook Inactive
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find(".disableButton").click
  sleep 0.2

  if page.driver.class == Capybara::Selenium::Driver
    page.driver.browser.switch_to.alert.accept
  elsif page.driver.class == Capybara::Webkit::Driver
    sleep 1 # prevent test from failing by waiting for popup
    page.driver.browser.accept_js_confirms
  else
    raise "Unsupported driver"
  end

  #Linkedin Inactive
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find(".disableButton").click
  sleep 0.2

  if page.driver.class == Capybara::Selenium::Driver
    page.driver.browser.switch_to.alert.accept
  elsif page.driver.class == Capybara::Webkit::Driver
    sleep 1 # prevent test from failing by waiting for popup
    page.driver.browser.accept_js_confirms
  else
    raise "Unsupported driver"
  end
end


require_relative '../../lib/cucumber_helper'

Given /^I should see "([^"]*)" on the left header title$/ do |option|
  find('.left .title', :text=> option, :match => :prefer_exact)
end

Given /^I should see Client Name on the left header title$/ do
  page.execute_script %Q{
   $('.headerProfile').click();
  }
  sleep 0.5
  page.execute_script %Q{
  $(".left .title").text().split(/\s+/)[0] == $('#clientFirstName').html();
  }
end

Given /^I create an activity with subject: "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
end

Given /^I found To Do of "([^"]*)" and select Radio button$/ do |user_input_match|
  page.find('li, .activity', :text => user_input_match, :match => :prefer_exact).find('.rdoFld').find('label[for^=ckStatus_splash_]').click
end
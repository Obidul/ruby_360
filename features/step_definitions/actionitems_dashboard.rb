require_relative '../../lib/cucumber_helper'

#Note: In scenario during argument pass - we kept liked this "blocked " -- there is space, removing space might case failure.
# followup with html to understand.

#Primary Action List
Given /^I create an activity with "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
  page.execute_script %Q{ _$.ui.fire('clean') }
end

Given /^I create activity of Todo with "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find('.selTxt').click
  find('.easy-list-item', :text => 'To Do').click
  find(:xpath, ".//*[@id='addTodo']/form/div[1]/div[1]/textarea").set "#{user_input}"
end

Given /^I select activity type "([^"]*)"$/ do |activity_type|
  find('.selTxt').click
  find('.easy-list-item', :text => activity_type).click
end

Given /^I searched for "([^"]*)" & select "([^"]*)" from the list$/ do |type, selected|
  find(:xpath, "//*[@placeholder = 'Enter company name or contact name.']").set "#{type}"
  sleep 1
  page.find('li', :text => selected, :match => :prefer_exact).click
end

Given /^I click to Save button$/ do
  find('.red').click
end

Given /^I click to Cancel button$/ do
  find('.cancel').click
end

## Inline Editing
Given /^I want to edit "([^"]*)"$/ do |user_input|
  find('.subject', :text => user_input, :match => :prefer_exact).click
end

Given /^I want to edit activities with "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
end

Given /^I edit activity of "([^"]*)" & select "([^"]*)" from drop-down menu and fill in "([^"]*)"$/ do |user_input, drop_down_menu, user_input_to_update|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input_to_update}"
end

Given /^I comments on activity of "([^"]*)" & clicked on comment icon and fill in "([^"]*)"$/ do |user_input, user_comments|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.btn-comment').click
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
end

Given /^I comments on activity of "([^"]*)" and I have select "([^"]*)" from drop-down menu and fill in "([^"]*)"$/ do |user_input, drop_down_menu, user_comments|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
end

#Todo Scenario
Given /^I found Todo: "([^"]*)" on Grid list and Updated to "([^"]*)"$/ do |user_input, update_user_entry|
  page.find('.activity .comment', :text => user_input, :match => :prefer_exact).click
  find(:xpath, ".//*[@id='addTodo']/form/div[1]/div[1]/textarea").set "#{update_user_entry}"
end

Given /^I found Todo: "([^"]*)" on Grid list and I have selected "([^"]*)" from drop-down menu and fill in "([^"]*)"$/ do |user_input, drop_down_menu, update_user_entry|
  page.driver.browser.action.move_to(page.find('.activity .comment', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
  find(:xpath, ".//*[@id='addTodo']/form/div[1]/div[1]/textarea").set "#{update_user_entry}"
end

#Starred Specific Activity
Given /^I found activity of "([^"]*)" and select Star button$/ do |user_input_match|
  page.find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).find('.gridIcon').click
end

Given /^I found activity of "([^"]*)" and unselect Star button$/ do |user_input_match|
  page.find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).find('.gridIcon').click
end

Given /^I should see Star button is active$/ do
  page.execute_script %Q{ $('#addressbookBody .customStyle.ckStarred[type="checkbox"]:checked') }
end

#Radio Button Select
Given /^I found activity of "([^"]*)" and select Radio button$/ do |user_input_match|
  page.find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).find('.rdoFld').find('label[for^=ckStatus_splash_]').click
end

#
# Given /^I want to return completed activity by clicking on checkbox from Grid list$/ do
#   find('.rdoFld').find('label[for^=ckStatus_splash_]').click
# end

#Verification of completed items
Given /^I switch to History to see Completed Items$/ do
  find('.icon-history').click
end

#Drop-down Inline Actions START

Given /^I found activity of "([^"]*)" and select View Contact from Drop-down menu$/ do |user_input_match|
  page.driver.browser.action.move_to(page.find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => 'View Contact').click
  expect(page).to have_selector('#contact', wait: 1)
end

Given /^I want to Email Contact from Grid list$/ do
  page.driver.browser.action.move_to(page.find('.subject', match: :first).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => 'Email Contact').click
  # expect(page).to have_selector('#contact', wait: 1)
end

Given /^I found activity of "([^"]*)" and select Delete from Drop-down menu$/ do |user_input_match|
  page.driver.browser.action.move_to(page.find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => 'Delete').click
end

Given /^I found Todo: "([^"]*)" on Grid list and I have selected "([^"]*)" from drop-down menu$/ do |user_input, drop_down_menu|
  page.driver.browser.action.move_to(page.find('.activity .comment', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
end

#Drop-down Inline Actions END

#Reminder and Star Hidden Selection - start

Given /^I select Reminder Clock$/ do
  find('label[for^=ckClk_splash_]').click
end

Given /^I select Star$/ do
  find('.ckbtnGrp').find('label[for^=ckStar_splash_]').click
end

Given /^I un-select Reminder Clock$/ do
  find('label[for^=ckClk_splash_]').click
end

Given /^I un-select Star$/ do
  find('.ckbtnGrp').find('label[for^=ckStar_splash_]').click
end

Given /^I select Reminder Clock & Star$/ do
  find('label[for^=ckClk_splash_]').click
  find('.ckbtnGrp').find('label[for^=ckStar_splash_]').click
end

#Reminder and Star Hidden Selection - end

#Select Checkbox marked and Checkbox Un-marked - Start

Given /^I checked mark an activity as completed$/ do
  # find('.rdoFld').find('label[for^=ckStatus_splash_]').click
  # find('.activity', match: :first).find('.role', match: :first).
  find('.rdoFld', match: :first).find('label[for^=ckStatus_splash_]').click
end

# Given /^I verify completed activity has enlisted$/ do
#   find('.rdoFld', match: :first).find('label[for^=ckStatus_splash_]').click
# end

#Select Checkbox marked and Checkbox Un-marked - End

#Matching the Entry has enlisted -- START

#Comments within comments block
Given /^I should see "([^"]*)" comment has enlisted$/ do |user_input|
  find('.comment-text', :text => user_input, :match => :prefer_exact)
end

#Verification Start

#Todo-task Verification
Given /^I should see "([^"]*)" Todo has enlisted$/ do |user_input|
  find('.comment', :text => user_input, :match => :prefer_exact)
end

#Task activities within subject block
Given /^I should see "([^"]*)" activity has enlisted$/ do |user_input|
  find('.subject', :text => user_input, :match => :prefer_exact)
end

#Scrolling
Given /^I scroll to find the content$/ do
  page.execute_script %Q{ 'window.scrollBy(0,100000)' }
  end

Given /^I scroll to mid level$/ do
  page.evaluate_script %Q{ $( "#searchByExampleViewBody #WomanOwned" ).scrollTop( 700 );}
end


#Verification End

#Matching the Entry has enlisted -- END

## Date & Time & Calender -- Start

Given /^I select Meeting$/ do
  find('.selTxt').click
  find('.easy-list-item', :text => 'Meeting').click
end

Given /^I select Calls$/ do
  find('.selTxt').click
  find('.easy-list-item', :text => 'Call').click
end

Given /^I select Task$/ do
  find('.selTxt').click
  find('.easy-list-item', :text => 'Task').click
end

#ID/Status - Select/Unselect/Verification - Start

When /^I found activity of "([^"]*)" and change ID&Status to "([^"]*)"$/ do |user_input_match, id_status_drop_down_menu|
  find('li, .activity, .subject', :text => user_input_match, :match => :prefer_exact).find('li.activity .role .hotspot').click
  find('.splash_easy_row', :text => id_status_drop_down_menu).click
end

#ID/Status - Select/Unselect/Verification - End
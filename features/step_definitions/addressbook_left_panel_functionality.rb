require_relative '../../lib/cucumber_helper'

Given /^I create an activity with "([^"]*)" in Details View$/ do |user_input|
  find('.addEvent').click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
end

#Notes
Given /^I found Notes of "([^"]*)" and I have select "([^"]*)" from drop-down menu and fill in "([^"]*)"$/ do |user_input, drop_down_menu, users_notes|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
  find(:xpath, "//*[@placeholder = 'What’s up?']").set "#{users_notes}"
end

Given /^I found Notes of "([^"]*)" and I have select "([^"]*)" from drop-down menu$/ do |user_input, drop_down_menu|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
end

Given /^I found Notes: "([^"]*)" on Details View Grid list$/ do |user_input|
  page.find('.activity .subject', :text => user_input, :match => :prefer_exact)
end

#Deals
Given /^I Create Deals in Details View with Product "([^"]*)" & Price "([^"]*)" & Lead Source "([^"]*)" & Sales Stage "([^"]*)"$/ do |product, price, lead_source, sales_stage|
  find('.customeTabs', :text => 'Deals').click
  find('.addDeal').click
  find(:xpath, "//*[@placeholder = 'Product']").click
  find('.easy-list-item', :text => product).click
  find(:xpath, "//*[@placeholder = 'Price']").click
  find('.price').set "#{price}"
  find(:xpath, "//*[@placeholder = 'Lead Source']").click
  find('.easy-list-item', :text => lead_source).click
  # find(:xpath, "//*[@placeholder = 'Due Date']").click
  # find('.datePickerSkin', :text => due_date).click
  find(:xpath, "//*[@placeholder = 'Sales Stage']").click
  find('.easy-list-item', :text => sales_stage).click
end

#Email Tabs
Given /^I want to Compose an Email$/ do
  find('.customeTabs', :text => 'Emails').click
  find('.compose').click
end

Given /^I am on Email Tab$/ do
  find('.customeTabs', :text => 'Emails').click
end

Given /^I found sent Email Subject of "([^"]*)" and I select "([^"]*)"$/ do |email_subject, drop_down_menu|
  find('.eventTitle', :text => email_subject, :match => :prefer_exact).hover
  find('.edit-row').click
  sleep 0.2
  find('.splash_easy_row', :text => drop_down_menu).click
end

Given /^I found sent Email Subject of "([^"]*)" and I select Delete Icon$/ do |email_subject|
  find('.eventTitle', :text => email_subject, :match => :prefer_exact).hover
  find('.edit-row').click
  sleep 0.2
  find('.remove-row').click
end

### Deals Dropdown related script

Given /^I found activity of "([^"]*)" and I have select "([^"]*)" from drop-down menu -- In Addressbook$/ do |user_input, drop_down_menu|
  page.driver.browser.action.move_to(page.find('.subject', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
end

Given /^I found existing Deals Price & Product & Company of "([^"]*)" and I select "([^"]*)" from drop-down menu -- In Addressbook$/ do |money_product_company, drop_down_menu|
  page.driver.browser.action.move_to(page.find('.regular .activity', :text => money_product_company, :match => :prefer_exact).native).perform
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :text => drop_down_menu).click
end

##
Given /^I found existing Deals Price & Product & Company of "([^"]*)" -- In Addressbook$/ do |money_product_company|
  # page.driver.browser.action.move_to(page.find('.regular .activity', :text => money_product_company, :match => :prefer_exact).native).perform
  page.driver.browser.action.move_to(page.find('li .activity', :text => money_product_company, :match => :prefer_exact).native).perform
end

# Sales Stage changed
When /^I found existing Deals Price & Product & Company of "([^"]*)" & Sale Stage "([^"]*)" & then Change Sales Stage to: "([^"]*)"$/ do |money_product_company, sale_stage_current, sale_stage_new|
  page.driver.browser.action.move_to(page.find('.regular .activity', :text => money_product_company, :match => :prefer_exact).native).perform
  execute_script %Q{ _$.$('#addressbookBody .role .hotspot:contains("#{sale_stage_current}")').click() }
  find('.splash_easy_row', :text => sale_stage_new).click
end

# Sales stage verification
Given /^I found existing Deals Price & Product & Company of "([^"]*)" and Sales Stage: "([^"]*)" -- In Addressbook$/ do |money_product_company, sales_stage|
  find('li .activity', :text => money_product_company, :match => :prefer_exact) and find('.role', :text => sales_stage, :match => :prefer_exact)
end

#Radio button for Deals complete
Given /^I found existing Deals : "([^"]*)" and select Radio button -- In Addressbook$/ do |user_input_match|
  page.find('li, .activity', :text => user_input_match, :match => :prefer_exact).find('.rdoFld').find('label[for^=ckStatus_splash_]').click
end


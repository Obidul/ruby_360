require_relative '../../lib/cucumber_helper'

#Common Access#

Given /^I am logged in with "([^"]*)" and "([^"]*)"$/ do |username, password|
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "isakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

#Action Items#

Given /^As Admin User, Action Items - Counter Updates Verification - Access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "dashboardta"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - Dashboard access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "dashboardta"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - Filter by Date Activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "dashboardta"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - Right Panel Functionality access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "actionitemdeals"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - Deals access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "actionitemdeals"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - In empty state navigation access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "emptyaccountta"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, User Profile access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "user_profile_access"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Sub User, User Profile access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "user_profile_access"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Admin User, Action Items - Timezone Verification to access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "user_profile_access"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

#Addressbook#

Given /^As Admin User, Addressbook - List View access to conduct the test execution$/ do
  ## binded sub-user, username: mrsubuser, password: 123456
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "listdetailsview"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Addressbook - Mixed activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "addressbookregression"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Advanced Search activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "advancedsearchautomation"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Tags and related activity activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "tagging"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end


Given /^As Admin User, Addressbook - Search By Example access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "searchbyexample"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Addressbook - Quick Search Activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "quicksearch"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Addressbook - Email Status Activity access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "emailstatus"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Admin User, Addressbook Counter Updates Verification - Access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "dashboardta"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Addressbook Left Panel Functionality access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "leftrightaddressbook"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end
#
# Given /^As Admin User, Addressbook - Right Panel Functionality access to conduct the test execution$/ do
#   steps '
#     Given I am on the "/Account/Login" page
#     And I fill in "username" with "isakib"
#     And I fill in "password" with "123456"
#     And I click on "Login" button
#   '
# end

#Social Module#
#Both access should be same for authorization and posting through social module.

Given /^As Admin User, Social Module access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "socialmodule"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Admin User, Social Auth Enable & Disable - Access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "socialmodule"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

#Email Module#

Given /^As Admin User, Email Module access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "isakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Admin User, Email Auto Post access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "autopost"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

Given /^As Admin User, Automated Email access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "isakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^As Admin User, Automated Email Module to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "isakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

#Signup Form

Given /^As Admin User, First time Signup form access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "signupnavigation"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end


Given /^As Admin User, Signup form access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "signupfunctionality"
    And I fill in "password" with "test-automation@#"
    And I click on "Login" button
  '
end

#Print Module

Given /^As Admin User, Print Module access to conduct the test execution$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "isakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

#Local Account -- Always #HASHOUT Before push to QA/Pre/Prod

Given /^Local Test User Account$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "hellosakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^Social Access$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "hellosakib"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end

Given /^Local Quick Search$/ do
  steps '
    Given I am on the "/Account/Login" page
    And I fill in "username" with "quicksearchexception"
    And I fill in "password" with "123456"
    And I click on "Login" button
  '
end
require_relative '../../lib/cucumber_helper'

# Email Right panel dropdown activation
Given /^I click on the Email right panel Dropdown Icon$/ do
  find('.editot_rightPanel_topContent .emailPrintRightHeader .headerIcon').click
end

# Email New Folder Creation
Given /^I want to create A New Folder named "([^"]*)"$/ do |text|
  page.driver.browser.action.move_to(page.find('.rightpanel-item', :text => 'My Folders' , :match => :prefer_exact).native).perform
  sleep 2
  find('.round-plus').click
  find(:xpath, ".//*[@id='txtFolderName']").set "#{text}"
  sleep 3
  find(:xpath, ".//*[@id='txtFolderName']").native.send_key("\n")
end

# Email cross button (cancel)
Given /^I clicked on the cross button to cancel$/ do
  find('.cancelButton').click
end
require_relative '../../lib/cucumber_helper'

#Navigate to Right Panel Drop-down Menus#

Given /^In Activity Tab I select "([^"]*)" and select "([^"]*)"$/ do |activity, drop_down_items|
  find('#dashboardContent .activity-tab li a .navigation-label', text: activity, :match => :prefer_exact).hover
  find('.icon-edit').click
  sleep 0.5
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^In Activity Tab I select "([^"]*)" and select "([^"]*)" -- while in Deals Menu$/ do |activity, drop_down_items|
  find('#dashboardContent .contactActionList li a .navigation-label', text: activity, :match => :prefer_exact).hover
  find('.icon-edit').click
  sleep 0.5
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^In Deals Tab I select "([^"]*)" and select "([^"]*)"$/ do |deals, drop_down_items|
  find('#secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('#dashboardContent #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => deals).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

#Tags

Given /^In Tags Tab I select "([^"]*)" and select "([^"]*)"$/ do |tags, drop_down_items|
  find('#dashboardContent #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => tags).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^I found existing "([^"]*)" tag and rename to "([^"]*)"$/ do |tags, tags_renamed|
  find('#dashboardContent #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => tags).hover
  find('.icon-edit').click
  find('.splash_easy_row', text: 'Rename Tag', :match => :prefer_exact).click
  sleep 0.1
  find('.tagOrFilterText').set "#{tags_renamed}"
  find('.tagOrFilterText').native.send_key("\n")
end

Given /^To create Tags I fill in "([^"]*)" and save it$/ do |tags_fill_in|
  find('#generalGroupTxtField').set "#{tags_fill_in}"
  find('.btnsave').click
end

Given /^In Tags Tab, I select "([^"]*)" this tag$/ do |tags_name|
  find('#secondaryActionListTab li', text: 'Tags', :match => :prefer_exact).click
  sleep 0.5
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => tags_name).click
  sleep 0.2
end

#Filter

Given /^In Filter Tab I select "([^"]*)" and select "([^"]*)"$/ do |filters, drop_down_items|
  find('#dashboardContent #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => filters).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^To create Filter - I fill in "([^"]*)" and save as "([^"]*)"$/ do |filter_name, save_filter_as|
  find('#txtField0').set "#{filter_name}"
  find('.btnsave').click
  find('#smartGroupName').set "#{save_filter_as}"
  find('.arButton.save').click
end

Given /^I found existing "([^"]*)" filter and rename to "([^"]*)"$/ do |filter, filter_renamed|
  find('#dashboardDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => filter).hover
  find('.icon-edit').click
  find('.splash_easy_row', text: 'Rename Filter', :match => :prefer_exact).click
  sleep 0.1
  find('.tagOrFilterText').set "#{filter_renamed}"
  find('.tagOrFilterText').native.send_key("\n")
end

#Status menu
Given /^From Status Tab I select "([^"]*)" and select "([^"]*)"$/ do |status, drop_down_items|
  find('#dashboardDiv a .navigation-label', :match => :prefer_exact, :text => status).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

#Drop-down items -> Landing page to element verification

When /^I wait until View Contacts - Details View is visible$/ do
  expect(page).to have_selector('#AddressBookLeftContent')
  find('#AddressBookLeftContent', visible: true)
end

When /^I wait until Email Contact - Email Compose Module is visible$/ do
  find('#to', visible: true)
  page.execute_script %Q{ _$.ui.fire('clean') }
end

When /^I wait until Edit Deal Stages - Edit Deal Stages is visible$/ do
  find('.title-block', visible: true)
  page.execute_script %Q{ _$.ui.fire('clean') }
end

When /^I wait until Edit Status -  Edit Status page is visible$/ do
  find('.header-title', visible: true)
  page.execute_script %Q{ _$.ui.fire('clean') }
end

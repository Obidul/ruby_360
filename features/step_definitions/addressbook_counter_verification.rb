require_relative '../../lib/cucumber_helper'

#Counter Verification

#Primary Action List Tab
#All Contacts

#diff
Given /^Number of All Contacts and Total counter matched$/ do
  page.execute_script %Q{

      var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      var counter = parseInt (strnumberval.replace(',',''), 10)

      var number = +($(this).find('#addressBookDiv .counter:eq(0)').text());
      var strnumberval = $('#addressBookDiv .counter:eq(0)').html();
      var right_counter = parseInt (strnumberval.replace(',',''), 10)

      counter = right_counter
      }

end

Given /^Number of Starred and Total counter matched$/ do
  page.execute_script %Q{

      var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      var counter = parseInt (strnumberval.replace(',',''), 10)

      var number = +($(this).find('#addressBookDiv .counter:eq(1)').text());
      var strnumberval = $('#addressBookDiv .counter:eq(1)').html();
      var right_counter = parseInt (strnumberval.replace(',',''), 10)

      counter = right_counter
      }
end

#diff
Given /^Number of New Leads and Total counter matched$/ do
  page.execute_script %Q{

      var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      var counter = parseInt (strnumberval.replace(',',''), 10)

      var number = +($(this).find('#addressBookDiv .counter:eq(2)').text());
      var strnumberval = $('#addressBookDiv .counter:eq(2)').html();
      var right_counter = parseInt (strnumberval.replace(',',''), 10)

      counter = right_counter
      }
end

#Activity
Given /^Number of All Activity Today and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('#addressBookDiv .counter:eq(4)').html()) }
end

Given /^Number of Meetings Today and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('#addressBookDiv .counter:eq(5)').html()) }
end

Given /^Number of Calls Today and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('#addressBookDiv .counter:eq(6)').html()) }
end

Given /^Number of Tasks Today and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('#addressBookDiv .counter:eq(7)').html()) }
end

#Deals
Given /^Number of All Deals and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_all_deals"] .counter-section .counter').html()) }
end

Given /^Number of Prospect and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_prospect"] .counter-section .counter').html()) }
end

Given /^Number of Needs Analysis and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_needs_analysis"] .counter-section .counter').html()) }
end


Given /^Number of Proposal and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_proposal"] .counter-section .counter').html()) }
end

Given /^Number of Negotiations and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_negotiations"] .counter-section .counter').html()) }
end

Given /^Number of Order&Contract and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="deal_order_contract"] .counter-section .counter').html()) }
end

#status in addressbook
Given /^Number of New Opportunity and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_new"] .counter-section .counter').html())  }
end

Given /^Number of Attempted Contact and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_attempted"] .counter-section .counter').html()) }
end

Given /^Number of Contacted and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_contacted"] .counter-section .counter').html()) }
end

Given /^Number of Hot Prospect and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_hot_prospect"] .counter-section .counter').html()) }
end

Given /^Number of Client and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_client"] .counter-section .counter').html()) }
end

Given /^Number of Disqualified and Total counter matched$/ do
  page.execute_script %Q{ var number = +($(this).find('.totalContact').text());
      var strnumberval = $('.totalContact').html();
      parseInt (strnumberval.replace(',',''), 10) == parseInt ($('a[data-navigation="idstatus_disqualified"] .counter-section .counter').html()) }
end

#Subscribers - active/inactive

#diff
Given /^Number of Active Subscribers and Total counter matched$/ do
  page.execute_script %Q{

  var number = +($(this).find('.totalContact').html());
  var strnumberval = $('.totalContact').html();
  var left_counter = parseInt (strnumberval.replace(',',''), 10)

  var number = +($(this).find('a[data-navigation="subscription_active"] .counter-section .counter').html());
  var strnumberval = $('a[data-navigation="subscription_active"] .counter-section .counter').html();
  var right_counter = parseInt (strnumberval.replace(',',''), 10)

  left_counter == right_counter

  }
end

#diff
Given /^Number of Inactive Subscribers and Total counter matched$/ do
  page.execute_script %Q{

  var number = +($(this).find('.totalContact').html());
  var strnumberval = $('.totalContact').html();
  var left_counter = parseInt (strnumberval.replace(',',''), 10)

  var number = +($(this).find('a[data-navigation="subscription_inactive] .counter-section .counter').html());
  var strnumberval = $('a[data-navigation="subscription_inactive] .counter-section .counter').html();
  var right_counter = parseInt (strnumberval.replace(',',''), 10)

  left_counter == right_counter

  }
 end
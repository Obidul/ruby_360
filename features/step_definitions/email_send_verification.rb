require_relative '../../lib/cucumber_helper'

When /^Goto recipient email to verify received email campaign$/ do
  visit('https://mailinator.com/inbox2.jsp?public_to=milonbsdhk')
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Direct - Campaign Email').click
  sleep 2
end

When /^Goto recipient email to verify Preview - Desktop View - Send Test Email$/ do
  visit('https://mailinator.com/inbox2.jsp?public_to=milonbsdhk1')
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Email Preview: See how I look').click
  sleep 2
end

When /^Goto recipient email to verify Preview - Mobile View - Send Test Email$/ do
  visit('https://mailinator.com/inbox2.jsp?public_to=milonbsdhk2')
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Email Preview: See how I look').click
  sleep 2
end
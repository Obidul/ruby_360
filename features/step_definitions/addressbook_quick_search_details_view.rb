require_relative '../../lib/cucumber_helper'

#Business rules:
#1. Contact Name and Company Name is empty -- Although if anyone search, that should be load on screen if any match data found.
#2. Quick search conducts on Quick Profile data
#3. #Note: (LV) = List View & (DV) = Details View

#click functions: working fine

Given /^I quick search with "([^"]*)" -- with click function$/ do |contact_person_name|
  find(:xpath, ".//*[@id='txtSearch']").set "#{contact_person_name}"
end

Then /^I select "([^"]*)" from the list of autocomplete -- with click selection$/ do |contact_person_name|
  page.find('li', :text => contact_person_name, exact: true).click
end

#click functions ends here

#enter functions: working in progress

Given /^I quick search with "([^"]*)" -- with enter function$/ do |quick_search_field|
  find(:xpath, ".//*[@id='txtSearch']").set "#{quick_search_field}"
  find(:xpath, ".//*[@id='txtSearch']").native.send_key("\n")
end

Then /^I select "([^"]*)" from the list of autocomplete - with enter function$/ do |contact_person_name|
  page.find('li', :text => contact_person_name, exact: true).click
end

#enter functions ends here

#no company name and contact name shown
Given /^I quick search with "([^"]*)" -- with enter function and no company and contact info not found$/ do |contact_person_name|
  find(:xpath, ".//*[@id='txtSearch']").set "#{contact_person_name}"
  # page.find('li', :text => 'No Results Found...')
  find(:xpath, ".//*[@id='txtSearch']").native.send_key("\n")
end
#end here

#Backspace verification test5
Given /^I quick search with "([^"]*)" -- In Drop-down Updates Information$/ do |search_on_quick_search_field|
  find(:xpath, ".//*[@id='txtSearch']").set "#{search_on_quick_search_field}"
end

Given /^In quick search I should see "([^"]*)"$/ do |match_info|
  page.find('li', :text => match_info, exact: true)
end
#end here





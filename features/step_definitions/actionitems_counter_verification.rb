require_relative '../../lib/cucumber_helper'

# Right Side:
# parseInt ($('.counter:eq(0)').html()) //Today
# parseInt ($('.counter:eq(1)').html()) //Starred
# parseInt ($('.counter:eq(2)').html()) //Overdue

# Left Side:
# $("li .activity .lgText:contains(Meeting -)").length // Meetings -
# $("li .activity .lgText:contains(Call -)").length // Call -
# $("li .activity .lgText:contains(Task -)").length // Task -
# $("li .time .lgText:contains(To Do)").length // To Do

Given /^Number of Today activity and Today counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(0)').html()) == $("li .time").length }
end

#ref
Given /^Number of Starred Activity and Starred Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(1)').html()) == $('.ckStar[type="checkbox"]:checked').length }
end

#ref
Given /^Number of Overdue Activity and Overdue Counter matched$/ do
  page.execute_script %Q{ $(".list-items .activity.overdue").length == parseInt ($('.counter:eq(2)').html()) }
end

Given /^Number of Total Activity and All Activity Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(3)').html()) == parseInt ($('.counter:eq(4)').html()) + parseInt ($('.counter:eq(5)').html()) + parseInt ($('.counter:eq(6)').html()) + parseInt ($('.counter:eq(7)').html()) }
end

Given /^Number of Meeting Activity and Meeting Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(4)').html()) == $("li .activity .lgText:contains(Meeting -)").length }
end

Given /^Number of Calls Activity and Calls Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(5)').html()) == $("li .activity .lgText:contains(Call -)").length }
end

Given /^Number of Tasks Activity and Tasks Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(6)').html()) == $("li .activity .lgText:contains(Task -)").length
  }
end

Given /^Number of Todo Activity and Todo Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(7)').html()) == $("li .time .lgText:contains(To Do)").length }
end

#status in action stream
Given /^Number of Status Activity and selected New Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(8)').html()) == $('li.activity .role .hotspot:contains(New)').length }
end

Given /^Number of Status Activity and selected Attempted Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(9)').html()) ==  $('li.activity .role .hotspot:contains(Attempted)').length }
end

Given /^Number of Status Activity and selected Hot Prospect Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(10)').html()) ==  $('li.activity .role .hotspot:contains(Hot Prospect)').length }
end

Given /^Number of Status Activity and selected Client Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(10)').html()) ==  $('li.activity .role .hotspot:contains(Client)').length }
end

Given /^Number of Status Activity and selected Contacted Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(11)').html()) ==  $('li.activity .role .hotspot:contains(Contacted)').length }
end
#status

Given /^Number of Status Activity and selected Disqualified Status Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(12)').html()) ==  $('li.activity .role .hotspot:contains(Disqualified)').length }
end

Given /^Number of Team Activity and selected Team Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.counter:eq(13)').html()) == $("li .activity .hotspot").length }
end

Given /^Number of Deals and All Deals Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(8)').html()) == parseInt ($('.deal-tab .navigation-label a, .counter:eq(9)').html()) + parseInt ($('.deal-tab .navigation-label a, .counter:eq(10)').html()) + parseInt ($('.deal-tab .navigation-label a, .counter:eq(11)').html()) + parseInt ($('.deal-tab .navigation-label a, .counter:eq(12)').html()) + parseInt ($('.deal-tab .navigation-label a, .counter:eq(13)').html())  }
  end

Given /^Number of Prospect and Prospect Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(9)').html()) == $("li .role .hotspot:contains(Prospect)").length }
end

Given /^Number of Need Analysis and Need Analysis Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(10)').html()) == $("li .role .hotspot:contains(Needs Analysis)").length }
end

Given /^Number of Proposal and Proposal Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(11)').html()) == $("li .role .hotspot:contains(Proposal)").length }
end

Given /^Number of Negotiations and Negotiations Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(12)').html()) == $("li .role .hotspot:contains(Negotiations)").length }
end

Given /^Number of Order-Contract and Order-Contract Counter matched$/ do
  page.execute_script %Q{ parseInt ($('.deal-tab .navigation-label a, .counter:eq(13)').html()) == $("li .role .hotspot:contains(Order/Contract)").length }
end

# # Left side:
# $("li .role .hotspot:contains(Prospect)").length // Prospect
# $("li .role .hotspot:contains(Needs Analysis)").length // Needs Analysis
# $("li .role .hotspot:contains(Proposal)").length // Proposal
# $("li .role .hotspot:contains(Negotiations)").length // Negotiations
# $("li .role .hotspot:contains(Order/Contract)").length // Order/Contract

#right side:
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(8)').html()) // all deals
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(9)').html()) // prospect
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(10)').html()) // need analysis
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(11)').html()) // proposal
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(12)').html()) // negotiations
# parseInt ($('.deal-tab .navigation-label a, .counter:eq(13)').html()) // order/contract
require_relative '../../lib/cucumber_helper'

Given /^Go for Search By Example$/ do
  find('.topSearchArrow').click
  expect(page).to have_selector('.splash_panel_listitem', wait: 5)
  find('.splash_panel_listitem', :text => 'Search by Example', exact: true).click
  expect(page).to have_selector('#AddressBookLeftContent')
end

#Note Starts In new search page, I will select "city/state/country - give class/id" and fill_in or type in "new york/bangladesh"
# Any Value Pass through RegExp, should be matched with selector (#id, class)

Given /^New Search with - "([^"]*)" & Start with "([^"]*)"$/ do |selector, text|
  find('.title', :text => 'New Search').click #default selected
  find(selector).set "#{text}"
end

Given /^Add to Search with - "([^"]*)" & Start with "([^"]*)"$/ do |selector, text|
  find('.title', :text => 'Add To Search').click
  find(selector).set "#{text}"
end

Given /^Subtract From Results with - "([^"]*)" & Start with "([^"]*)"$/ do |selector, text|
  find('.title', :text => 'Subtract From Results').click
  find(selector).set "#{text}"
end

Given /^Search in Current Results with - "([^"]*)" & Start with "([^"]*)"$/ do |selector, text|
  find('.title', :text => 'Search Current Results').click
  find(selector).set "#{text}"
end

#Note Ends Here.

#########

### NEW SEARCH START ###

#########

#New Search : Email

Given /^New Search with Email & Start with "([^"]*)"$/ do |text|
  find('.title', :text => 'New Search').click #default selected
  find('#searchByExampleViewBody #email').click
  find('#searchByExampleViewBody #email').set "#{text}"
end

#End here

#New Search: Birthday & Anniversary#

Given /^New Search for Birthday & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#addressbookBody #birthdate').val("#{date}");
}

end

Given /^New Search for Anniversary & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#addressbookBody #AnniversaryDateValue').val("#{date}");
}

end

#End Here

#New Search : Email Status

Given /^New Search for Email Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #emailstatus').click
  find('.easy-list-item', :text => select_status).click
end

#End Here

#New Search : Status

Given /^New Search for Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #IDLISTVALUE').click
  find('.easy-list-item', :text => select_status).click
end

#End Here

#New Search : Team Owner

Given /^New Search for Team Owner and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #firstdatevalue').click
  find('.easy-list-item', :text => select_status).click
end

#End Here


#########

### ADD TO SEARCH ###

#########


#New Search: Birthday & Anniversary#

Given /^Add to search for Birthday & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #birthdate').val("#{date}");
}

end

Given /^Add to search for Anniversary & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #AnniversaryDateValue').val("#{date}");
}

end

#End Here

#Add to Search : Email

Given /^Add to Search with Email & Start with "([^"]*)"$/ do |text|
  find('.title', :text => 'Add To Search').click
  find('#searchByExampleViewBody #email').click
  find('#searchByExampleViewBody #email').set "#{text}"
end

#End here

#Add to Search: Email Status

Given /^Add to Search for Email Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #emailstatus', :exact => true).click
  find('.easy-list-item', :text => select_status).click
end

#End here

#Status -- Add to Search

Given /^Add to Search for Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #IDLISTVALUE').click
  find('.easy-list-item', :text => select_status).click
end

#End here

#Add to Search : Team Owner

Given /^Add to Search for Team Owner and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #firstdatevalue').click
  find('.easy-list-item', :text => select_status).click
end

#End Here


#########

### SUBSTRACT FROM RESULTS ###

#########

#Subtract From Results : Email

Given /^Subtract From Results with Email & Start with "([^"]*)"$/ do |text|
  # find('.title', :text => 'Subtract From Results').click
  find('#searchByExampleViewBody #email').click
  find('#searchByExampleViewBody #email').set "#{text}"
end

#End here

#Subtract From Results: Email Status

Given /^Subtract From Results with Email Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  # find('.title', :text => 'Subtract From Results').click #default selected
  find('#searchByExampleViewBody #emailstatus').click
  find('.easy-list-item', :text => select_status).click
end

#End here

#Subtract From Results : Status

Given /^Subtract From Results with Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  # find('.title', :text => 'Subtract From Results').click #default selected
  find('#searchByExampleViewBody #IDLISTVALUE').click
  find('.easy-list-item', :text => select_status).click
end

#End here

#New Search: Birthday & Anniversary#

Given /^Subtract From Results of Birthday & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #birthdate').val("#{date}");
}

end

Given /^Subtract From Results of Anniversary & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #AnniversaryDateValue').val("#{date}");
}

end

#End here

#Subtract From Results : Team Owner

Given /^Subtract From Results of Team Owner and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #firstdatevalue').click
  find('.easy-list-item', :text => select_status).click
end

#End Here

#########

### SEARCH IN CURRENT RESULTS ###

#########

#Search in Current Results: Birthday & Anniversary#

Given /^Search in Current Results with Birthday & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #birthdate').val("#{date}");
}

end

Given /^Search in Current Results with Anniversary & selected Date is "([^"]*)"$/ do |date|
  page.execute_script %Q{ $('input[readonly]').click(function(){
    $(this).removeProp("readonly");
    });

    $('#searchByExampleViewBody #AnniversaryDateValue').val("#{date}");
}

end

#End here

#Search in Current Results : Email

Given /^Search in Current Results with Email & Start with "([^"]*)"$/ do |text|
  # find('.title', :text => 'Subtract From Results').click
  find('#searchByExampleViewBody #email').click
  find('#searchByExampleViewBody #email').set "#{text}"
end

#End here

#Search in Current Results: Email Status

Given /^Search in Current Results with Email Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  # find('.title', :text => 'Subtract From Results').click #default selected
  find('#searchByExampleViewBody #emailstatus').click
  find('.easy-list-item', :text => select_status).click
end

#End here

#Search in Current Results : Status

Given /^Search in Current Results with Status and selected "([^"]*)" from drop-down menu$/ do |select_status|
  # find('.title', :text => 'Subtract From Results').click #default selected
  find('#searchByExampleViewBody #IDLISTVALUE').click
  find('.easy-list-item', :text => select_status).click
end

#End here


#Search in Current Results : Team Owner

Given /^Search in Current Results for Team Owner and selected "([^"]*)" from drop-down menu$/ do |select_status|
  find('#searchByExampleViewBody #firstdatevalue').click
  find('.easy-list-item', :text => select_status).click
end

#End Here


#For Combined Search #Zip, #City, #Country -- Any Value Pass through RegExp.

Given /^Multiple Search with - "([^"]*)" & Start with "([^"]*)"$/ do |selector, text|
  expect(page).to have_selector(".quickProfileWrapper")  #When using or getting into details profile
  find(selector).set "#{text}"
end

Given /^I click to Search Button -- Search By Example$/ do
  find('.saveImg').click
  # expect(page).to have_selector(".content-body-inner")
end

#Search By Example: Search Filtering

Given /^I select New Search$/ do
  find('.title', :text => 'New Search').click
end

Given /^I select Add To Search$/ do
  find('.title', :text => 'Add To Search').click
end

Given /^I select Subtract From Results$/ do
  find('.title', :text => 'Subtract From Results').click
end

Given /^I select Search Current Results$/ do
  find('.title', :text => 'Search Current Results').click
end

#Ends here

# Demo Data:
#
# # | selector              | text          | secs | number  |
# | #city                 | New York      | 2    | 3      |
# | #companyname          | Myworks       | 2    | 2      |
# | #contact              | Sa            | 2    | 2      |
# | #salutation           | Sr            | 2    | 23     |
# | #title                | Mr            | 2    | 45     |
# | #address              | 12            | 2    | 2      |
# | #address2             | Oak Valley    | 2    | 2      |
# | #email                | sakibqa       | 2    | 99     |
# | #phone                | 5             | 2    | 15     |
# | #ext                  | 53130         | 2    | 1      |
# | #state                | Ohio          | 2    | 8      |
# | #cell                 | 4             | 2    | 14     |
# | #fax                  | 2             | 2    | 11     |
# | #zip                  | 6             | 2    | 6      |
#
# # | selector              | text          | secs | number  |
# | #ProfileDescription   | viva          | 10    | 2      |
# | #country              | United States | 10    | 100    |
# | #website              | meetup.com    | 10    | 1      |
# | #Ud1Name              | customteam1   | 10    | 100    |
# | #Ud2Name              | customteam2   | 10    | 100    |
# | #SICCode              | 2             | 10    | 8      |
# | #SICDescription       | non           | 10    | 3      |
# | #AnnualSales          | $77,962.39    | 10    | 1      |
# | #Employees            | 3             | 10    | 2      |
# | #YearsBusiness        | 3             | 10    | 100    |
# | #Franchise            | TRUE          | 10    | 62     |
# | #LocationType         | in            | 10    | 6      |
# | #WomanOwned           | FALSE         | 10    | 57     |
# | #LResidence           | 1             | 10    | 4      |
# | #Income               | $23,607.62    | 10    | 1      |


#    | #OwnRent              | TRUE          | 10    | 63     |
#    | #BldgStructure        | non           | 10    | 1      |
#    | #Age                  | 76            | 10    | 3      |
#    | #Married              | FALSE         | 10    | 54     |
#    | #Gender               | Male          | 10    | 60     |
#    | #Children             | 5             | 10    | 15     |
#    | #Ethnicty             | vestibulum    | 10    | 4      |
#    | #Religion             | at            | 10    | 1      |
#    | #Pool                 | TRUE          | 10    | 60     |
#    | #Pets                 | TRUE          | 10    | 52     |
#    | #CharityDonor         | FALSE         | 10    | 67     |
#    | #OppSeeker            | FALSE         | 10    | 62     |
#    | #OfficeLocation       | 2             | 10    | 14     |
#    | #YearsInBusiness      | 31            | 10    | 1      |
#
#    | #createdate           | 4/13/2016     | 5    | 7      |
#    | #lastmodifiedon       |               | 5    |        |
#    | #IDLISTVALUE          | IDorStatus    | 5    | 100    |
#    | #FIRSTDATEVALUE       | Account       | 5    | 100    |
#    | #birthdate            | Dec 2         | 5    | 1      |
#    | #AnniversaryDateValue | Apr 3         | 5    | 32     |
#    | #emailStatus          | Active        | 5    | 100    |

require_relative '../../lib/cucumber_helper'

# Automated Email proceed from step 2 to step 3
Given /^I click on the Next button$/ do
  find('.arNextButton').click
end

# Select Joins Group
Given /^I choose automated email type: Joins Group$/ do
  page.find('.join').click
end

# Select Date / Event
Given /^I choose automated email type: Date Event$/ do
  page.find('.date').click
end

# Select Email Activity
Given /^I choose automated email type: Email Activity$/ do
  page.find('.email').click
end

# Select Group from the Group List
Given /^I select "([^"]*)" group from the group list$/ do |selector|
  page.execute_script %Q{$('#allgrouplist .radio-row label:contains("#{selector}")').parent().find('input').click();}
end

# Subject + Delevery time setup
Given /^I fillup the subject: "([^"]*)" and select Delivery time: "([^"]*)"$/ do |subject, time|
  find('.autoResponderSubject').set "#{subject}"
  sleep 2
  page.find('.emailSendOption').click
  find('.easy-list-item', :text => time, :match => :prefer_exact).click
end

# Automated Email proceed from step 2 to step 3
Given /^I click the Save button on the Delevery Setup page$/ do
  find('.save').click
end

# Folder + Autoresponder name
Given /^I create a folder named: "([^"]*)" and Autoresponder name: "([^"]*)"$/ do |folder_name, ar_name|
  find('#nameFolder').set "#{folder_name}"
  sleep 2
  page.find('#nameAutoresponder').set "#{ar_name}"
end

# Start Autoresponder button
Given /^I click on the start autoresponder button$/ do
  find('.startAutoresponderButton').click
  sleep 15
end

# Autoresponder name
Given /^I type Autoresponder name: "([^"]*)"$/ do | ar_name|
  page.find('#autoresponderName').set "#{ar_name}"
end

# Autoresponder start button
Given /^I click on the start button$/ do
  find('#autoresponderSaveButton').click
  sleep 15
end

# Subject + Date Trigger + Delevery time setup
Given /^I fillup the subject: "([^"]*)" and set Delivery time: "([^"]*)" & Date Trigger to: "([^"]*)"$/ do |subject, time, date_trigger|
  find('.autoResponderSubject').set "#{subject}"
  sleep 2
  page.find('.emailSendOption').click
  find('.easy-list-item', :text => time, :match => :prefer_exact).click
  sleep 2
  page.find('.selectedate').click
  find('.easy-list-item', :text => date_trigger, :match => :prefer_exact).click
end

# Delete the autoresponder
Given /^I click on the delete button of the autoresponder named: "([^"]*)"$/ do |ar_name|
  page.driver.browser.action.move_to(page.find('.gallery .template .templateName', :text => ar_name, :match => :prefer_exact).native).perform
  sleep 2
  find('.EmailTemplateDeleteView').click
  sleep 2
end

# Subject + Campaign + Link + Delevery time setup
Given /^I fillup the subject: "([^"]*)" and set Campaign to: "([^"]*)" Link to: "([^"]*)" & Date Trigger to: "([^"]*)"$/ do | subject, campaign, link, time|
  find('.autoResponderSubject').set "#{subject}"
  sleep 2
  page.find('.whatEmail').click
  find('.easy-list-item', :text => campaign, :match => :prefer_exact).click
  sleep 2
  page.find('.whatLink').click
  find('.easy-list-item', :text => link, :match => :prefer_exact).click
  sleep 2
  page.find('.emailSendOption').click
  find('.easy-list-item', :text => time, :match => :prefer_exact).click
end

# Autoresponder : Button Save Draft
Given /^I click on the Save Draft button$/ do
  find('.cke_button__emailarsave_label').click
  sleep 1
end

# Automated Email : Save template as draft
Given /^I save the template as a draft$/ do
  find('.buttonWrapper .save').click
end

# Delete Template
Given /^I want to delete template: "([^"]*)"$/ do |template_name|
  page.driver.browser.action.move_to(page.find('.template', :text => template_name, :match => :prefer_exact).native).perform
  sleep 5
  find('.EmailTemplateDeleteView').click
  sleep 1
  page.driver.browser.switch_to.alert.accept
end

# Autoresponder : Button Preview
Given /^I click on the Preview button$/ do
  find('.cke_button__emailpreview_icon').click
  sleep 1
end

# Autoresponder : Button Preview
Given /^I click on the Cancle button$/ do
  find('.cke_button__emailcancel_label').click
  sleep 1
end

# Autoresponder : Background color
Given /^I change the background color to Green$/ do
  find('.cke_colorpreview').click
  sleep 2
  find('.preview_input.color_input').click
  sleep 1
  page.execute_script %Q{$('.splash360colorpicker .preview_input.color_input>input').val('#00CC00');}
  sleep 3
  find('.btnOk').click
end

# Autoresponder : View Source Code Button
Given /^I click on the view source code button$/ do
  find('.cke_button__source_icon').click
end

# Create Automated Email
Given /^I want to create Automated Email$/ do
  page.find('.rightpanel-item', :text => 'Automated Emails' , :match => :prefer_exact).click
  sleep 2
  find('#arAddAnother').click
end

# Activate Automated email edit dropdown and select option
Given /^I clicked on the edit button of folder : "([^"]*)" and select option: "([^"]*)"$/ do |folder_name,option|
  page.driver.browser.action.move_to(page.find('.rightpanel-item', :text => folder_name , :match => :prefer_exact).native).perform
  sleep 2
  find('.editAutoresponder').click
  sleep 2
  page.execute_script %Q{$('.dropText:contains("#{option}")').click();}
end

# Rename folder
Given /^I rename the folder name to : "([^"]*)"$/ do |option|
  find('input[id^=autoresponder-folder-text-]').set "#{option}"
  sleep 2
  find('input[id^=autoresponder-folder-text-]').native.send_key("\n")
end

# Select Autoresponder folder
Given /^I clicked on the folder : "([^"]*)"$/ do |folder_name|
  find('.rightpanel-item', :text => folder_name , :match => :prefer_exact).click
end

# Add Another (Green Button)
Given /^I clicked on the Add Another Green button$/ do
  find('#arAddAnother').click
end

# Autoresponder Cancel button
Given /^I clicked on the autoresponder cancel button$/ do
  find('#ardone').click
end
require_relative '../../lib/cucumber_helper'

#Note:
# 1. Prerequisites -> Quality Test Splash and Nick Adams or any Fan Page or Profile Account name must be added within script.
# 2. Session clear is important after each scenario
# 3. After the value placed into input field social_txt -- wait time is important, very quick click functions kept the server busy and fail to execute.
# 4. Clean Browser Session & Close all browser windows (including popup) is mandatory.
# 5. Never touch or switch the LCP or FFP switch manually where as the test will be executed automatically.

#Twitter Limits: https://support.twitter.com/articles/15364 => 1,000
#Linkedin Limits: https://developer.linkedin.com/docs/company-pages => Per individual user = 500, & Per individual developer = 500
#Facebook Limits:

Given /^I click on social tab$/ do
  find(:xpath, ".//*[@id='social']").click
end

#For Scenario Outliners:
Given /^I select "([^"]*)" -- for social post$/ do |social_network|
  find(social_network).click
end

#select/unselect any social networks

Given /^I select facebook for social post$/ do
  expect(page).to have_selector('#newMessageFacebook_btn')
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
end

Given /^I Unselect facebook$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
end

Given /^I select linkedin for social post$/ do
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
end

Given /^I Unselect linkedin$/ do
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
end

Given /^I select twitter for social post$/ do
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
end

Given /^I Unselect twitter for social post$/ do
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
end

Given /^I Unselect Facebook & Twitter & Linkedin$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
end

Given /^I Unselect Facebook & Twitter$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
end

Given /^I Unselect Facebook & Linkedin$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
end

Given /^I Unselect Twitter & Linkedin$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
end

#ends here

#Buttons Post Now/Save Now selectors
Then /^I click Post Now button$/ do
  find('#SocailMessagePost_btn').click
end

Then /^I click Save Post button$/ do
  find(:xpath, ".//*[@id='SocailScheduledMessagePost_btn']").click
end

#ends here

Given /^I click to delete schedule post$/ do
  find(:xpath, ".//*[@id='socialTimeline']/div[1]/div[2]/a").click
end

Given /^I select all social network for post$/ do
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
end

#switch_between social accounts

And /^I switch to Facebook Fan Page$/ do
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find('.profileName', :text => 'Quality Test Splash').click #Place custom available title.
end

And /^I switch to Linkedin Company Page$/ do
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find('.profileName', :text => 'Quality Test Splash').click #Place custom available title.
end

And /^I switch to Facebook Profile$/ do
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find('.profileName', :text => 'Nick Adams').click #Place custom available title.
end

And /^I switch to Linkedin Profile$/ do
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find('.profileName', :text => 'Rick Dan').click #Place custom available title.
end

And /^I switch to Facebook Fan Page & Linkedin Company Page$/ do
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find('.profileName', :text => 'Quality Test Splash').click #Place custom available title.
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find('.profileName', :text => 'Rick Dan').click #Place custom available title.
end

And /^I switch to Facebook Profile & Linkedin Profile$/ do
  find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
  find('.profileName', :text => 'Nick Adams').click #Place custom available title.
  find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
  find('.profileName', :text => 'Rick Dan').click #Place custom available title.
end

#TC - Structured approach from the combination test coverage documents -> http://tinyurl.com/zl89vcq

#Navigation to Social Module

Given /^Go for Social Post$/ do
  expect(page).to have_selector('#social')
  find(:xpath, ".//*[@id='social']").click
end

#Select Different Social Media Single/Combined

Given /^Select Facebook Profile and Fill "([^"]*)"$/ do |user_input|
  expect(page).to have_selector('#newMessageFacebook_btn')
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find('#social_txt').set "#{user_input}"
end

Then /^Post Now And Accept to proceed$/ do
  expect(page).to have_selector('#newMessageFacebook_btn')
  find('#SocailMessagePost_btn').click
  page.driver.browser.switch_to.alert.accept
end

Given /^Select Facebook Fan Page and Fill "([^"]*)"$/ do |user_input|
  expect(page).to have_selector('#newMessageFacebook_btn')
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Linkedin Profile and Fill "([^"]*)"$/ do |user_input|
  expect(page).to have_selector('#newMessageLinkedin_btn')
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Linkedin Profile Company Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Twitter and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Profile & Twitter and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Profile & Linkedin Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Profile & Linkedin Company Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Twitter & Linkedin Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Twitter & Linkedin Company Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Twitter & Facebook Fan Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Fan Page & Linkedin Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Linkedin Company Page & Facebook Fan Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Profile & Twitter & Linkedin Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

Given /^Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  sleep 0.4
  find('#social_txt').set "#{user_input}"
end

Given /^Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "([^"]*)"$/ do |user_input|
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  find('#social_txt').set "#{user_input}"
end

#NOTE: JS POPUP Handler.

And(/^Expected popup validation message should be "([^"]*)"$/) do |alert_messages|
  if page.driver.class == Capybara::Selenium::Driver
    page.driver.browser.switch_to.alert.text.should eq(alert_messages)
    page.driver.browser.switch_to.alert.accept
  elsif page.driver.class == Capybara::Webkit::Driver
    sleep 1 # prevent test from failing by waiting for popup
    page.driver.browser.confirm_messages.should eq(alert_messages)
    page.driver.browser.accept_js_confirms
  else
    raise "Unsupported driver"
  end
end


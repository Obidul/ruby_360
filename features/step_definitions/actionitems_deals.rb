require_relative '../../lib/cucumber_helper'

Given /^I want to create a meeting under Activity Today$/ do
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "Get things done Sakib"
end

#Removed required
Given /^I want to comments on deals activities of "([^"]*)" and I clicked on comment icon and fill in "([^"]*)"$/ do |user_input, user_comments|
  page.driver.browser.action.move_to(page.find('.lgText', :text => user_input, :match => :prefer_exact).native).perform
  find('.btn-comment').click
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
end

#Inline Edit
Given /^I found existing comments "([^"]*)" and clicked on comments icon and fill in "([^"]*)"$/ do |user_input, user_comments|
  page.driver.browser.action.move_to(page.find('.comment-text', :text => user_input, :match => :prefer_exact).native).perform
  find('.btn-comment').click
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
end

#Deal edit Drop-down menu
#No duplicate content should be available.
When /^I found Price: "([^"]*)" & Product&Company: "([^"]*)" & Sale Stage "([^"]*)" & then Change Sales Stage to: "([^"]*)"$/ do |money_amount, product_company, sale_stage_current, drop_down_menu|
  find('.amount', :text => money_amount, :match => :prefer_exact)and find('li.regular .activity .hotspot', :text => product_company, :match => :prefer_exact)[1]
  execute_script %Q{ _$.$('#dashboardContent .role .hotspot:contains("#{sale_stage_current}")').click() }
  find('.splash_easy_row', :text => drop_down_menu).click
end

When /^I found Price: "([^"]*)" and Product & Company: "([^"]*)" shows in Details view$/ do |money_amount, product_company|
  find('li, .regular, .amount', :text => money_amount, :match => :prefer_exact).find('.activity, .lgText', :text => product_company, :match => :prefer_exact).find('.hotspot', :text => product_company, :match => :prefer_exact).click
end

When /^For the Deal Activities of "([^"]*)" switch to Details View$/ do |user_input|
  page.driver.browser.action.move_to(page.find('.lgText', :text => user_input, :match => :prefer_exact).native).perform
  find('.lgText', :text => user_input, :match => :prefer_exact).click
  expect(page).to have_selector('#contact', wait: 1)
end

#Add quick deal all together
Given /^I Create Quick Deal By selecting Product "([^"]*)" & Price "([^"]*)" & Lead Source "([^"]*)" & Sales Stage "([^"]*)"$/ do |product, price, lead_source, sales_stage|
  find('.icon-plus').click
  find(:xpath, "//*[@placeholder = 'Product']").click
  find('.easy-list-item', :text => product).click
  find(:xpath, "//*[@placeholder = 'Price']").click
  find('.price').set "#{price}"
  find(:xpath, "//*[@placeholder = 'Lead Source']").click
  find('.easy-list-item', :text => lead_source).click
  # find(:xpath, "//*[@placeholder = 'Due Date']").click
  # find('.datePickerSkin', :text => due_date).click
  find(:xpath, "//*[@placeholder = 'Sales Stage']").click
  find('.easy-list-item', :text => sales_stage).click
end

Given /^I Update Quick Deal By selecting Product "([^"]*)" & Price "([^"]*)" & Lead Source "([^"]*)" & Sales Stage "([^"]*)"$/ do |product, price, lead_source, sales_stage|
  find('.icon-plus').click
  find(:xpath, "//*[@placeholder = 'Product']").click
  find('.easy-list-item', :text => product).click
  find(:xpath, "//*[@placeholder = 'Price']").click
  find('.price').set "#{price}"
  find(:xpath, "//*[@placeholder = 'Lead Source']").click
  find('.easy-list-item', :text => lead_source).click
  # find(:xpath, "//*[@placeholder = 'Due Date']").click
  # find('.datePickerSkin', :text => due_date).click
  find(:xpath, "//*[@placeholder = 'Sales Stage']").click
  find('.easy-list-item', :text => sales_stage).click
end

#Deal Inline Edit (Multiple Data Matching)
When /^I Update existing Deal with Price "([^"]*)" & Product & Company "([^"]*)"$/ do |price,product_company|
  find('.amount', :text => price, :match => :prefer_exact) and find('.activity', :text => product_company, :match => :prefer_exact) and find('.activity', :text => product_company, :match => :prefer_exact).click
  #expect(page).to have_selector('#contact', wait: 1)
end

#Deal stage change Verification
Given /^I should see Sales Stage has changed to "([^"]*)"$/ do |user_input|
  find('.role', :text => user_input, :match => :prefer_exact)
end

#Add Product Name
Given /^I click on the Product and select "([^"]*)"$/ do |user_input|
  find('.icon-plus').click
  find(:xpath, "//*[@placeholder = 'Product']").click
  find('.easy-list-item', :text => user_input).click
end

#Add Price
Given /^I click on the Price and type "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'Price']").click
  find('.price').set "#{user_input}"
end

#Add Lead Source
Given /^I click on the Lead Source and select "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'Lead Source']").click
  find('.easy-list-item', :text => user_input).click
end

#Add Sales Stage
Given /^I click on the Sales Stage and change to "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'Sales Stage']").click
  find('.easy-list-item', :text => user_input).click
end

#Deal Posted Verification
Given /^I should see on "([^"]*)" & "([^"]*)" & "([^"]*)" & "([^"]*)" Deal Activity has enlisted$/ do |date, money, product, sales_stage|
  find('.lgText', :text => date, :match => :prefer_exact)
  find('.lgText', :text => money, :match => :prefer_exact)
  find('.lgText', :text => product, :match => :prefer_exact)
  find('.lgText', :text => sales_stage, :match => :prefer_exact)
end

#Completed Deal Status Verification
# Given /^I should see on Date: "([^"]*)" Price: "([^"]*)" Product & Company: "([^"]*)" and Sales Stage: "([^"]*)" activity has enlisted$/ do |date, price, product_company, sales_stage|
#   find('.time', :text => date, :match => :prefer_exact) and find('.amount', :text => price, :match => :prefer_exact) and find('.activity', :text => product_company, :match => :prefer_exact)and find('.role', :text => sales_stage, :match => :prefer_exact)
# end

Given /^I should see Price: "([^"]*)" Product & Company: "([^"]*)" and Sales Stage: "([^"]*)" activity has enlisted$/ do |price, product_company, sales_stage|
  find('.amount', :text => price, :match => :prefer_exact) and find('.activity', :text => product_company, :match => :prefer_exact)and find('.role', :text => sales_stage, :match => :prefer_exact)
end

Given /^I found deal activity with Product & Company: "([^"]*)" and select Radio button$/ do |user_input_match|
  page.find('li, .regular, .activity', :text => user_input_match, :match => :prefer_exact).find('.rdoFld').find('label[for^=ckStatus_splash_]').click
end


#Match Deal and Execute rest
Given /^I found Price: "([^"]*)" and Product & Company: "([^"]*)"$/ do |money_amount, product_company|
  page.driver.browser.action.move_to(page.find('li, .regular, .amount', :text => money_amount, :match => :prefer_exact).find('.activity, .lgText', :text => product_company, :match => :prefer_exact).native).perform
end

#Drop-down selection
When /^I select "([^"]*)" from Drop-down menu$/ do |drop_down_menu|
  find('.icon-edit').click
  find('.splash_easy_row', :text => drop_down_menu).click
end

#Comments
Given /^I want to comments on deals activities of "([^"]*)"$/ do |user_comments|
  find('.btn-comment').click
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
end

Given /^I comments on deals and fill in "([^"]*)" -- From Dropdown menu$/ do |user_comments|
  find(:xpath, "//*[@placeholder = 'Add comment']").set "#{user_comments}"
  sleep 0.2
  find('.red').click
end

Given /^I should be in Details View$/ do
  expect(page).to have_selector('.quickProfileWrapper', wait: 1)
end

#Deals Dropdown menu
Given /^I found existing Deals Price: "([^"]*)" Product & Company: "([^"]*)" and Sales Stage: "([^"]*)" and select "([^"]*)" from drop-down menu -- In Action Stream$/ do |price, product_company, sales_stage, drop_down_menu|
  find('.amount', :text => price, :match => :prefer_exact) and find('.activity', :text => product_company, :match => :prefer_exact)and find('.role', :text => sales_stage, :match => :prefer_exact).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :text => drop_down_menu).click
end

#Radio button for Deals complete
Given /^I found existing Deals : "([^"]*)" and select Radio button$/ do |user_input_match|
  page.find('li, .activity', :text => user_input_match, :match => :prefer_exact).find('.rdoFld').find('label[for^=ckStatus_splash_]').click
end
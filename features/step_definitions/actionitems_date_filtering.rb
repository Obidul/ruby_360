require_relative '../../lib/cucumber_helper'

#Custom Date - 27,28,29 eg selection#

Given /^Filtered by Date - Today within Calender - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end


Given /^Filtered by Date - Tomorrow within Calender - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end


Given /^Filtered by Date - Yesterday within Calender - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

#Custom date selection ended

#Caldner Popup to select - Start#

Given /^Filtered by Today - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

Given /^Filtered by Yesterday - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

Given /^Filtered by Tomorrow - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

Given /^Filtered by Week - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

Given /^Filtered by Month - Number matched$/ do
  page.execute_script %Q{ parseInt ($('#dashboardContent .count').html()) == $("#dashboardContent .time .lgText").length
    }
end

#Caldner Popup to select - End#

#For All Selection -- Start#

Given /^Filtered by All - Number matched -- While Today selected$/ do
  page.execute_script %Q{
          $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_all_activity"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Starred selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="starred"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Overdue selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="overdue"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While All Activity selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_all_activity"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Meeting selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_meetings"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Calls selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_calls"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Tasks selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_tasks"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Todo selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="activity_to_do"] .counter-section .counter').html())
    }
end

        # Status -- Start
Given /^Filtered by All - Number matched -- While New Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_new"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Attempted Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_attempted"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Contacted Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_contacted"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Hot Prospect Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_hot_prospect"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Client Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_client"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Disqualified Status selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="idstatus_disqualified"] .counter-section .counter').html())
    }
end
        # Status -- End

      #Deals -- Start

Given /^Filtered by All - Number matched -- While All Deals selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_all_deals"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Prospect selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_prospect"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Needs Analysis selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_needs_analysis"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Proposal selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_proposal"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Negotiations selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_negotiations"] .counter-section .counter').html())
    }
end

Given /^Filtered by All - Number matched -- While Order&Contract selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="deal_order_contract"] .counter-section .counter').html())
    }
end
      #Deals -- End

      #Team - Start

Given /^Filtered by All - Number matched -- While Me selected$/ do
  page.execute_script %Q{
         $("#dashboardContent .time .lgText").length == parseInt ($('a[data-navigation="account_razorfish"] .counter-section .counter').html())
    }
end

      #Team - end

#For All Selection -- End#

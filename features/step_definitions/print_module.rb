require_relative '../../lib/cucumber_helper'

# Email/Print Right Panel Selection
Given /^I clicked on "([^"]*)"$/ do |selector_text|
  find('.rightpanel-item', :text => selector_text).click
end

Given /^I click on Lets get started button$/ do
  find('.btn-started').click
end


# Print New Folder Creation
Given /^I want to create a New folder named "([^"]*)" in Print$/ do |folder_name|
  page.driver.browser.action.move_to(page.find('.rightpanel-item', :text => 'My Folders' , :match => :prefer_exact).native).perform
  sleep 2
  find('.icon-add-folder').click
  page.execute_script %Q{$ ('input[type=text]').val("#{folder_name}") }
  find('.newFolderAddFiled').native.send_key("\n")
end

# Print Category Selection
Given /^I click on the "([^"]*)" category$/ do |category|
  page.find('.mainGallery, .info, .title', :text => category, :match => :prefer_exact).find('.img-wrapper, .overlay, .zoom').click
end

# Print Template Selection
Given /^I select the "([^"]*)" template$/ do |template_name|
  page.find('.template, .templateName', :text => template_name, :match => :prefer_exact).find('.thumbnailWrapper').click
end

#print order processing --- Start
# Step 2 [Design]
Given /^I complete the check box and continue to next step$/ do
  find('.grpCheckBox').click
  sleep 3
  find('.continuebutton').click
end

# Step 3 [Order]
Given /^I select the quantity to "([^"]*)"$/ do |quantity|
  find('.easy-list-input-wrapper').click
  sleep 3
  find('.easy-list-item', :text => quantity, :match => :prefer_exact).click
  sleep 2
end

# Step 4 [Cart]
Given /^I checkout the order$/ do
  find('#btnPrintCheckout').click
end

# Step 5 [Checkout]
Given /^I fill up the Card Number "([^"]*)" and CVV number "([^"]*)"$/ do |card_number, cvv|
  find('#CreditCardNumber').set "#{card_number}"
  sleep 2
  page.find('#Month').click
  page.execute_script %Q{$('.easy-list-item:last').click();}
  sleep 2
  page.find('#Year').click
  page.execute_script %Q{$('.easy-list-item:last').click();}
  sleep 2
  find('#SecurityCode').set "#{cvv}"
end

# PrintCompleteOrder
Given /^I complete the Print order$/ do
  find('#btnPrintCompleteOrder').click
end

# Add another print
Given /^I add another print order$/ do
  find(:xpath, ".//*[@id='AmountCount']/div[2]/div[1]/input").click
end

# Select Send to list option
Given /^I checked the Send to list option$/ do
  find('.fRight .jquery-safari-radiobox').click
end

# Select deleviry date
Given /^I select the first available mailing date$/ do
  find('.orderDelivaryDate').click
  find('.datePickerSkin.calendarDate.active').click
end

#Step 3 for step 4
Given /^I continue to step 3 to step 4$/ do
  find('#btnAddtoCart').click
end

# Leads category wise contact number
Given /^I select type "([^"]*)" and number of contacts "([^"]*)"$/ do |category, contacts|
  if page.has_text?(:visible,category)
    if (category) === "Individuals"
      find('#mlc-ind').click
      sleep 2
      find('#ndesiredrecords').set "#{contacts}"
      sleep 2
      find('#btn-buy').click
    else
      find('#mlc-business').click
      sleep 2
      find('#ndesiredrecords').set "#{contacts}"
      sleep 2
      find('#btn-buy').click
    end
  end
end

# List Purchase Confirm
Given /^I Confirm the purchase$/ do
  find('.x-btn-mc', :text => 'I Agree', :match => :prefer_exact).click
end

#print order processing --- End

# Select Sub Category
Given /^I select the sub category "([^"]*)"$/ do |sub_category|
  find('.subCategory', :text => sub_category).click
end

# Right panel dropdown activation
Given /^I click on the right panel Dropdown Icon$/ do
  find('.print_editor_right_top .emailPrintRightHeader .headerIcon').click
end

# Select option from Right panel dropdown
Given /^I clicked on "([^"]*)" from the right panel Dropdown$/ do |name_option|
  find('.splash_panel_listitem', :text => name_option).click
end

#------------ Import --------------

# Import move from step 1 to step 2
Given /^Import process move from step 1 to step 2$/ do
  find('.saveButton').click
end

# Import move from step 2 to step 3
Given /^Import process move from step 2 to step 3$/ do
  find('.saveButton').click
end

# Import Final Step
Given /^Import process final step -> Import Now$/ do
  page.find('.rightButons .saveButton',:text => 'Import Now').click
end

# Delete Folder from My Folder
Given /^I want to delete folder "([^"]*)"$/ do |folder_name|
  page.driver.browser.action.move_to(page.find('.myFolder', :text => folder_name, :match => :prefer_exact).native).perform
  sleep 5
  find('.EmailTemplateDeleteView.printMyFolderDelete').click
end

# Change mouse/cursor position
Given /^Change mouse cursor position$/ do
  page.driver.browser.action.move_to(page.find('.leadspleaselogo').native).perform
  sleep 2
end
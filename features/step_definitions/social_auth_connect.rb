require_relative '../../lib/cucumber_helper'

# authorize social accounts#
Given /^I want to enable twitter social account$/ do
  find(:xpath, ".//*[@id='btnTwitterAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='username_or_email']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='password']").set "test123456789"
  find(:xpath, ".//*[@id='allow']").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
end

Given /^I want to enable facebook social account$/ do
  find(:xpath, ".//*[@id='btnFacebookAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='email']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='pass']").set "test123456789"
  find(:xpath, ".//*[@id='u_0_2']").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
  find('.profileName', :text => 'Nick Adams').click
end

Given /^I want to enable linkedin social account$/ do
  find(:xpath, ".//*[@id='btnLinkedinAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='session_key-oauthAuthorizeForm']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='session_password-oauthAuthorizeForm']").set "BS123456"
  find(".allow").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
end

Given /^Enable all social network$/ do
  #Twitter Active
  find(:xpath, ".//*[@id='btnTwitterAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='username_or_email']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='password']").set "test123456789"
  find(:xpath, ".//*[@id='allow']").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
  find(:xpath, ".//*[@id='newMessageTwitter_btn']").click
  sleep 0.2

  #Facebook Active
  find(:xpath, ".//*[@id='btnFacebookAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='email']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='pass']").set "test123456789"
  find(:xpath, ".//*[@id='u_0_2']").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
  find('.profileName', :text => 'Nick Adams').click
  find(:xpath, ".//*[@id='newMessageFacebook_btn']").click
  sleep 0.2

  #Linkedin Active
  find(:xpath, ".//*[@id='btnLinkedinAuthorize']").click
  last_popup = page.driver.browser.window_handles.last
  page.driver.browser.switch_to.window(last_popup)
  find(:xpath, ".//*[@id='session_key-oauthAuthorizeForm']").set "splash360test@gmail.com"
  find(:xpath, ".//*[@id='session_password-oauthAuthorizeForm']").set "BS123456"
  find(".allow").click
  sleep 3
  main_handle = page.driver.browser.window_handles.first
  page.driver.browser.switch_to.window(main_handle)
  find(:xpath, ".//*[@id='newMessageLinkedin_btn']").click
  sleep 0.2
end

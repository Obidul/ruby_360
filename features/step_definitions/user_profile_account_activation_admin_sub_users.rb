require_relative '../../lib/cucumber_helper'

When /^Subuser account activation and login$/ do
  visit('https://mailinator.com/inbox2.jsp?public_to=abdulalim6677')
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Invitation from Splash').click
  sleep 2
  within_frame 'publicshowmaildivcontent' do
    click_on 'Please click here to set up an account.'
  end
  sleep 3
end

When /^Admin account activation and login$/ do
  visit('https://mailinator.com')
  find('#inboxfield', :text => 'email_address').click
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Welcome to Splash360').click
  sleep 2
  within_frame 'publicshowmaildivcontent' do
    click_on 'Activate my account'
  end
  sleep 3
end

Given /^Production: My email: "([^"]*)" and username: "([^"]*)" & password: "([^"]*)" and confirmation at : "([^"]*)"$/ do |email_address, username, password, mailinator_email_id|
    visit('https://www.splash360.com/Account/Register')
    # # visit('https://qa.splash360.com/Account/Register')
    # # visit('https://preprod.splash360.com/Account/Register')
    find(:xpath, "//*[@placeholder = 'email address']").set "#{email_address}"
    find(:xpath, "//*[@placeholder = 'username']").set "#{username}"
    find(:xpath, "//*[@placeholder = 'password']").set "#{password}"
    find('#createaccount').click
    sleep 5

    #another part at mailinator.com

    visit('https://mailinator.com')
    find(:xpath, "//*[@placeholder = 'Check Any Inbox!']").set "#{mailinator_email_id}"
    find('.btn-dark').click
    page.driver.browser.manage.window.maximize
    sleep 5
    find('.innermail', :text => 'Welcome to Splash360', :match => :first).click
    sleep 2
    within_frame 'publicshowmaildivcontent' do
      click_on 'Activate my account'
    end
    sleep 3
end

Given /^QA: My email: "([^"]*)" and username: "([^"]*)" & password: "([^"]*)" and confirmation at : "([^"]*)"$/ do |email_address, username, password, mailinator_email_id|
  # visit('https://www.splash360.com/Account/Register')
  visit('https://qa.splash360.com/Account/Register')
  # visit('https://preprod.splash360.com/Account/Register')
  find(:xpath, "//*[@placeholder = 'email address']").set "#{email_address}"
  find(:xpath, "//*[@placeholder = 'username']").set "#{username}"
  find(:xpath, "//*[@placeholder = 'password']").set "#{password}"
  find('#createaccount').click
  sleep 5

  #another part at mailinator.com

  visit('https://mailinator.com')
  find(:xpath, "//*[@placeholder = 'Check Any Inbox!']").set "#{mailinator_email_id}"
  find('.btn-dark').click
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Welcome to Splash360', :match => :first).click
  sleep 2
  within_frame 'publicshowmaildivcontent' do
    click_on 'Activate my account'
  end
  sleep 3
end

Given /^Preprod: My email: "([^"]*)" and username: "([^"]*)" & password: "([^"]*)" and confirmation at : "([^"]*)"$/ do |email_address, username, password, mailinator_email_id|
  # visit('https://www.splash360.com/Account/Register')
  # visit('https://qa.splash360.com/Account/Register')
  visit('https://preprod.splash360.com/Account/Register')
  find(:xpath, "//*[@placeholder = 'email address']").set "#{email_address}"
  find(:xpath, "//*[@placeholder = 'username']").set "#{username}"
  find(:xpath, "//*[@placeholder = 'password']").set "#{password}"
  find('#createaccount').click
  sleep 5

  #another part at mailinator.com

  visit('https://mailinator.com')
  find(:xpath, "//*[@placeholder = 'Check Any Inbox!']").set "#{mailinator_email_id}"
  find('.btn-dark').click
  page.driver.browser.manage.window.maximize
  sleep 5
  find('.innermail', :text => 'Welcome to Splash360', :match => :first).click
  sleep 2
  within_frame 'publicshowmaildivcontent' do
    click_on 'Activate my account'
  end
  sleep 3
end

Given /^Logged me to system with new username: "([^"]*)" & password: "([^"]*)"$/ do |username_to_login, password_to_login|
    find('#username').set "#{username_to_login}"
    find('#password').set "#{password_to_login}"
    find('.btn-login').click
    sleep 2
    find('.info_close_btn').click
    find('.info_close_btn').click
    page.execute_script %Q{
    _$.subscription.loadSubscriptionPlan().click();
    }
end

Given /^I have Purchased Subscription Plan$/ do
  # find('.headerProfile').click
  # find('.splash_easy_row', :text => 'Profile').click
  # find('.profileMainMenu', :text => 'Subscription Plan').click
  find('.buyNow').click
  sleep 2
  #Purchase Account
  find('#CreditCardNumber').set "4111111111111111"
  sleep 2
  page.find('#Month').click
  find('.easy-list-item', :text => '03').click
  page.find('#Year').click
  find('.easy-list-item', :text => '2017').click
  page.find('#Csv').set "123"
  page.find('#FullName').set "Sakib Mahmud"
  page.find('#Address').set "East Land Block B"
  page.find('#Address2').set "North West"
  page.find('#City').set "Dhaka"
  page.find('#State').set "Dhaka"
  page.find('#Zip').set "32323"
  find('.btnPrintCompleteOrder').click
  sleep 5
end
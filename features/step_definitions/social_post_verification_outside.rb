require_relative '../../lib/cucumber_helper'

#stream public posted data verification
Given /^I should see "([^"]*)" on Twitter stream$/ do |user_public_data|
  find( '.tweet-text', :text => user_public_data, :match => :prefer_exact)
  # find(('.tweet-text').expect(page).to have_content, :text => user_public_data, :match => :prefer_exact)
end

Given /^I should see "([^"]*)" on Facebook Profile stream$/ do |user_public_data|
  find( '.userContent', :text => user_public_data, :match => :prefer_exact)
end

Given /^I should see "([^"]*)" on Facebook Fan Page stream$/ do |user_public_data|
  # find( '.userContent', :text => user_public_data, :match => :prefer_exact)
  expect(page).to have_content, :text => user_public_data, :match => :prefer_exact
end

Given /^I should see "([^"]*)" on Linkedin Profile stream$/ do |user_public_data|
  expect(page).to have_content, :text => user_public_data, :match => :prefer_exact
end

Given /^I should see "([^"]*)" on Linkedin Company Profile stream$/ do |user_public_data|
  find( '.text-entity', :text => user_public_data, :match => :prefer_exact)
end

#user access to different account

When /^Access Linkedin Profile - Post Verification$/ do
    visit('https://www.linkedin.com/')
    find('#login-email').set "splash360test@gmail.com"
    find('#login-password').set "BS123456"
    find(:xpath, ".//*[@id='pagekey-uno-reg-guest-home']/div[1]/div/form/input[6]").click
    visit('https://www.linkedin.com/pulse/activities/splash-team+0_38cb8_1--3HSHprpLPcY_Q?trk=nav_responsive_sub_nav_yourupdates')
end

When /^Access Twitter Account - Post Verification$/ do
  visit('https://twitter.com/bsteam2')
end

When /^Access Facebook Profile - Post Verification$/ do
  visit('https://www.facebook.com/nick.adams.96343405')
  find('#email').set "splash360test@gmail.com"
  find('#pass').set "test123456789"
  find('#loginbutton').click
end

When /^Access Facebook Fan Page - Post Verification$/ do
  visit('https://www.facebook.com/Quality-Test-Splash-1413015995695411/')
  find('#email').set "splash360test@gmail.com"
  find('#pass').set "test123456789"
  find('#loginbutton').click
end

require_relative '../../lib/cucumber_helper'

#####
###########
#Module Switching
#####
###########

Given /^Go for Addressbook$/ do
  page.execute_script %Q{ $('#addressBook').click(); }
end

Given /^I click on Contacts Tab$/ do
  find(:xpath, ".//*[@id='addressBook']").click
end

Given /^I click on Action Items Tab$/ do
  find(:xpath, ".//*[@id='dashboard']").click
end

Given /^I click on Email Tab$/ do
  find(:xpath, ".//*[@id='email']").click
end

Given /^I click on Social Tab$/ do
  find(:xpath, ".//*[@id='social']").click
end

Given /^I click on Print Tab$/ do
  find(:xpath, ".//*[@id='print']").click
end

Given /^I click on Reports Tab$/ do
  find(:xpath, ".//*[@id='campaign']").click
end

Given /^I click on Leads Tab$/ do
  find(:xpath, ".//*[@id='datawidget']").click
end

#####
###########
#Action Streams - Right Panel Navigation
#####
###########

Given /^I select Today from Primary Right Panel$/ do
  expect(page).to have_selector('.primaryActionList')
  page.find('li', :text => 'Today', :match => :prefer_exact).click
end

Given /^I select Starred from Primary Right Panel$/ do
  expect(page).to have_selector('.primaryActionList')
  page.find('.primaryActionList li a .navigation-label', :text => 'Starred', :match => :prefer_exact).click
end

Given /^I select Overdue from Primary Right Panel$/ do
  find('.primaryActionList li a .navigation-label', text: 'Overdue', :match => :prefer_exact).click
end

Given /^I select Deals -- while in Action Stream$/ do
  find('#dashboardDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('#dashboardDiv .contactActionList li a .navigation-label', text: 'All Deals', :match => :prefer_exact).click
end

Given /^I select Deals from Primary Right Panel -- while in Action Stream$/ do
  find('#dashboardDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  # find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
end

Given /^I select All Activity Under Activity Tab$/ do
  find( '#secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find( 'a .navigation-label', text: 'All Activity', :match => :prefer_exact).click
end

Given /^I select Meeting Under Activity Tab$/ do
  find('#secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Meeting', :match => :prefer_exact).click
end

Given /^I select Calls Under Activity Tab$/ do
  find('#secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Calls', :match => :prefer_exact).click
end

Given /^I select Tasks Under Activity Tab$/ do
  find('#secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Tasks', :match => :prefer_exact).click
end

Given /^I select Todo Under Activity Tab$/ do
  find('#secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'To Do', :match => :prefer_exact).click
end

Given /^I select Tags Under Tags Tab$/ do
  find('#secondaryActionListTab li', text: 'Tags', :match => :prefer_exact).click
  sleep 0.5
end

Given /^I select Filter Under Filter Tab$/ do
  find('#secondaryActionListTab li', text: 'Filter', :match => :prefer_exact).click
  sleep 0.5
end

# Status of Action Stream

Given /^Contact of "([^"]*)" Status has updated to "([^"]*)"$/ do |contact_name, status_select_drop_down|
  find(:xpath, ".//*[@id='txtSearch']").set "#{contact_name}"
  find(:xpath, ".//*[@id='txtSearch']").native.send_key("\n")
  sleep 0.5
  find('#IDLISTVALUE').click
  find('.easy-list-item', :text => status_select_drop_down).click
  find('.save').click
end

Given /^I select Status Tab - while in Action Stream module$/ do
  find('#dashboardDiv #secondaryActionListTab li', text: 'Status', :match => :prefer_exact).click
  sleep 0.5
end

Given /^I select New from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'New', :match => :prefer_exact).click
end

Given /^I select Attempted from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Attempted', :match => :prefer_exact).click
end

Given /^I select Contacted from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Contacted', :match => :prefer_exact).click
end

Given /^I select Hot Prospect from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Hot Prospect', :match => :prefer_exact).click
end

Given /^I select Client from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Client', :match => :prefer_exact).click
end

Given /^I select Disqualified from Status - while in Action Stream module$/ do
  find('#dashboardDiv .secondaryActionList li', text: 'Status', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Disqualified', :match => :prefer_exact).click
end

#
# Given /^I select "([^"]*)" Under Status Menu$/ do |user_selected_status|
#   find('.idstatus-list-button').click
#   find('.idstatus-list').find('li', :text => user_selected_status, :match => :prefer_exact).click
# end

Given /^I select Team$/ do
  find('.account-list-button').click
end

Given /^I select "([^"]*)" Under Team Menu$/ do |user_selected_team|
  find('.account-list-button').click
  find('.account-list, li .navigation-label', :text => user_selected_team, :match => :prefer_exact).click
end


# Team

Given /^I select Team from Primary Right Panel -- while in Action Stream$/ do
  find('#dashboardDiv .contactActionList li a', text: 'Team', :match => :prefer_exact).click
  sleep 0.2
end

Given /^I select Me from Team menu -- while in Action Stream$/ do
  find('#dashboardDiv .contactActionList li a', text: 'Team', :match => :prefer_exact).click
  sleep 0.5
  find('a .navigation-label', :text => /\AMe\z/, exact: true).click
end

#####
###########
#Action Stream Right Panel - Deals
#####
###########

Given /^I select All Deals Under Deals options -- while in Action Stream Module$/ do #test
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'All Deals', :match => :prefer_exact).click
end

Given /^I select Prospect Under Deals options -- while in Action Stream Module$/ do
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Prospect', :match => :prefer_exact).click
end

Given /^I select Needs Analysis Under Deals options -- while in Action Stream Module$/ do
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Needs Analysis', :match => :prefer_exact).click
end

Given /^I select Proposal Under Deals options -- while in Action Stream Module$/ do
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Proposal', :match => :prefer_exact).click
end

Given /^I select Negotiations Under Deals options -- while in Action Stream Module$/ do
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Negotiations', :match => :prefer_exact).click
end

Given /^I select Order&Contact Under Deals options -- while in Action Stream Module$/ do
  find('#dashboardContent .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  sleep 0.2
  find('a .navigation-label', text: 'Order/Contract', :match => :prefer_exact).click
end

#####
###########
# Addressbook - Details View Left Tab Navigation
#####
###########

Given /^I select Activity Tab from Left Panel$/ do
  find('.customeTabs', :text => 'Activity').click
end

Given /^I select Deals Tab from Left Panel$/ do
  find('.customeTabs', :text => 'Deals').click
end

Given /^I select Emails Tab from Left Panel$/ do
  find('.customeTabs', :text => 'Emails').click
end

#####
###########
# #Addressbook - Right Panel Navigation
#####
###########

#Primary List Action Tab

Given /^I select All Contacts -- while in Addressbook$/ do
  find('#addressBookDiv .primaryActionList li a', text: 'All Contacts', :match => :prefer_exact).click
end

Given /^I select Starred -- while in Addressbook$/ do
  find('#addressBookDiv .primaryActionList li a', text: 'Starred', :match => :prefer_exact).click
end

Given /^I select New Leads -- while in Addressbook$/ do
  find('#addressBookDiv .primaryActionList li a', text: 'New Leads', :match => :prefer_exact).click
end

#Seconary List Action Tab

Given /^I select All Activity Today from Activity Tab -- while in Addressbook$/ do
  find('#addressBookDiv #secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'All Activity Today', :match => :prefer_exact).click
end

Given /^I select Meetings Today from Activity Tab -- while in Addressbook$/ do
  find( '#addressBookDiv #secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find( 'a .navigation-label', text: 'Meetings Today', :match => :prefer_exact).click
end

Given /^I select Calls Today from Activity Tab -- while in Addressbook$/ do
  find('#addressBookDiv #secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Calls Today', :match => :prefer_exact).click
end

Given /^I select Tasks Today from Activity Tab -- while in Addressbook$/ do
  find('#addressBookDiv #secondaryActionListTab li', text: 'Activity', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Tasks Today', :match => :prefer_exact).click
end

#Subscriptions
Given /^I select Active Subscribers from Subscriptions Status$/ do
  find( '#addressBookDiv .contactActionList li a', text: 'Subscription Status', :match => :prefer_exact).click
  find( 'a .navigation-label', text: 'Active Subscribers', :match => :prefer_exact).click
end

Given /^I select Inactive Subscribers from Subscriptions Status$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Subscription Status', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Inactive Subscribers', :match => :prefer_exact).click
end

#Addressbook - Deals - Right Panel

Given /^I select Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
end

Given /^I select All Deals from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'All Deals', :match => :prefer_exact).click
end

Given /^I select Prospect from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Prospect', :match => :prefer_exact).click
end

Given /^I select Need Analysis from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Needs Analysis', :match => :prefer_exact).click
end

Given /^I select Proposal from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Proposal', :match => :prefer_exact).click
end

Given /^I select Negotiations from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Negotiations', :match => :prefer_exact).click
end

Given /^I select Order&Contract from Deals -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Deals', :match => :prefer_exact).click
  # find('#addressBookDiv #secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('a .navigation-label', text: 'Order/Contract', :match => :prefer_exact).click
end

# Status of Addressbook

Given /^I select Status Tab - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv #secondaryActionListTab li', :text => /\AStatus\z/, exact: true).click
  sleep 0.2
end

Given /^I select New from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  # find('#addressBookDiv li a .navigation-label', text: 'New', :match => :prefer_exact).click #workable but conflicts with New Leads and New Status
  find('#addressBookDiv li a .navigation-label', :text => /\ANew\z/, exact: true).click
end

Given /^I select Attempted from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  find('#addressBookDiv li a .navigation-label', text: 'Attempted', :match => :prefer_exact).click
end

Given /^I select Contacted from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  find('#addressBookDiv li a .navigation-label', text: 'Contacted', :match => :prefer_exact).click
end

Given /^I select Hot Prospect from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  find('#addressBookDiv li a .navigation-label', text: 'Hot Prospect', :match => :prefer_exact).click
end

Given /^I select Client from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  find('#addressBookDiv li a .navigation-label', text: 'Client', :match => :prefer_exact).click
end

Given /^I select Disqualified from Status - while in Addressbook module$/ do
  # find('#addressBookDiv .contactActionList li a', :text => /\AStatus\z/, exact: true).click
  find('#addressBookDiv .secondaryActionList li', :text => 'Status', exact: true).click
  sleep 0.2
  find('#addressBookDiv li a .navigation-label', text: 'Disqualified', :match => :prefer_exact).click
end

# Team Start

Given /^I select Team from Primary Right Panel -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Team', :match => :prefer_exact).click
  sleep 0.2
end

Given /^I select Me from Team menu -- while in Addressbook$/ do
  find('#addressBookDiv .contactActionList li a', text: 'Team', :match => :prefer_exact).click
  sleep 0.5
  find('a .navigation-label', :text => /\AMe\z/, exact: true).click
end

  ## Sub User
Given /^I select sub-user of "([^"]*)" from Team Menu -- while in Addressbook$/ do |select_sub_user|
  find('#addressBookDiv .contactActionList li a', text: 'Team', :match => :prefer_exact).click
  sleep 0.5
  find('.a .navigation-label', :text => select_sub_user, :match => :prefer_exact).click
end


# Team End

#####
###########
#Next & Previous Contact Switching
#####
###########

Given /^I move to next contact address$/ do
  find('#nextLink').click
end

Given /^I move to previous contact address$/ do
  find('#nextLink').click
end

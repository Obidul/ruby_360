require_relative '../../lib/cucumber_helper'

#Business rules:
#1. Contact Name and Company Name is empty -- Although if anyone search, that should be load on screen if any match data found.
#2. Quick search conducts on Quick Profile data

Given /^Go for Contact Module$/ do
  expect(page).to have_selector('#addressBook', wait: 10)
  find(:xpath, ".//*[@id='addressBook']").click
end

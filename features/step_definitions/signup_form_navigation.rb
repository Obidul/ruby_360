require_relative '../../lib/cucumber_helper'

# Select a category of the form
Given /^I select the "([^"]*)" category$/ do |category|
  page.find('.container .boxes h6', :text => category, :match => :prefer_exact).click
end

# Page verification
Given /^I should see "([^"]*)" on the screen$/ do |text|
  page.find('.activity-header', :text => text, :match => :prefer_exact)
end

# Select the form template
Given /^I select the "([^"]*)" form template$/ do |template|
  page.find('.container li figcaption', :text => template, :match => :prefer_exact).click
end

# Signup form next button
Given /^I click on the signup form Next button$/ do
  find('.btn-next').click
end

# Signup form save button
Given /^I click on the signup form Save button$/ do
  find('#webform-save').click
end

# Folder + Form name
Given /^I create a Name Folder: "([^"]*)" and Name Webform: "([^"]*)"$/ do |folder_name, form_name|
  find('#nofolder-popup-folder-name').set "#{folder_name}"
  sleep 2
  page.find('#nofolder-popup-form-name').set "#{form_name}"
end

# Start Webform button
Given /^I click on the Start Webform button$/ do
  find('.startAutoresponderButton').click
  sleep 15
end

# Click on the Cross button
Given /^I click on the cross button$/ do
  find('.activity-header .icon-cross').click
end

# Expand the folder
Given /^I expand the folder "([^"]*)" from the folder view page$/ do |folder_name|
  page.find('.folder-name a span', :text => folder_name, :match => :prefer_exact).click
end

# Select From and click on the form edit button
Given /^I click on the Edit button of the form name "([^"]*)"$/ do |form_name|
  page.execute_script %Q{$('li h3:contains("#{form_name}")').parent().find('.btn-editText').click();}
end

# Select the menu from dropdown
Given /^I select "([^"]*)" from the dropdown$/ do |drop_down_menu|
  find('.splash_easy_row', :text => drop_down_menu).click
end

# Find the folder and click on the edit dropdown
Given /^I found folder: "([^"]*)" on Grid list and I clicked the edit dropdown$/ do |user_input|
  page.driver.browser.action.move_to(page.find('.folder-name', :text => user_input, :match => :prefer_exact).native).perform
  find('.icon-edit').click
end

# Click on the red cross button
Given /^I click on the Red Cross button on the Signup Form page$/ do
  find('.signup-forms .btn-addIcon').click
end

# Click on the Done button
Given /^I click on the Done button on the Signup Form page$/ do
  find('.btn-text').click
end

# Normal save in the form
Given /^I create Name Webform: "([^"]*)" and click on the start button$/ do |form_name|
  page.find('#form-name').set "#{form_name}"
  sleep 1
  find('#webform-save-button').click
end

# Normal write form name on the save popup
Given /^I enter a new Webform Name: "([^"]*)"$/ do |form_name|
  page.find('#form-name').set "#{form_name}"
end

# Inline click
Given /^I click on the form name: "([^"]*)"$/ do |form_name|
  page.find('li h3', :text => form_name, :match => :prefer_exact).click
end

# Click on the thumbnail
Given /^I click on the thumbnail of the form name: "([^"]*)"$/ do |form_name|
  page.execute_script %Q{$('li h3:contains("#{form_name}")').parent().find('.thumb').click();}
end

# Form status changed from Active to Paused
Given /^I should see the status changed from Active to Paused$/ do
  page.find('.status', :text => 'Paused', :match => :prefer_exact)
end

# Form status changed from Paused to Active
Given /^I should see the status changed from Paused to Active$/ do
  page.find('.status', :text => 'Show Preview', :match => :prefer_exact)
end

#Create a form quickly
Given /^I quickly create a Form$/ do
  find('.signup-forms .btn-addIcon').click
  page.find('.container .boxes h6', :text => 'Popup', :match => :prefer_exact).click
  page.find('.container li figcaption', :text => '1 Popup From', :match => :prefer_exact).click
  find('.btn-next').click
  find('.btn-next').click
  find('#webform-save').click
  page.find('#form-name').set "Sample Form"
  sleep 1
  find('#webform-save-button').click
  find('.activity-header .icon-cross').click
  sleep 1
end

# Create a new folder
Given /^I create a New Folder : "([^"]*)"$/ do |folder_name|
  page.find('.folder-name .rename-folder').set "#{folder_name}"
  page.find('.activity-header').click
end

# Rename a new folder
Given /^I rename a New Folder to : "([^"]*)"$/ do |folder_name|
  page.find('.folder-name .rename-folder').set "#{folder_name}"
  page.find('.activity-header').click
end

#Delete the renamed folder quickly
Given /^I delete the renamed folder$/ do
  page.driver.browser.action.move_to(page.find('.folder-name', :text => "My New Folder renamed", :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => "Delete Folder").click
  page.driver.browser.switch_to.alert.accept
  sleep 3
end

#Create a folder quickly
Given /^I quickly create a Folder$/ do
  page.driver.browser.action.move_to(page.find('.folder-name', :text => "Test", :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => "New Folder").click
  page.find('.folder-name .rename-folder').set "Folder for Delete"
  page.find('.activity-header').click
end

# Button Save Draft
Given /^I click on the Save Draft button on the signup$/ do
  find('.cke_button__signupsavedraft_label').click
  sleep 1
end

# Button Preview
Given /^I click on the Preview button on the signup$/ do
  find('.cke_button__signuppreview_icon').click
end

# Button Cancel
Given /^I click on the Cancel button on the signup$/ do
  find('.cke_button__signupcancel_label').click
end

# Button Preview
Given /^I click on the Source button on the signup$/ do
  find('.cke_button__source_icon').click
end

# Button Mobile Preview
Given /^I select the Mobile view button$/ do
  find('.icon-mobile').click
end

# Button Desktop Preview
Given /^I select the Desktop view button$/ do
  find('.icon-tablet').click
end

# Button Cross from Preview
Given /^I click on the Cross button of the preview page$/ do
  find('.icon-remove').click
end

# Save the form from editor panel
Given /^I save the form as a draft named : "([^"]*)"$/ do |form_name|
  page.find('#form-name').set "#{form_name}"
  sleep 1
  find('#webform-save-button').click
end

# Button Cancel: Save popup
Given /^I click on the Cancel button of the save popup$/ do
  find('.btn-default').click
end

# Button Start : Save popup
Given /^I click on the Start button of the save popup$/ do
  find('#webform-save-button').click
end

# Button Cancel
Given /^I click on the Cancel button of the signup$/ do
  find('.btn-cancel').click
end

# Select checkbox and change the data
Given /^I select the checkbox for : "([^"]*)" and change the data to : "([^"]*)"$/ do |conditions , data|
  page.execute_script %Q{$('.valueSettings li p:contains("#{conditions}")').parent().find('.rdo-button label').click();}
  page.execute_script %Q{$('.valueSettings li p:contains("#{conditions}")').parent().find('input').val("#{data}");}
end

# Select only checkbox
Given /^I select the checkbox for : "([^"]*)"$/ do |conditions|
  page.execute_script %Q{$('.valueSettings li p:contains("#{conditions}")').parent().find('.rdo-button label').click();}
end

# Change only data
Given /^I change the data to : "([^"]*)" for : "([^"]*)"$/ do |data , conditions|
  page.execute_script %Q{$('.valueSettings li p:contains("#{conditions}")').parent().find('input').val("#{data}");}
end

# Change only data for frequency
Given /^I change the value to : "([^"]*)" for Frequency$/ do |data|
  page.find('.day-value').set "#{data}"
end

# Change only data type for frequency
Given /^I change the type value to : "([^"]*)" for Frequency$/ do |data|
  page.find('#frequency-type').click
  page.find('.easy-list-item', :text => data , :match => :prefer_exact).click
end

# Select the radio button for frequency
Given /^I select the radio button of : "([^"]*)" for Page Visibility$/ do |conditions|
  page.find('.page-visibility li label', :text => conditions , :match => :prefer_exact).click
end

# Include URLs
Given /URLs:/ do |text|
  page.find('.hidden-content textarea').set "#{text}"
end

# Select the check box for frequency
Given /^I select the check box of : "([^"]*)" for Page Visibility$/ do |conditions|
  page.find('.checkbox-settings li label', :text => conditions , :match => :prefer_exact).click
end

# Select Group from the Group List
Given /^I select "([^"]*)" group from the group list of singup form$/ do |selector|
  page.execute_script %Q{$('.group-list li span:contains("#{selector}")').parent().find('label').click();}
end

# Goto the Signup form and Delete the form
Given /^Goto the Signup form folder view and Delete the form$/ do
  find('.activity-header .icon-cross').click
  page.execute_script %Q{$('li h3:contains("My Third Form")').parent().find('.btn-editText').click();}
  sleep 1
  find('.splash_easy_row', :text => 'Delete').click
  page.driver.browser.switch_to.alert.accept
  sleep 2
end

# Click on the New folder icon
Given /^I click on the New Folder Icon$/ do
  find('.newFolderIcon').click
end

# Enter new folder name
Given /^I enter a new folder name: "([^"]*)"$/ do |folder_name|
  find('#new-folder-name').set "#{folder_name}"
end

# New folder popup: OK button
Given /^I click on the OK button of the new folder popup$/ do
  find('.ok').click
end

# New folder popup: Cancel button
Given /^I select on the Cancel button of the new folder popup$/ do
  find('.btn-default').click
end

# Create a new folder
Given /^I enter a folder name : "([^"]*)" and press Ok button$/ do |folder_name|
  find('#new-folder-name').set "#{folder_name}"
  sleep 1
  find('.ok').click
end

# Goto the Signup form and Delete the folder
Given /^Goto the Signup form folder view and Delete the folder$/ do
  find('.btn-default').click
  find('.btn-cancel').click
  find('.rightpanel-item', :text => 'Signup Forms').click
  sleep 5
  page.driver.browser.action.move_to(page.find('.folder-name', :text => 'Temp Folder', :match => :prefer_exact).native).perform
  find('.icon-edit').click
  find('.splash_easy_row', :text => 'Delete Folder').click
  page.driver.browser.switch_to.alert.accept
  sleep 2
end

# Select the folder name from dropdown
Given /^I select the folder : "([^"]*)" from the dropdown$/ do |folder_name|
  page.find('#webform-folder').click
  page.find('.easy-list-item', :text => folder_name , :match => :prefer_exact).click
end

# Delete the created form
Given /^Now goto the Signup form folder view and Delete the form$/ do
  page.execute_script %Q{$('li h3:contains("My Third Form")').parent().find('.btn-editText').click();}
  find('.splash_easy_row', :text => 'Delete').click
  page.driver.browser.switch_to.alert.accept
  sleep 2
end

# Get code page: Link click
Given /^I click on the link : "([^"]*)"$/ do |link|
  page.find('.get-code p a', :text => link , :match => :prefer_exact).click
end

# Popup Validation
Given /^I should see popup message : "([^"]*)" on the screen$/ do |message|
  text = page.driver.browser.switch_to.alert.text
  expect(text).to eq message
end

# Draft status check
Given /^I should see the status changed to Draft Version$/ do
  page.find('.status', :text => 'Draft Version', :match => :prefer_exact)
end

# Page should have selector
Given /^The page should have selector: "([^"]*)"$/ do |selector|
  expect(page).to have_selector(selector, wait: 2)
end

#Write some text in the block
Given /^I delete a block$/ do
  within_frame(find('.cke_wysiwyg_frame')) do
    # page.driver.browser.action.move_to(page.find('div data-block-name="webform-heading"').native).perform
    page.driver.browser.action.move_to(page.find('tbody tr data-ui-area').native).perform
    find('.remove').click
  end
end
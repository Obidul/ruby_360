require_relative '../../lib/cucumber_helper'

Given /^Create Smart Group From Addressbook$/ do
  expect(page).to have_selector('#addressBook', wait: 10)
  find(:xpath, ".//*[@id='addressBook']").click
  find('#secondaryActionListTab li', text: 'Filter', :match => :prefer_exact).click
  sleep 0.5
  page.execute_script %Q{ $('li:first-child .btn-edit').css({'visibility':'visible','opacity':1}) }
  page.execute_script %Q{ _$.$('#dashboardContent #secondaryActionListGrp ul li:first-child a').hover() }
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => 'New Filter').click
end

Given /^I save Smart Group as "([^"]*)"$/ do |smart_group_save|
  find('#smartGroupName').set "#{smart_group_save}"
  find('.arButton.save').click
end
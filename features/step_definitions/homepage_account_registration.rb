require_relative '../../lib/cucumber_helper'

#FREE TRIAL button
When /^I click on the FREE TRIAL button$/ do
  find('.btn-free').click
end

#Create Account button
When /^I click on the Create Account button$/ do
  find('#createaccount').click
end

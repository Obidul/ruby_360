require_relative '../../lib/cucumber_helper'

#quick contact create start
Given /^I quick create activity with "([^"]*)" and type "([^"]*)" and searched for contact of "([^"]*)" & select "([^"]*)"$/ do |user_input_subject, activity_type, contact_find, select_contact|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input_subject}"
  page.execute_script %Q{ _$.ui.fire('clean') } # required to clean dirty state when webdriver is trying to quit the browser.
  find('.selTxt').click
  find('.easy-list-item', :text => activity_type).click
  find(:xpath, "//*[@placeholder = 'Enter company name or contact name.']").set "#{contact_find}"
  sleep 1
  page.find('li', :text => select_contact, :match => :prefer_exact).click
  find('.red').click
end
#quick contact create end#

#quick deal create start
#Deals
Given /^I quick create deals Product "([^"]*)" & Price "([^"]*)" & Lead Source "([^"]*)" & Sales Stage "([^"]*)" and searched for contact of "([^"]*)" & select "([^"]*)"$/ do |product, price, lead_source, sales_stage, contact_find, select_contact|
  find('.activity-add').click
  find(:xpath, "//*[@placeholder = 'Product']").click
  find('.easy-list-item', :text => product).click
  find(:xpath, "//*[@placeholder = 'Price']").click
  find('.price').set "#{price}"
  find(:xpath, "//*[@placeholder = 'Lead Source']").click
  find('.easy-list-item', :text => lead_source).click
  # find(:xpath, "//*[@placeholder = 'Due Date']").click
  # find('.datePickerSkin', :text => due_date).click
  find(:xpath, "//*[@placeholder = 'Sales Stage']").click
  find('.easy-list-item', :text => sales_stage).click
  #here to search
  find(:xpath, "//*[@placeholder = 'Enter company name or contact name.']").set "#{contact_find}"
  sleep 1
  page.find('li', :text => select_contact, :match => :prefer_exact).click
  find('.red').click
end
#quick deal create end

#Switch between Contact List View and Details View#
Given /^I switch to Contact List View$/ do
  find('#btnContactsListView').click
  sleep 3
end

Given /^I switch to Contact Details View$/ do
  find(:xpath, ".//*[@id='listViewBody']/div[1]/div/div[2]/div/div[1]/div[4]").click
  sleep 3
end

#Select checked and Omit checked

Given /^I omit checked$/ do
  find('#linkOmitList').click
  sleep 5
end

Given /^I select checked$/ do
  find('#linkSelectList').click
  sleep 5
end

#Counter number match

Given /^I should see in Current Lookup : "([\-\d,\.]+)"$/ do |number|
  page.find('.totalContact').should have_text number
  # expect(page).to have_selector('.totalContact', count: number)
  sleep 2
end

Given /^I should see in All Contacts : "([\-\d,\.]+)"$/ do |number|
  expect(page).to have_content number
  sleep 2
end

Given /^I should see in Search Results : "([\-\d,\.]+)"$/ do |number|
  expect(page).to have_content number
  sleep 2
end

Given /^I should see in Details View : "([\-\d,\.]+)"$/ do |number|
  page.find('.totalContact').should have_text number
  sleep 1
end

Given /^I select three row from List View$/ do
  find(:xpath, ".//*[@id='gridbox']/div[2]/table/tbody/tr[2]/td[1]/div/span/span/span/img").click
  find(:xpath, ".//*[@id='gridbox']/div[2]/table/tbody/tr[3]/td[1]/div/span/span/span/img").click
  find(:xpath, ".//*[@id='gridbox']/div[2]/table/tbody/tr[4]/td[1]/div/span/span/span/img").click
end

Given /^I select all contacts from List View$/ do
  find(:xpath, ".//*[@id='gridbox']/div[1]/table/tbody/tr[2]/td[1]/div/span/span/img").click
end

Given /^I select one row from List View$/ do
  find(:xpath, ".//*[@id='gridbox']/div[2]/table/tbody/tr[2]/td[1]/div/span/span/span/img").click
end

# #5 Types of Selection from right Panel:  - Active, InActive, Group, Smartgroup etc
# Given /^I select All Contacts from - right panel container$/ do
#   find('.rightsideround', :text => 'All Contacts').click
# end

# Given /^I select Active Emails from - right panel container$/ do
#   find('#groupactive', :text => 'Active Emails').click
# end
#
# Given /^I select Inactive Emails from - right panel container$/ do
#   find('#groupinactive', :text => 'Inactive Emails').click
# end

# Given /^I select Test Group from Groups - right panel container$/ do
#   find('.rightsideround', match: :first, :text => 'Groups').click
#   find('.deselectgroup', :text => 'Test Group').click
# end

# Given /^I select Test Smart Group from Smart Groups - right panel container$/ do
#   find('.rightsideround', match: :first, :text => 'Smart Groups').click
#   find('.deselectgroup', :text => 'Test Smart Group').click
# end

#3 Types of Selection from right Panel: New Group, New Smart Group, Import Contacts
Given /^I want to Import Contacts$/ do
  find('.importConatct', :text => 'Import Contacts').click
end

Given /^I want to create new Group$/ do
  find('.group', :text => 'New Group').click
end

Given /^I want to create new Smart Group$/ do
  find('#openSmartGroup', :text => 'New Smart Group').click
end

Then /^I should see selected items in list view$/ do
  find('.obj', :text => 'Aivee', :match => :prefer_exact)
  find('.obj', :text => 'Catherine Harvey', :match => :prefer_exact)
  find('.obj', :text => 'sakibqa+7@gmail.com', :match => :prefer_exact)
end

Then /^I should not see selected items in list view$/ do
  page.should have_no_content('.obj', :text => 'Aivee', :match => :prefer_exact)
  page.should have_no_content('.obj', :text => 'Catherine Harvey', :match => :prefer_exact)
  page.should have_no_content('.obj', :text => 'sakibqa+7@gmail.com', :match => :prefer_exact)
end

#Wait Until List View Table Loads#
Given /^I wait until List View Table Load on Screen$/ do
  expect(page).to have_selector('.objbox')
end

Given /^I wait until Details View Block Load on Screen$/ do
  expect(page).to have_selector('.quickProfileWrapper')
end
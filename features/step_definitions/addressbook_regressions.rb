require_relative '../../lib/cucumber_helper'

Given /^I create a New contact in Short$/ do
  find('#btnAddContact').click
  find(:xpath, ".//*[@id='companyname']").set "Affan Inc."
  find('#ProfileDescription', visible: true)
  find('#contact').set "Affan Mahmud"
  find('#salutation').set "Muhammad"
  find('#title').set "Lead Entertainer"
  find(:xpath, ".//*[@placeholder = 'email']").set "affan@mailinator.com"
  find('#addressbookBody #emailstatus').click
  find('.easy-list-item', :text => 'Active').click
  find(:xpath, "//*[@placeholder = 'phone']").set "(832)583820"
  find('#ext').set "503-0206"
  find('#cell').set "(513) 503-0206"
  find('#fax').smaet "(717) 635-0692"
  find('#IDLISTVALUE').click
  find('.easy-list-item', :text => 'New').click
  find('#address').set "7381,Burrows"
  find('#address2').set "54691	Crest Line"
  find('#city').set "New York City"
  find('#state').set "Georgia"
  find('#zip').set "49510-37939"
  find('.saveImg').click
end

Given /^I create a new contact in Detail$/ do
##Quick Profile Block##
  find('#btnAddContact').click
  find(:xpath, ".//*[@id='companyname']").set "Affan Incorporation"
  find('#ProfileDescription', visible: true)
  find('#contact').set "Mahmud"
  find('#salutation').set "Muhammad Affan Mahmud"
  find('#title').set "Lead Entertainer"
  find(:xpath, "//*[@placeholder = 'email']").set "affan@mailinator.com"
  find('#addressbookBody #emailstatus').click
  find('.easy-list-item', :text => 'Active').click
  find(:xpath, "//*[@placeholder = 'phone']").set "(832)583820"
  find('#ext').set "503-0206"
  find('#cell').set "(513) 503-0206"
  find('#fax').set "(717) 635-0692"
  find('#IDLISTVALUE').click
  find('.easy-list-item', :text => 'Attempted').click
  find('#address').set "7381, Burrows"
  find('#address2').set "54691 Crest Line"
  find('#city').set "New York City"
  find('#state').set "Georgia"
  find('#zip').set "49510-37939"
##Mid Profile Block##
  find('#ProfileDescription').set "Each download comes with the demo content as well as the theme options settings for themes. ... With our plugins there is only one file to import, the .xml file (the demo content). ... We already include a demo content file in the WooCommerce plugin folder itself which you can import into .."
  # find('#FIRSTDATEVALUE').click
  # find('.easy-list-item', :text => 'Account').click
  find(:xpath, ".//*[@id='birthdate']").click
  find('.datePickerSkin', :text => '25').click
  find(:xpath, ".//*[@id='AnniversaryDateValue']").click
  find('.datePickerSkin', :text => '15').click
  find('#country').set "Bangladesh"
  find('#website').set "http://www.yahoo.com"
  find('#Ud1Name').set "customteam1"
  find('#Ud2Name').set "customteam2"
##Demography Block##
  find('#SICCode').set "SFC 10 G"
  find('#SICDescription').set "SFC 10 G Model"
  find('#AnnualSales').set "$150000"
  find('#Employees').set "58"
  find('#YearsBusiness').set "2016"
  find('#Franchise').set "SAP Inc"
  find('#LocationType').set "In Mars"
  find('#WomanOwned').set "True"
  find('#LResidence').set "2000 sqft"
  find('#Income').set "$20000"
  find('#OwnRent').set "Yes"
  find('#BldgStructure').set "Iron Made"
  find('#Age').set "25"
  find('#Married').set "Yes"
  find('#Gender').set "Male"
  find('#Children').set "Yes"
  find('#Ethnicty').set "Lorem Ipsm"
  find('#Religion').set "Lorem Ipsm"
  find('#Pool').set "No Pool"
  find('#Pets').set "No Pets"
  find('#CharityDonor').set "Yes"
  find('#OppSeeker').set "Yes -- Looking for"
  find('#OfficeLocation').set "North West Media Ltd, NYC Brooklyn"
  find('#YearsInBusiness').set "19 Years"
  find('.saveImg').click
end

Given /^I Delete a contact$/ do
  find('#btnDeleteContact').click
  find('.button-ok').click
end

# Given /^I want to set a meeting reminder$/ do
#   find('.addEvent').click
#   find(:xpath, "//*[@placeholder = 'Subject']").set "Automatic Meeting set with Sakib"
#   find(:xpath, "//*[@placeholder = 'Details']").set "We need to Discuss with you Sakib -- and expand the automation. Dummy text"
#   find('.save').click
# end

Given /^I create a Note with "([^"]*)"$/ do |notes|
  find('.addNote').click
  find(:xpath, "//*[@placeholder = 'What’s up?']").set "#{notes}"
  find('.red').click
end


Given /^I want to add a deal$/ do
  find('.customeTabs', :text => 'Deals').click
  find('.addDeal', visible: true)
  find('.addDeal').click
  find('.product').click
  find('.easy-list-item', :text => 'Apple').click
  find('.price').click
  find('.easy-list-item', :text => '$1,200.00').click
  find('.stage').click
  find('.easy-list-item', :text => 'Prospect').click
  find('.source').click
  find('.easy-list-item', :text => 'Email Marketing').click
  find('.details-text').set "Create a real deal automatically and move on"
  find('.save').click
end

Given /^I Assign few contacts in "([^"]*)" Tag$/ do |available_group_name|
  find('#btnAddGroup').click
  find('.largeButton', visible: true) #element present verification
  find('.dropdownGN', :text => available_group_name).click
  find('.largeButton').click
end

Given /^I want to Unassign few contacts from newly created Group$/ do
  find('#btnAddGroup').click
  # find('.dropdownGN', :text => selected_group_name)
  page.driver.browser.execute_script %Q{
    $('.groupTagCheckBox').attr('checked','');
  }
end

Given /^Save Tags Button clicked$/ do
  find('.largeButton').click
end

#failing to handle javascript popup
# Given /^I want to delete an activities from details view$/ do
#   find('.btn-remove').click
#   page.driver.browser.switch_to.alert.text.should eq(OK)
# end

# #Create Group from right panel
# Given /^I create a group name as "([^"]*)"$/ do |group_name|
#   #nav
#   expect(page).to have_selector('#addressBook', wait: 10)
#   find(:xpath, ".//*[@id='addressBook']").click
#   #main
#   find('.new.group').click
#   find('#generalGroupTxtField').set "#{group_name}"
#   find('.promtbtn.btnsave').click
# end

#Create Group from right panel
Given /^I quick create a Tag of "([^"]*)" and save it$/ do |group_name|
  find('#btnAddGroup').click
  find('#groupNameForSelectedContacts').set "#{group_name}"
  page.driver.browser.execute_script %Q{ $('.NewGroupNameCheckBox').attr('checked','checked'); }
  find('.largeButton').click
end

Given /^I looked for group named as "([^"]*)" and Delete this group$/ do |look_for_group|
  #nav
  expect(page).to have_selector('#addressBook', wait: 10)
  find(:xpath, ".//*[@id='addressBook']").click
  #main
  # find('#groupName', :match => :first, :text => 'Groups').click
  page.driver.browser.action.move_to(page.find('.deselectgroup', :match => :prefer_exact, :text => look_for_group).native).perform
  find('.editContactGroups').click
  find('#add_delete_folder #groupDDDeleteFolder', :text => 'Delete Group', :match => :prefer_exact).click
end

Given /^I Found a "([^"]*)" group & updated to "([^"]*)"$/ do |existing_group_name, group_name_updated|
  #nav
  expect(page).to have_selector('#addressBook', wait: 10)
  find(:xpath, ".//*[@id='addressBook']").click
  #main
  # find('#groupName', :match => :first, :text => 'Groups').click
  page.driver.browser.action.move_to(page.find('.deselectgroup', :match => :prefer_exact, :text => existing_group_name).native).perform
  find('.editContactGroups').click
  find('#add_delete_folder #groupDDRenameGroupFolder', :text => 'Rename Group', :match => :prefer_exact).click
  find('#txtDoubleClickRenameGroup').set "#{group_name_updated}"
  find('#txtDoubleClickRenameGroup').native.send_key("\n")
end

Given /^I select Manage Column options$/ do
  find('.manageColumn').click
end

Given /^I select All Columns Checkbox$/ do
  page.driver.browser.execute_script %Q{
      $('.field1 .fieldCheck').attr('checked','checked');
  }
end

Given /^I Unselect All Columns Checkbox, Except One$/ do
  page.driver.browser.execute_script %Q{
      $('.field1 .fieldCheck').attr('checked','');
  }
  find(:xpath, ".//*[@id='divEditColumn']/div[2]/div[2]/div[1]/span/span/img").click
end

Given /^I save the selected Columns$/ do
  find('.saveImg').click
end

Given /^Replace Field Data selected "([^"]*)" and Replace with "([^"]*)"$/ do |enlisted_field, user_input|
  find('#ContactOptionMenu').click
  find('.splash_easy_row', :text => 'Replace Field Data', exact: true).click
  find('#ColumnListContainer').click
  find('.updateColumnList', :text => enlisted_field, :match => :prefer_exact).click
  find('#updateTo').set "#{user_input}"
  find('.saveImg').click
end

Given /^I Found the Group named as "([^"]*)"$/ do |select_existing_group|
  find('#group', :match => :first, :text => 'Groups').click
  page.find('.deselectgroup', :match => :prefer_exact, :text => select_existing_group).click
end

Given /^Export Contacts selected$/ do
  find('#ContactOptionMenu').click
  find('.splash_easy_row', :text => 'Export Contacts', exact: true).click
  expect(page).to have_selector('.EditColumnFieldsR')
  find('.saveImg').click
end

Given /^I select All Contact from List View$/ do
  find(xpath: ".//*[@id='gridbox']/div[1]/table/tbody/tr[2]/td[1]/div/span/span/img").click
end

#-------------------
# Contact Right panel dropdown activation
Given /^I click on the contact right panel Dropdown Icon$/ do
  find('#ContactOptionMenu').click
end

# Select any options from the dropdown list
Given /^I select "([^"]*)" from the dropdown list$/ do |action_item|
find('.splash_panel_listitem .splash_easy_row', :match => :prefer_exact, :text => action_item).click
end

# Contact report page -- Print button
Given /^I click on the Print button on the contact report page$/ do
  find('.print').click
end

# Contact report page -- Cancel button
Given /^I click on the Cancel button on the contact report page$/ do
  find('.cancel').click
end

# Contact report page -- Status button
Given /^I click on the Status button on the contact report page$/ do
  find('.status').click
end

# Select any options from the dropdown list
Given /^I select option "([^"]*)"$/ do |action_item|
  find('.easy-list-item', :text => action_item).click
end

# Select any options from the addressbook right panel
Given /^I select option "([^"]*)" from right panel$/ do |action_item|
  find('#groupName', :text => action_item).click
end

# Addressbook email icon click
Given /^I click on the Email Icon$/ do
  find('.icon-email').click
end

# Given /^I accept the popup$/ do
#   #page.driver.browser.switch_to.alert.accept
#   page.driver.browser.execute_script %Q{ window.confirm = function() { return true; } }
#   page.driver.browser.switch_to.window
#   # page.evaluate_script('window.confirm = function() { return true; }')
#   # page.click('Remove')
# end
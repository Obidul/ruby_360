require_relative '../../lib/cucumber_helper'

# Select option from avater dropdown
Given /^I click on the avater button and select "([^"]*)"$/ do |select_option|
  find('.headerProfile').click
  find('.splash_easy_row', :text => select_option).click
end

# Select option from the profile right panel
Given /^I click and select "([^"]*)" from the profile right panel$/ do |select_option|
  find('.profileMainMenu', :text => select_option).click
end

#Account Profile --------------Start
# Account information fillup
Given /^I fillup the account informations$/ do
  find('#CompanyName').set "Automation Inc."
  find('#FullName').set "Mohammad Ali"
  find('#Title').set "World Champion Boxer"
  find('#Phone').set "0123456789"
  find('#PhoneExt').set "1234"
  find('#Address1').set "1708"
  find('#Address2').set "Yakima Ave"
  find('#City').set "Dhaka"
  find('#State').set "Dhaka"
  find('#Zip').set "98027"
  find('#Website').set "www.google.com"
  @company_name = find('#CompanyName').value
  @full_name = find('#FullName').value
  find('#FromName').set @company_name
  find('#FooterName').set @full_name
end

# Click on the profile save button
Given /^I click on the Save button$/ do
  find('.saveProfile').click
end

# Click on the profile cancel button
Given /^I click on the Cancel button$/ do
  find('.cancelProfile').click
end
#Account Profile --------------End

#Subscription Plan --------------Start
#Cancel Account button
Given /^I click on the Cancel Account button on the subscription Plan page$/ do
  find('.cancelAccount', :text => 'Cancel Account').click
end

# Username and password fillup for cancel account
Given /^I fillup the username and password$/ do
  find('.userName').set "milon64"
  find('.password').set "123456"
  @pass = find('.password').value
  find('.confirmPassword').set @pass
  sleep 2
end

#Return: Do Not Cancel button
Given /^I click on the Return: Do Not Cancel button$/ do
  find('.returnFromCancel').click
end

#Cancel Account button
Given /^I click on the Cancel Account button$/ do
  find('.cancelAccountButton').click
end
#Subscription Plan --------------End

#Purchase History --------------Start
# Checekd Monthly Invoices Option
Given /^I checked the Monthly Invoices$/ do
  find('.monthlyInvoice .mark').click
end
#Purchase History --------------End

#Manage Users --------------Start
# Click on the Add New User button
Given /^I click on the Add New User button$/ do
  find('.addNewUser').click
end

# Sub user account details
Given /^I fillup the new user account detials$/ do
  find('#FirstName').set "Abdul"
  find('#LastName').set "Alim"
  find('#Email').set "abdulalim6677@mailinator.com"
  sleep 2
end

# Click on the Send Invitaion button
Given /^I click on the Send Invitaion button$/ do
  find('.addNewUser').click
end

# Click on the Edit button on the manage users page
Given /^I click on the edit of the subuser$/ do
  find('.manageUserTable tr:nth-child(3) .edit').click
  #find(:xpath, "//tr[contains(.,'Edit')]", :text => subuser_name, :match => :first).click #-- work
  #find('.manageUserTable', :text => subuser_name).find(:xpath, "//tr[contains(.,'Edit')]").click #-- work
end

# Click on the Edit button on the manage users page
Given /^I click on the delete of the subuser$/ do
  find('.manageUserTable tr:nth-child(3) .delete').click
end

# Block access
Given /^I select Block access$/ do
  find(:xpath, ".//*[@id='myProfile']/div/div[2]/div[2]/div[1]/span[1]/span/img").click
  # page.driver.browser.execute_script %Q{ 
  #   $('#blockAccess').attr('checked','checked');
  # }
end

# Assign New User to Account
Given /^I select Assign New User to Account$/ do
  find(:xpath, ".//*[@id='myProfile']/div/div[2]/div[2]/div[4]/span[1]/span/img").click
end

# Assign New User to Account details
Given /^I fillup the Assign New User detials$/ do
  find('#FirstName').set "Habibur"
  find('#LastName').set "Rahman"
  find('#Email').set "rahimrahman6677@mailinator.com"
  sleep 2
end

# Login and register to the system
Given /^I click on the Login button$/ do
  find('#createaccount').click
end
#Manage Users --------------End

#Update password -----------Start
# Sub user account details
Given /^I fillup the current & new passowrd$/ do
  find('#CurrentPassword').set "123456"
  find('#NewPassword').set "112233"
  @pass = find('#NewPassword').value
  find('#VerifyPassword').set @pass
  sleep 2
end

# Click on the profile cancel button
Given /^I click on the Reset Password button$/ do
  find('.saveLarge').click
end
#Update password -----------End
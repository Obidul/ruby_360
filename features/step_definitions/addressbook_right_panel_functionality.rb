require_relative '../../lib/cucumber_helper'

#Navigate to Right Panel Drop-down Menus#

Given /^In Activity Tab I select "([^"]*)" and select "([^"]*)" -- while in Addressbook$/ do |all_deals, drop_down_items|
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', text: all_deals, :match => :prefer_exact).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^In Deals Tab I select "([^"]*)" and select "([^"]*)" -- while in Addressbook$/ do |deals, drop_down_items|
  # find('#secondaryActionListTab li', text: 'Deals', :match => :prefer_exact).click
  find('#addressBookDiv .contactActionList li a .navigation-label', :match => :prefer_exact, :text => deals).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

#Tags

Given /^In Tags Tab I select "([^"]*)" and select "([^"]*)" -- while in Addressbook$/ do |tags, drop_down_items|
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => tags).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^I found existing "([^"]*)" tag and rename to "([^"]*)" -- while in Addressbook$/ do |tags, tags_renamed|
  find('#addressBookDiv #secondaryActionListGrp li a .navigation-label', :match => :prefer_exact, :text => tags).hover
  find('.icon-edit').click
  find('.splash_easy_row', text: 'Rename Tag', :match => :prefer_exact).click
  sleep 0.1
  find('.tagOrFilterText').set "#{tags_renamed}"
  find('.tagOrFilterText').native.send_key("\n")
end

Given /^To create Tags I fill in "([^"]*)" and save it -- while in Addressbook$/ do |tags_fill_in|
  find('#generalGroupTxtField').set "#{tags_fill_in}"
  find('.btnsave').click
end

#Filter

Given /^In Filter Tab I select "([^"]*)" and select "([^"]*)" -- while in Addressbook$/ do |filters, drop_down_items|
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => filters).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end

Given /^To create Filter - I fill in "([^"]*)" and save as "([^"]*)" -- while in Addressbook$/ do |filter_name, save_filter_as|
  find('#txtField0').set "#{filter_name}"
  find('.btnsave').click
  find('#smartGroupName').set "#{save_filter_as}"
  find('.arButton.save').click
end

Given /^I found existing "([^"]*)" filter and rename to "([^"]*)" -- while in Addressbook$/ do |filter, filter_renamed|
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => filter).hover
  find('.icon-edit').click
  find('.splash_easy_row', text: 'Rename Filter', :match => :prefer_exact).click
  sleep 0.1
  find('.tagOrFilterText').set "#{filter_renamed}"
  find('.tagOrFilterText').native.send_key("\n")
end

Given /^In Filter Tab I select "([^"]*)" this filter$/ do |filters_name|
  find('#secondaryActionListTab li', text: 'Filters', :match => :prefer_exact).click
  sleep 0.5
  find('#addressBookDiv #secondaryActionListGrp a .navigation-label', :match => :prefer_exact, :text => filters_name).click
  sleep 0.2
end

#Status menu
Given /^In Status menu I select "([^"]*)" and select "([^"]*)" -- while in Addressbook$/ do |status, drop_down_items|
  find('#addressBookDiv a .navigation-label', :match => :prefer_exact, :text => status).hover
  find('.icon-edit').click
  sleep 0.2
  find('.splash_easy_row', :match => :prefer_exact, :text => drop_down_items).click
end
require_relative '../../lib/cucumber_helper'

#From Calender Selection - Start

#Calender Range Outside

Given /^From Calender Range - I select Next Month$/ do
  find('.nextMonth')
end

Given /^From Calender Range - I select Previous Month$/ do
  find('.prevMonth')
end

Given /^From Calender Range - I select Today$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  find('.today', :text => /\AToday\z/, exact: true).click
  # find('.datePickerSkin.calendarDate.today.active').click
end

Given /^From Calender Range - I select Week$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  find('.week').click
end

Given /^From Calender Range - I select Month$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  find('.month').click
end

Given /^From Calender Range - I select All$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  find('.all').click
end

Given /^Within Calender Range - I select Today$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  find('.datePickerSkin.calendarDate.today').click
end

Given /^Within Calender Range - I select Tomorrow$/ do
  find('.timeRangeLabel').click
  sleep 0.2
  page.execute_script %Q{
  var currentValue = parseInt($('.datePickerSkin.calendarDate.today').html(), 10);
  var newValue = parseInt(currentValue) + 1
  $('.datePickerSkin.calendarDate.today').html(newValue);
     }
  find('.datePickerSkin.calendarDate.today').click
end

Given /^Within Calender Range - I select Yesterday$/ do
  find('.timeRangeLabel').click
  page.execute_script %Q{
  var currentValue = parseInt($('.datePickerSkin.calendarDate.today').html(), 10);
  var newValue = parseInt(currentValue) - 1
  $('.datePickerSkin.calendarDate.today').html(newValue);
    }
  find('.datePickerSkin.calendarDate.today').click
end

#Calender Inside Selection #

Given /^I select Next Date - Tomorrow$/ do
  find('.dateTime').click
  page.execute_script %Q{
  var currentValue = parseInt($('.datePickerSkin.calendarDate.today').html(), 10);
  var newValue = parseInt(currentValue) + 1
  $('.datePickerSkin.calendarDate.today').html(newValue);
    }
  find('.datePickerSkin.calendarDate.today').click
  find('.blue').click
end

Given /^I select Previous Date - Yesterday$/ do
  find('.dateTime').click
  page.execute_script %Q{
  var currentValue = parseInt($('.datePickerSkin.calendarDate.today').html(), 10);
  var newValue = parseInt(currentValue) - 1
  $('.datePickerSkin.calendarDate.today').html(newValue);
    }
  find('.datePickerSkin.calendarDate.today').click
  find('.blue').click
end

Given /^From Calender Select Date: "([^"]*)" and set Time: "([^"]*)"$/ do |custom_date, custom_time|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find('.dateTime').click
  find('.datePickerSkin', :text => custom_date).click
  find('#slider-value').set "#{custom_time}"
  find('.blue').click
end

#Timezone Scenario steps

#Find: Today + 1 = Next Date
# Given /^I select Next Date - Tomorrow$/ do
#   page.execute_script %Q{
#   var currentValue = parseInt($('.datePickerSkin.calendarDate.today.active').html(), 10);
#   var newValue = parseInt(currentValue) + 1
#   $('.datePickerSkin.calendarDate.today.active').html(newValue);
#     }
#   find('.datePickerSkin.calendarDate.today.active').click
#   find('.blue').click
# end

# #Find: Today - 1 = Previous Date
# Given /^I select Previous Date - Yesterday Timezone$/ do
#   page.execute_script %Q{
#   var currentValue = parseInt($('.datePickerSkin.calendarDate.today.active').html(), 10);
#   var newValue = parseInt(currentValue) - 1
#   $('.datePickerSkin.calendarDate.today.active').html(newValue);
#   }
#   find('.datePickerSkin.calendarDate.today.active').click
#   find('.blue').click
# end

#Default and Custom Time & Date

Given /^I select Date "([\-\d,\.]+)" & select activity type "([^"]*)"$/ do |date, activity_type|
  find('.dateTime').click
  find('.datePickerSkin', :text => date).click
  find('.blue').click
  find('.selTxt').click
  find('.easy-list-item', :text => activity_type).click
end

Given /^I select Default Date & Time and select activity type "([^"]*)"$/ do |activity_type|
  find('.selTxt').click
  find('.easy-list-item', :text => activity_type).click
end

Given /^I select Date to "([^"]*)"$/ do |user_input|
  find(:xpath, "//*[@placeholder = 'Due Date']").click
  find('.datePickerSkin', :text => user_input).click
end

#From Calender Selection - End

Given /^I should see Overdue$/ do
  page.evaluate_script('$(".overdue").css("background-color")')
end

# Example scenario: I should see Date & Time: "12:00 PM" & Activity Type with Company Title: "Meeting - Abata" & Activity Subject: "Default Date & Time Activity" has enlisted
Given /^I should see "([^"]*)" & "([^"]*)" & "([^"]*)" has enlisted$/ do |time_date_anytime, activity_type_company_title, activity_summary_in_subject|
  expect(page).to have_selector('.ui-sortable')
  find('.time .lgText', :text => time_date_anytime, :match => :prefer_exact) and find('.lgText', :text => activity_type_company_title, :match => :prefer_exact) and find('.subject', :text => activity_summary_in_subject, :match => :prefer_exact)
end

Given /^I have updated the activities of "([^"]*)" and set new time "([^"]*)"$/ do |user_input, custom_time|
  find('.subject', :text => user_input, :match => :prefer_exact).click
  find('.dateTime').click
  find('#slider-value').set "#{custom_time}"
  find('.blue').click
end

Given /^I create activity of "([^"]*)" and with custom time: "([^"]*)"$/ do |user_input, custom_time|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
  find('.dateTime').click
  find('#slider-value').set "#{custom_time}"
  find('.blue').click
end

Given /^I create activity of "([^"]*)" and set custom time: "([^"]*)"$/ do |user_input, custom_time|
  find(:xpath, ".//*[@id='leftHeaderRight']/li[2]/i").click
  find(:xpath, "//*[@placeholder = 'What action is next?']").set "#{user_input}"
  find('.dateTime').click
  find('#slider-value').set "#{custom_time}"
end

Given /^I have verified activity of "([^"]*)" and pre-set time is "([^"]*)"$/ do |user_input, time_month_match|
  find('.subject', :text => user_input, :match => :prefer_exact).click
  expect(page).to have_content time_month_match
  find('.cancel').click
end

#Windows console to switch:

# tzutil /s "Bangladesh Standard Time"
# tzutil /s "Central Standard Time"

#    #Sorting works with "scheduled time"
#    #Each scenario does
#    a. add to verify,
#    b. update to verify,
#    c. task completed
#    c1. task completed verification (time and date)
#    d. task retained or returned to life.
#    d1. task active verification (time and date)


#Note: -> http://screencast.com/t/tOR2oUducI
#  @tomorrow => current date + 1
#  @yesterday => current date - 1
#  @update_date => requires to update date manually (both @tomorrow & yesterday)


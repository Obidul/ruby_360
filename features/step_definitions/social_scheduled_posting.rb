require_relative '../../lib/cucumber_helper'

#Notes & Known issues:

# 1. Passed Date has Validation
# 2. Passed Time has no validation.
# 3. Schedule Post save will be failed if we execute on exact 15 min block. Such as, Now 12:00PM and test executed to make schedule post at 12:00PM. It will be failed.
    #Unexpected modal dialog (text: Schedule time has already passed. Please choose correct schedule date and time.): "Schedule time has already passed. Please choose correct schedule date and time." (Selenium::WebDriver::Error::UnhandledAlertError)
# 4. FFP post scheduled but later on selected FP network => Post will be posted to FP network and vice-versa.
# 5. LCP post scheduled but later on selected LP network => Post will be posted to LP network and vice-versa.

#Start Here - Default and Custom Time & Date Selection

Given /^Scheduling Social Post with Default Time & Date$/ do
  find(:xpath, ".//*[@id='SocialHistory']").click
end

Given /^Scheduling Social Post with Date "([^"]*)"$/ do |date|
  find(:xpath, ".//*[@id='SocialHistory']").click
  find(:xpath, ".//*[@id='SocialDate']").click
  page.execute_script %Q{ $("input[id^='splash_']:contains('28')").trigger("click"); }
  find('.calendarDate', :text => date).click
end

Given /^Scheduling Social Post with Time "([^"]*)"$/ do |time| #Time format => http://tinyurl.com/h97uq4x
  find(:xpath, ".//*[@id='SocialHistory']").click
  find('#social_sche_time__iCombo').click
  page.find('li', :text => time).click #selection works without showing visually selected but jump
end

Given /^Scheduling Social Post, Select Date "([^"]*)" & Time "([^"]*)"$/ do |date, time|
  find(:xpath, ".//*[@id='SocialHistory']").click
  find(:xpath, ".//*[@id='SocialDate']").click
  page.execute_script %Q{ $("input[id^='splash_']:contains('28')").trigger("click"); }
  find('.calendarDate', :text => date).click
  find('#social_sche_time__iCombo').click
  page.find('li', :text => time).click #selection works without showing visually selected but jumps
end

#End Here - Default and Custom Time & Date Selection

#Possibly Garbage
Given /^I select date from social calender$/ do
  find(:xpath, ".//*[@id='tc_tableOfCalRow']/tbody/tr[4]/td[7]").click #28
end

Given /^I select schedule post with default time and date$/ do
  find(:xpath, ".//*[@id='SocialHistory']").click
end

Given /^I want to make schedule posting with custom time and date$/ do
  find(:xpath, ".//*[@id='SocialHistory']").click
  find(:xpath, ".//*[@id='SocialDate']").click
  page.execute_script %Q{ $("input[id^='splash_']:contains('28')").trigger("click"); }
end

#end Possibly Garbage
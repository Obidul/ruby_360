require_relative '../../lib/cucumber_helper'

Given /^Go for Email Service$/ do
  expect(page).to have_selector('#email')
  find('#email').click
end

Given /^Go for Email Compose$/ do
  expect(page).to have_selector('#emailCompose')
  # find('#emailCompose').click
  # find(:xpath, ".//*[@id='emailCompose']/span").click
  find(:xpath, ".//*[@id='emailCompose']").click
end

#For Campaign Group Selection - Auto Post
Given /^In To field: I typed "([^"]*)" campaign group and found "([^"]*)" and selected "([^"]*)" from the list$/ do |to_whom, selected, select_campaign_name|
  find('#to').set "#{to_whom}"
  sleep 1
  page.execute_script %Q{ $('.ui-state-focus a:contains("#{selected}")').click(); }
  find('.group', :text => select_campaign_name).click
end

Given /^In Subject field: I fill in "([^"]*)"$/ do |subject|
  find('#subject').set "#{subject}"
end

#Single Email + Campaign Email Combined -- TEST
Given /^In field: Typed "([^"]*)" and found "([^"]*)" campaign group and selected "([^"]*)" from the list and input "([^"]*)"$/ do |to_whom, selected, select_campaign_name, text|
  find('#to').set "#{to_whom}"
  sleep 1
  page.execute_script %Q{ $('.ui-state-focus a:contains("#{selected}")').click(); }
  find('.group', :text => select_campaign_name).click
end

#For Single Email Typed - Auto Post
Given /^In To field: I typed an Email Address "([^"]*)"$/ do |to_whom_single_email|
  find('#to').set "#{to_whom_single_email}"
end

#Email Form: Send/Schedule Email Button

Given /^I click on Send button$/ do
  find('.cke_button__emailsend_label').click
end

Given /^I click on Schedule button$/ do
  find('.cke_button__emailschedule_label').click
end

#Email Sent Confirmation Verify.

Given /^I should see Email has sent confirmation$/ do
  expect(page).to have_selector('#EmailSentSuccessfullyHeaderTop', wait: 10)
  expect(page).to have_content 'Email sent successfully.'
end

#Email Scheduled Confirmation Verify from the Scheduled Campaigns List.
Given /^Email has scheduled in list with this subject: "([^"]*)"$/ do |text|
  expect(page).to have_selector('.templateSize', wait: 10)
  expect(page).to have_content text
end
### Buttons To select Social Networks ###

## Direct Post ##
Given /^I select Facebook for Social Auto Post & Send Now$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[1]/span/span/img").click
  find('#deliver_send_now').click
end

Given /^I select Twitter for Social Auto Post & Send Now$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[3]/span/span/img").click
  find('#deliver_send_now').click
end

Given /^I select Linkedin for Social Auto Post & Send Now$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[5]/span/span/img").click
  find('#deliver_send_now').click
end

Given /^I select All Network for Auto Post & Send Now$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[1]/span/span/img").click
  find(:xpath, ".//*[@id='social_share_button']/div[3]/span/span/img").click
  find(:xpath, ".//*[@id='social_share_button']/div[5]/span/span/img").click
  find('#deliver_send_now').click
end

Given /^I select Twitter & Linkedin Network for Auto Post & Send Nowe$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[3]/span/span/img").click
  find(:xpath, ".//*[@id='social_share_button']/div[5]/span/span/img").click
  find('#deliver_send_now').click
end

Given /^I select Facebook & Linkedin Network for Auto Post & Send Now$/ do
  find(:xpath, ".//*[@id='social_share_button']/div[1]/span/span/img").click
  find(:xpath, ".//*[@id='social_share_button']/div[5]/span/span/img").click
  find('#deliver_send_now').click
end

## Scheduled Post ##

Given /^I select Facebook for Social Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[1]/span/span/img").click
  find('.scheduleSave').click
end

Given /^I select Twitter for Social Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[3]/span/span/img").click
  find('.scheduleSave').click
end

Given /^I select Linkedin for Social Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[5]/span/span/img").click
  find('.scheduleSave').click
end

Given /^I select All Network for Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[1]/span/span/img").click
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[3]/span/span/img").click
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[5]/span/span/img").click
  find('.scheduleSave').click
end

Given /^I select Twitter & Linkedin Network for Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[3]/span/span/img").click
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[5]/span/span/img").click
  find('.scheduleSave').click
end

Given /^I select Facebook & Linkedin Network for Auto Post & Saved for Schedule$/ do
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[1]/span/span/img").click
  find(:xpath, ".//*[@id='schedule_social_share_button']/div[5]/span/span/img").click
  find('.scheduleSave').click
end

## Ends Here -- Buttons to select social networks

#Append value additional in email subject input field.

#Credit goes to -> https://twitter.com/isakib/status/739853604996059141

Given /^I add contacts of "([^"]*)"$/ do |additional|
  page.driver.browser.execute_script %Q{
    var $to = $('#to');
    $to.val($to.val() + '#{additional}');
  }
end

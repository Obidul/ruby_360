require_relative '../../lib/cucumber_helper'

Given /^Go for Social Module$/ do
  expect(page).to have_selector('#social', wait: 10)
  find(:xpath, ".//*[@id='social']").click
  # expect(page).to have_selector('#socialMessagingBackgroundImage')
end

Given /^I should see in Message Queue : ([\-\d,\.]+)$/ do |number|
  page.find('.socialPostCount').should have_text number
  sleep 1
end

Given /^I should see in Limit Counter : ([\-\d,\.]+)$/ do |number|
  page.find('#socialCharacterCount').should have_text number
  sleep 1
end
require_relative '../../lib/cucumber_helper'

# Click on New button to compose an email
Given /^I compose a new email$/ do
  find('#emailCompose').click
end

# Email : Button Save
Given /^I click on the save button$/ do
  find('.cke_button__emailsave_label').click
  sleep 1
end

# Email : Send Now Green button
Given /^I click on the Send Now button$/ do
  find('#deliver_send_now').click
  sleep 1
end

# Email : Save Green button
Given /^I click on the Save button to Schedule campaign$/ do
  find('.scheduleSave').click
  sleep 5
end

# Delete the Scheduled Campaign
Given /^I click on the delete button of the scheduled campaign named: "([^"]*)"$/ do |sc_name|
  page.driver.browser.action.move_to(page.find('.gallery .template .templateSize', :text => sc_name, :match => :prefer_exact).native).perform
  sleep 2
  find('.EmailTemplateDeleteView').click
  sleep 2
end

# Click on Email: Preview button
Given /^I click on the email preview button$/ do
  find('.cke_button__emailpreview_icon').click
end

# Click on Email: Preview button
Given /^I switch to mobile view$/ do
  find('.mobileButton').click
end

# Email Preview : Send Email
Given /^I send the test email to : "([^"]*)"$/ do |email|
  find('.emailPreviewPage .email').set "#{email}"
  sleep 2
  find('.sendButton').click
end

# Create Scheduled Campaign from Plus button
Given /^I want to create Scheduled Campaign$/ do
  page.driver.browser.action.move_to(page.find('.rightpanel-item', :text => 'Scheduled Campaigns' , :match => :prefer_exact).native).perform
  sleep 2
  find('.round-plus').click
end
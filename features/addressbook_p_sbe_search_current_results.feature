@sbe
Feature: SBE - Search Current Results

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Search By Example access to conduct the test execution
    Given Wait until the Application is ready
    And Go for Search By Example
    And I wait for 2 seconds
    And New Search for Email Status and selected "Active" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "100"

  #  @test4 @generic
  #  Scenario: SBE - Search Current Results
  #    And Go for Search By Example
  #    And Search Current Results with - "#city" & Start with "New York"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "3"
  #    And I wait for 3 seconds

  @sbe_search_current_results @qp122
  Scenario: SBE - Search Current Results - Company
    And Go for Search By Example
    And Search in Current Results with - "#companyname" & Start with "Myworks"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @sbe_search_current_results @sa
  Scenario: SBE - Search Current Results - Contact
    And Go for Search By Example
    And Search in Current Results with - "#contact" & Start with "Sa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Salutation/Prefix
    And Go for Search By Example
    And Search in Current Results with - "#salutation" & Start with "Sr"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "19"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Title
    And Go for Search By Example
    And Search in Current Results with - "#title" & Start with "Ms"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "17"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Address
    And Go for Search By Example
    And Search in Current Results with - "#address" & Start with "4"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "12"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Address2
    And Go for Search By Example
    And Search in Current Results with - "#address2" & Start with "Oak Valley"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Phone
    And Go for Search By Example
    And Search in Current Results with - "#phone" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "120"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Cell/Mobile
    And Go for Search By Example
    And Search in Current Results with - "#cell" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "120"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Fax
    And Go for Search By Example
    And Search in Current Results with - "#fax" & Start with "4-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "120"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Ext
    And Go for Search By Example
    And Search in Current Results with - "#ext" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - State
    And Go for Search By Example
    And Search in Current Results with - "#state" & Start with "Ohio"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "7"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - City
    And Go for Search By Example
    And Search in Current Results with - "#city" & Start with "New York"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Zip
    And Go for Search By Example
    And Search in Current Results with - "#zip" & Start with "6"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  @sbe_new @qp @em
  Scenario: SBE - Search Current Results - Email
    And Go for Search By Example
    And Search in Current Results with Email & Start with "sakibqa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "99"
    And I wait for 3 seconds

  @sbe_new @qp5 @es
  Scenario Outline: SBE - Search Current Results - Email Status
    And Go for Search By Example
    And Search in Current Results with Email Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status    | number | secs |
    | Active           | 100    | 3    |
    | No Email Address | 20     | 3    |

  @sbe_new @qp6
  Scenario Outline: SBE - Search Current Results - Status
    And Go for Search By Example
    And Search in Current Results with Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status | number | secs |
    | Attempted     | 5      | 3    |
    | Hot Prospect  | 5      | 3    |

  @sbe_new @wip @qp
  Scenario: SBE - Search Current Results - Team Owner
    And Go for Search By Example
    And I select Search Current Results
    And Search in Current Results for Team Owner and selected "Sakib Mahmud" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "99"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Profile Description
    And Go for Search By Example
    And Search in Current Results with - "#ProfileDescription" & Start with "viva"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @sbe_new @bi
  Scenario: SBE - Search Current Results - Birthday
    And Go for Search By Example
    And Search in Current Results with Birthday & selected Date is "February 1"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "23"
    And I wait for 3 seconds

  @sbe_new @an
  Scenario: SBE - Search Current Results - Anniversary
    And Go for Search By Example
    And Search in Current Results with Anniversary & selected Date is "April 3"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "32"
    And I wait for 3 seconds

  @sbe_search_current_results @qp
  Scenario: SBE - Search Current Results - Country
    And Go for Search By Example
    And Search in Current Results with - "#country" & Start with "Australia"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "20"
    And I wait for 3 seconds

  @sbe_new @qp
  Scenario: SBE - Search Current Results - Website
    And Go for Search By Example
    And Search in Current Results with - "#website" & Start with "meetup.com"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  #TODO: BELOW TEST REQUIRES SCROLLING FUNCTIONS =>> PULL FROM m_sbe_new_search.feature rest scenario

@sbe
Feature: SBE - New Search

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Search By Example access to conduct the test execution
    Given Wait until the Application is ready
    Given Go for Search By Example

  @new_search @qp
  Scenario: SBE - New Search - Company
    And New Search with - "#companyname" & Start with "Myworks"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Contact
    And New Search with - "#contact" & Start with "Sa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Salutation
    And New Search with - "#salutation" & Start with "Sr"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "23"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Title
    And New Search with - "#title" & Start with "Mr"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "45"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Address
    And New Search with - "#address" & Start with "12"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Address2
    And New Search with - "#address2" & Start with "Oak Valley"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Phone
    And New Search with - "#phone" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "5"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Cell
    And New Search with - "#cell" & Start with "4"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "14"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Fax
    And New Search with - "#fax" & Start with "2"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "11"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Ext
    And New Search with - "#ext" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - State
    And New Search with - "#state" & Start with "Ohio"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "8"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - City
    And New Search with - "#city" & Start with "New York"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Zip
    And New Search with - "#zip" & Start with "6"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "6"
    And I wait for 3 seconds

  @sbe_new @qp @em
  Scenario: SBE - New Search - Email
    And New Search with Email & Start with "sakibqa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "99"
    And I wait for 3 seconds

  @sbe_new @qp @es
  Scenario Outline: SBE - New Search - Email Status
    And New Search for Email Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status    | number | secs |
    | Active           | 100    | 3    |
    | No Email Address | 20     | 3    |

  @sbe_new @qp
  Scenario Outline: SBE - New Search - Status
    And New Search for Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status | number | secs |
    | Attempted     | 5      | 3    |
    | Hot Prospect  | 5      | 3    |

  @sbe_new @wip @qp
  Scenario: SBE - New Search - Team Owner
    And New Search for Team Owner and selected "Sakib Mahmud" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "119"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Profile Description
    And New Search with - "#ProfileDescription" & Start with "viva"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "2"
    And I wait for 3 seconds

  @sbe_new @qp
  Scenario: SBE - New Search - Birthday
    And I wait for 2 seconds
    And New Search for Birthday & selected Date is "December 2"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  @sbe_new @qp @wip
  Scenario: SBE - New Search - Anniversary
    And I wait for 2 seconds
    And New Search for Anniversary & selected Date is "April 3"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  @new_search @qp
  Scenario: SBE - New Search - Country
    And New Search with - "#country" & Start with "Australia"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "20"
    And I wait for 3 seconds

  @sbe_new @qp
  Scenario: SBE - New Search - Website
    And New Search with - "#website" & Start with "meetup.com"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  #TODO: BELOW TEST REQUIRES SCROLLING FUNCTIONS

  #  @sbe_new
  #  Scenario: SBE - New Search - SIC Code
  #    And Go for Search By Example
  #    And I scroll to find the content
  #    And New Search with - "#SICCode" & Start with "1"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "11"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - SIC Description
  #    And Go for Search By Example
  #    And New Search with - "#SICDescription" & Start with "non"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "3"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Annual Sales
  #    And Go for Search By Example
  #    And I scroll to find the content
  #    And New Search with - "#AnnualSales" & Start with "$77,962.39"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Employees
  #    And Go for Search By Example
  #    And I scroll to find the content
  #    And New Search with - "#Employees" & Start with "3"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "2"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - YearsBusiness
  #    And Go for Search By Example
  #    And I scroll to find the content
  #    And New Search with - "#YearsBusiness" & Start with "3"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "100"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Annual Sales
  #    And Go for Search By Example
  #    And I scroll to find the content
  #    And New Search with - "#Franchise" & Start with "TRUE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "62"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - LocationType
  #    And Go for Search By Example
  #    And I scroll to mid level
  #    And New Search with - "#LocationType" & Start with "in"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "6"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - WomanOwned
  #    And Go for Search By Example
  #    And New Search with - "#WomanOwned" & Start with "FALSE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "57"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - LResidence
  #    And Go for Search By Example
  #    And New Search with - "#LResidence" & Start with "1"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "4"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Income
  #    And Go for Search By Example
  #    And New Search with - "#Income" & Start with "$23,607.62"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Own Rent
  #    And Go for Search By Example
  #    And New Search with - "#OwnRent" & Start with "TRUE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "63"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - BldgStructure
  #    And Go for Search By Example
  #    And New Search with - "#BldgStructure" & Start with "non"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Age
  #    And Go for Search By Example
  #    And New Search with - "#Age" & Start with "76"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "3"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Married
  #    And Go for Search By Example
  #    And New Search with - "#Married" & Start with "FALSE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "54"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Gender
  #    And Go for Search By Example
  #    And New Search with - "#Gender" & Start with "Male"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "60"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Children
  #    And Go for Search By Example
  #    And New Search with - "#Children" & Start with "5"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "15"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Ethnicty
  #    And Go for Search By Example
  #    And New Search with - "#Ethnicty" & Start with "vestibulum"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "4"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Religion
  #    And Go for Search By Example
  #    And New Search with - "#Religion" & Start with "at"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Pool
  #    And Go for Search By Example
  #    And New Search with - "#Pool" & Start with "TRUE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "60"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Pets
  #    And Go for Search By Example
  #    And New Search with - "#Pets" & Start with "TRUE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "52"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - CharityDonor
  #    And Go for Search By Example
  #    And New Search with - "#CharityDonor" & Start with "FALSE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "67"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - OppSeeker
  #    And Go for Search By Example
  #    And New Search with - "#OppSeeker" & Start with "FALSE"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "62"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - OfficeLocation
  #    And Go for Search By Example
  #    And New Search with - "#OfficeLocation" & Start with "2"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "14"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - YearsInBusiness
  #    And Go for Search By Example
  #    And New Search with - "#YearsInBusiness" & Start with "31"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds

  #  @sbe_new
  #  Scenario: SBE - New Search - Create date
  #    And Go for Search By Example
  #    And New Search with - "#createdate" & Start with "4/13/2016"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "7"
  #    And I wait for 3 seconds
  #
  #  @sbe_new
  #  Scenario: SBE - New Search - Modified date
  #    And Go for Search By Example
  #    And New Search with - "#lastmodifiedon" & Start with "31"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : "1"
  #    And I wait for 3 seconds

  #Notes: Refactored for scenario outline:
  #  @demo
  #  Scenario Outline: SBE - New Search - Company
  #    And Go for Search By Example
  #    And New Search with - "<selector>" & Start with "<text>"
  #    Then I click to Search Button -- Search By Example
  #    And I should see in Current Lookup : in Search Results : "<number>"
  #    And I wait for <secs> seconds
  #  Examples:
  #    | selector     | text    | number | secs |
  #    | #companyname | Myworks | 2      | 3    |
  
  

@addressbook
Feature: Email Status - Is Equal/Not Equal

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Email Status Activity access to conduct the test execution
    Given Wait until the Application is ready

  #Note: Downsizing sec will cause entire scenario failure. No other tweaks worked.
  #Run script to cleanup saved smart group in database:  --DELETE from ListProfile where USERID = 1923

  @email_status
  Scenario Outline: Email Status - Equal
    Given Create Smart Group From Addressbook
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | smart_group_save | secs | number |
    | Email Status  | Equal      | Active               | ES - EQ - A      | 15   | 6      |
    | Email Status  | Equal      | Bad Email Format     | ES - EQ - BEF    | 15   | 4      |
    | Email Status  | Equal      | Blocked              | ES - EQ - B      | 15   | 1      |
    | Email Status  | Equal      | Hard Bounce          | ES - EQ - HB     | 15   | 2      |
    | Email Status  | Equal      | No Email Address     | ES - EQ - NEA    | 15   | 5      |
    | Email Status  | Equal      | Unsubscribed         | ES - EQ - UN     | 15   | 2      |

  @email_status_equal
  Scenario Outline: Email Status - Not Equal
    Given Create Smart Group From Addressbook
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | smart_group_save | secs | number |
    | Email Status  | Not Equal  | Active               | ES - NE - A      | 15   | 14     |
    | Email Status  | Not Equal  | Bad Email Format     | ES - NE - BEF    | 15   | 16     |
    | Email Status  | Not Equal  | Blocked              | ES - NE - B      | 15   | 19     |
    | Email Status  | Not Equal  | Hard Bounce          | ES - NE - HB     | 15   | 18     |
    | Email Status  | Not Equal  | No Email Address     | ES - NE - NEA    | 15   | 15     |
    | Email Status  | Not Equal  | Unsubscribed         | ES - NE - UN     | 15   | 18     |

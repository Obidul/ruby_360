@email
Feature: Go to Email tab and create Automated Email. Also perform some Navigation

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Automated Email access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #STORY BLOCK: Automated email creation , verification and deletion

  @j1
  Scenario: As admin user, Create Automated Email for the first time
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "1 Basic Header" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Joins Group
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Test AR" and select Delivery time: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I create a folder named: "New" and Autoresponder name: "Joining"
    Then I click on the start autoresponder button
    Then I should see "Automated Email: Joining" on screen

  @d1
  Scenario: As Admin user, Delete the Automated email that was created for the first time
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Joining"
    And I accept to proceed remaining scenario

  @j2
  Scenario: As admin user, Create Automated Email : Joining
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Joins Group
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Another Test AR" and select Delivery time: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Joining 2"
    Then I click on the start button
    Then I should see "Automated Email: Joining 2" on screen

  @d2
  Scenario: As Admin user, Delete the Automated email that was created for Joining
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Joining 2"
    And I accept to proceed remaining scenario

  @j3
  Scenario: As admin user, Create another Automated email from the folder edit dropdown -> Add Another Email to Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the edit button of folder : "New" and select option: "Add Another Email to Folder"
    And I wait for 5 seconds
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Joins Group
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Automated email from the folder edit dropdown" and select Delivery time: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Welcome"
    Then I click on the start button
    Then I should see "Automated Email: Welcome" on screen

  @d3
  Scenario: As Admin user, Delete the Automated email that was created from the Automated email folder edit dropdown
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Welcome"
    And I accept to proceed remaining scenario

  @j4
  Scenario: As admin user, Create Automated Email : Birthday
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Events & Invitations" category
    Then I should see "Events & Invitations" on screen
    Then I select the "3 Modern Image" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Date Event
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Birthday AR" and set Delivery time: "Hours later" & Date Trigger to: "Birthday"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Happy Birthday"
    Then I click on the start button
    Then I should see "Automated Email: Happy Birthday" on screen

  @d4
  Scenario: As Admin user, Delete the Automated email that was created for Birthday
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Happy Birthday"
    And I accept to proceed remaining scenario

  @j5
  Scenario: As admin user, Create Automated Email : Anniversary
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Holidays & Seasonal" category
    Then I should see "Holidays & Seasonal" on screen
    Then I select the "5 Hero Left" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Date Event
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Anniversary AR" and set Delivery time: "Hours later" & Date Trigger to: "Anniversary"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Happy Anniversary"
    Then I click on the start button
    Then I should see "Automated Email: Happy Anniversary" on screen

  @d5
  Scenario: As Admin user, Delete the Automated email that was created for Anniversary
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Happy Anniversary"
    And I accept to proceed remaining scenario

  @j6
  Scenario: As admin user, Create Automated Email : Email Activity
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Newsletters" category
    Then I should see "Newsletters" on screen
    Then I select the "4 Dateline" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Email Activity
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Congratulation!" and set Campaign to: "Link email" Link to: "http://www.nasa.gov" & Date Trigger to: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Link Trigger"
    Then I click on the start button
    Then I should see "Automated Email: Link Trigger" on screen

  @d6
  Scenario: As Admin user, Delete the Automated email that was created for Email Activity
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Link Trigger"
    And I accept to proceed remaining scenario

  @j7
  Scenario: As admin user, Create Automated Email : Add Another (Green Button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the folder : "New"
    And I wait until "#arAddAnother" is visible
    Then I clicked on the Add Another Green button
    And I wait for 5 seconds
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Joins Group
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Automated email from the Add Another (Green Button)" and select Delivery time: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Add Another (Green Button)"
    Then I click on the start button
    Then I should see "Automated Email: Add Another (Green Button)" on screen

  @d7
  Scenario: As Admin user, Delete the Automated email that was created from the Add Another (Green Button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Add Another (Green Button)"
    And I accept to proceed remaining scenario

  @j8
  Scenario: As admin user, Create Automated Email from Let�s get started! link
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    Then I click on "Let�s get started!" link
    And I wait for 5 seconds
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Next button
    Then I should see "Delivery Setup" on screen
    And I choose automated email type: Joins Group
    Then I should see "( Which group do you want to use? )" on screen
    Then I select "Automated Email" group from the group list
    And I fillup the subject: "Automated email from the Let�s get started link" and select Delivery time: "Hours later"
    Then I click the Save button on the Delevery Setup page
    And I type Autoresponder name: "Let�s get started link"
    Then I click on the start button
    Then I should see "Automated Email: Let�s get started link" on screen

  @d8
  Scenario: As Admin user, Delete the Automated email that was created from the Add Another (Green Button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I click on the delete button of the autoresponder named: "Automated Email: Let�s get started link"
    And I accept to proceed remaining scenario

  #STORY BLOCK: Automated email navigation from inside automated process

  @n1
  Scenario: As admin user, Navigation Automated Email : Save Draft
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Save Draft button
    Then I save the template as a draft
    And I wait for 3 seconds

  @d6
  Scenario: As admin user, Delete the template from the Draft
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    Then I should see "Drafts" on screen
    Then I want to delete template: "2 Text Header"
    And I wait for 2 seconds

  @n2
  Scenario: As admin user, Navigation Automated Email : Preview
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Preview button
    And I wait for 2 seconds

  @n3
  Scenario: As admin user, Navigation Automated Email : Cancel
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the Cancle button
    Then I should see "Select a Design" on screen

  @n4
  Scenario: As admin user, Navigation Automated Email : Background Color
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I change the background color to Green
    And I wait for 3 seconds

  @n5
  Scenario: As admin user, Navigation Automated Email : View Source Code
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the view source code button
    And I wait for 3 seconds

  @n6
  Scenario: As admin user, Automated Emails Folder -> Cancel
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the folder : "New"
    And I wait until "#arAddAnother" is visible
    Then I clicked on the autoresponder cancel button
    Then I should see "Select a Design" on screen

  #STORY BLOCK: Automated email Folder Edit Dropdown navigation

  @n7
  Scenario: As admin user, Automated Emails Folder Edit Dropdown : Add Another Email to Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the edit button of folder : "New" and select option: "Add Another Email to Folder"
    And I wait for 5 seconds
    Then I should see "Create an Automated Email" on screen

  @n8
  Scenario: As admin user, Automated Emails Folder Edit Dropdown : Rename Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the edit button of folder : "New" and select option: "Rename Folder"
    Then I rename the folder name to : "New 2"
    Then I should see "New 2" on screen

  @last
  Scenario: As admin user, Automated Emails Folder Edit Dropdown : Delete Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I clicked on the edit button of folder : "New 2" and select option: "Delete Folder"
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  # Bulk automated email creation

#  @bulk1
#  Scenario Outline: As admin user, Create Automated Email : Joining
#    And I wait for <secs> seconds
#    Then I click on Email Tab
#    And I wait for <secs> seconds
#    Then I want to create Automated Email
#    And I click on the "<category>" category
#    Then I select the "<template_name>" template
#    And I wait for <secs> seconds
#    Then I click on the Next button
#    And I choose automated email type: Joins Group
#    Then I select "<selector>" group from the group list
#    And I fillup the subject: "<subject>" and select Delivery time: "<time>"
#    Then I click the Save button on the Delevery Setup page
#    And I type Autoresponder name: "<ar_name>"
#    Then I click on the start button
#    And I wait for <secs> seconds
#    Examples:
#      | secs | category | template_name | selector | subject | time        | ar_name |
#      | 3    | Birthday | 2 Text Header | Join     | Join 1  | Hours later | Join 1  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 2  | Hours later | Join 2  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 3  | Hours later | Join 3  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 4  | Hours later | Join 4  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 5  | Hours later | Join 5  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 6  | Hours later | Join 6  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 7  | Hours later | Join 7  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 8  | Hours later | Join 8  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 9  | Hours later | Join 9  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 10  | Hours later | Join 10  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 11  | Hours later | Join 11  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 12  | Hours later | Join 12  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 13  | Hours later | Join 13  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 14  | Hours later | Join 14  |
#      | 3    | Birthday | 2 Text Header | Join     | Join 15  | Hours later | Join 15  |
#
#  @bulk2
#  Scenario Outline: As admin user, Create Automated Email : Birthday
#    And I wait for <secs> seconds
#    Then I click on Email Tab
#    And I wait for <secs> seconds
#    Then I want to create Automated Email
#    And I click on the "<category>" category
#    Then I select the "<template_name>" template
#    And I wait for <secs> seconds
#    Then I click on the Next button
#    And I choose automated email type: Date Event
#    Then I select "<selector>" group from the group list
#    And I fillup the subject: "<subject>" and set Delivery time: "<time>" & Date Trigger to: "<date_trigger>"
#    Then I click the Save button on the Delevery Setup page
#    And I type Autoresponder name: "<ar_name>"
#    Then I click on the start button
#    And I wait for <secs> seconds
#    Examples:
#      | secs | category             | template_name  | selector | subject | time        | date_trigger | ar_name |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 1   | Hours later | Birthday     | HBD 1   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 2   | Hours later | Birthday     | HBD 2   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 3   | Hours later | Birthday     | HBD 3   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 4   | Hours later | Birthday     | HBD 4   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 5   | Hours later | Birthday     | HBD 5   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 6   | Hours later | Birthday     | HBD 6   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 7   | Hours later | Birthday     | HBD 7   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 8   | Hours later | Birthday     | HBD 8   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 9   | Hours later | Birthday     | HBD 9   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 10   | Hours later | Birthday     | HBD 10   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 11   | Hours later | Birthday     | HBD 11   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 12   | Hours later | Birthday     | HBD 12   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 13   | Hours later | Birthday     | HBD 13   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 14   | Hours later | Birthday     | HBD 14   |
#      | 3    | Events & Invitations | 3 Modern Image | HBD      | HBD 15   | Hours later | Birthday     | HBD 15   |
#
#  @bulk3
#  Scenario Outline: As admin user, Create Automated Email : Anniversary
#    And I wait for <secs> seconds
#    Then I click on Email Tab
#    And I wait for <secs> seconds
#    Then I want to create Automated Email
#    And I click on the "<category>" category
#    Then I select the "<template_name>" template
#    And I wait for <secs> seconds
#    Then I click on the Next button
#    And I choose automated email type: Date Event
#    Then I select "<selector>" group from the group list
#    And I fillup the subject: "<subject>" and set Delivery time: "<time>" & Date Trigger to: "<date_trigger>"
#    Then I click the Save button on the Delevery Setup page
#    And I type Autoresponder name: "<ar_name>"
#    Then I click on the start button
#    And I wait for <secs> seconds
#    Examples:
#      | secs | category            | template_name | selector | subject       | time        | date_trigger | ar_name       |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 1 | Hours later | Anniversary  | Anniversary 1 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 2 | Hours later | Anniversary  | Anniversary 2 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 3 | Hours later | Anniversary  | Anniversary 3 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 4 | Hours later | Anniversary  | Anniversary 4 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 5 | Hours later | Anniversary  | Anniversary 5 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 6 | Hours later | Anniversary  | Anniversary 6 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 7 | Hours later | Anniversary  | Anniversary 7 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 8 | Hours later | Anniversary  | Anniversary 8 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 9 | Hours later | Anniversary  | Anniversary 9 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 10 | Hours later | Anniversary  | Anniversary 10 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 11 | Hours later | Anniversary  | Anniversary 11 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 12 | Hours later | Anniversary  | Anniversary 12 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 13 | Hours later | Anniversary  | Anniversary 13 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 14 | Hours later | Anniversary  | Anniversary 14 |
#      | 3    | Holidays & Seasonal | 5 Hero Left   | Anni     | Anniversary 15 | Hours later | Anniversary  | Anniversary 15 |
#
#  @bulk4
#  Scenario Outline: As admin user, Create Automated Email : Email Activity
#    And I wait for <secs> seconds
#    Then I click on Email Tab
#    And I wait for <secs> seconds
#    Then I want to create Automated Email
#    And I click on the "<category>" category
#    Then I select the "<template_name>" template
#    And I wait for <secs> seconds
#    Then I click on the Next button
#    And I choose automated email type: Email Activity
#    Then I select "<selector>" group from the group list
#    And I fillup the subject: "<subject>" and set Campaign to: "<campaign>" Link to: "<link>" & Date Trigger to: "<time>"
#    Then I click the Save button on the Delevery Setup page
#    And I type Autoresponder name: "<ar_name>"
#    Then I click on the start button
#    And I wait for <secs> seconds
#    Examples:
#      | secs | category    | template_name | selector | subject           | campaign | link                  | time        | ar_name |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 1! | Link     | http://www.google.com | Hours later | Link 1  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 2! | Link     | http://www.google.com | Hours later | Link 2  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 3! | Link     | http://www.google.com | Hours later | Link 3  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 4! | Link     | http://www.google.com | Hours later | Link 4  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 5! | Link     | http://www.google.com | Hours later | Link 5  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 6! | Link     | http://www.google.com | Hours later | Link 6  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 7! | Link     | http://www.google.com | Hours later | Link 7  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 8! | Link     | http://www.google.com | Hours later | Link 8  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 9! | Link     | http://www.google.com | Hours later | Link 9  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 10! | Link     | http://www.google.com | Hours later | Link 10  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 11! | Link     | http://www.google.com | Hours later | Link 11  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 12! | Link     | http://www.google.com | Hours later | Link 12  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 13! | Link     | http://www.google.com | Hours later | Link 13  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 14! | Link     | http://www.google.com | Hours later | Link 14  |
#      | 3    | Newsletters | 4 Dateline    | Trigger  | Congratulation 15! | Link     | http://www.google.com | Hours later | Link 15  |
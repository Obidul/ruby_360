@social
Feature: Social Post Character Counter Min/Max Verification
  Social Post Counter of Twitter
  Social Post Counter of Facebook
  Social Post Counter of Linkedin
  Social Post Queue Counter Number Default

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Social Module access to conduct the test execution
    Given Wait until the Application is ready

 #STORY BLOCK:SOCIAL MIN/MAX CHARACTER LIMIT TEST COVERAGE

  @static_counter
  Scenario: Message Queue Counter Updates -- Verification
    Given Go for Social Module
    And I should see in Message Queue : 0

  #STORY BLOCK: Default Selected

  @twitter
  Scenario: Limit Counter Twitter
    Given I wait for 30 seconds
    Given Go for Social Module
    And I select twitter for social post
    Then I wait for 1 seconds
    And I should see in Limit Counter : 140

  @linkedin
  Scenario: Limit Counter Linkedin
    Given I wait for 30 seconds
    Given Go for Social Module
    And I select linkedin for social post
    Then I wait for 1 seconds
    And I should see in Limit Counter : 700

  #STORY BLOCK: Single Selected

  Scenario: Limit Counter Twitter with value
    Given I wait for 30 seconds
    Given Go for Social Module
    And Select Twitter and Fill "Limit Counter Twitter Post"
    Then I wait for 1 seconds
    And I should see in Limit Counter : 114

  Scenario: Limit Counter Linkedin with value
    Given I wait for 30 seconds
    Given Go for Social Module
    And Select Twitter and Fill "Limit Counter Linkedin"
    Then I wait for 1 seconds
    And I should see in Limit Counter : 678

  #STORY BLOCK: Combined Selected

  Scenario: Limit Counter Twitter & Linkedin
    Given I wait for 30 seconds
    Given Go for Social Module
    And Select Twitter & Linkedin Profile and Fill "twitter and linkedin selected"
    Then I wait for 1 seconds
    And I should see in Limit Counter : 111

  @limit
  Scenario: Limit Counter Twitter + Linkedin + Facebook
    Given I wait for 30 seconds
    Given Go for Social Module
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "facebook and twitter and linkedin selected"
    Then I wait for 1 seconds
    And I should see in Limit Counter : 98

#SOCIAL MIN/MAX CHARACTER LIMIT TEST COVERAGE

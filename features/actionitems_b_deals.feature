@action_items
Feature: Dashboard - Deal

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Deals access to conduct the test execution
#   Given Local Test User Account
    Given Wait until the Application is ready
#    Given Allow to close browser or tab anytime

  #Note: Tags are important.
  # Related test scenario data should be as well matched.

  #STORY BLOCK: Default

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Create A Deal (Default Time)
    And I wait for 3 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Gold" & Price "$130.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I should see Price: "$130.00" Product & Company: "Gold - Abata" and Sales Stage: "Proposal" activity has enlisted

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Add a Comment
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$177.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Ja" & select "James Bennett, Gabcube" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$177.00" and Product & Company: "Apple - Gabcube"
    And I want to comments on deals activities of "A In in Deal"
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Edit Sales Stage From Drop-down
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I wait for 5 seconds
    And I Create Quick Deal By selecting Product "Diamond" & Price "$135.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Nico" & select "Nicole Morrison, Cogibox" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$135.00" & Product&Company: "Diamond - Cogibox" & Sale Stage "Proposal" & then Change Sales Stage to: "Negotiations"
    And I wait for 20 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Deals Load in Details View
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Gold" & Price "$190.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Mild" & select "Mildred Perez, Kwideo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$190.00" and Product & Company: "Gold - Kwideo" shows in Details view
    And I should be in Details View
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Delete a Deal from Dropdown menu
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Gold" & Price "$166.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Je" & select "Jennifer Hunter, Gigazoom" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$166.00" and Product & Company: "Gold - Gigazoom"
    And I select "Delete" from Drop-down menu
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab - Grid List - Deal Stage Changed - Verification
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I wait for 5 seconds
    And I Create Quick Deal By selecting Product "Gold" & Price "$123.00" & Lead Source "Email Marketing" & Sales Stage "Negotiations"
    And I searched for "Matt" & select "Matthew Williamson, Buzzdog" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$123.00" & Product&Company: "Gold - Buzzdog" & Sale Stage "Negotiations" & then Change Sales Stage to: "Order/Contract"
    And I wait for 3 seconds
    And I should see Sales Stage has changed to "Order/Contract"
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Inline Edit Comments
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$199.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Mi" & select "Mildred Perez, Kwideo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$199.00" and Product & Company: "Apple - Kwideo"
    And I want to comments on deals activities of "A Comments in Deal"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing comments "A Comments in Deal" and clicked on comments icon and fill in "Inline Comments Edited - in Deal"
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Existing Comments Updated - Verification
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$199.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Pa" & select "Pamela Phillips, Leexo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found Price: "$199.00" and Product & Company: "Apple - Leexo"
    And I want to comments on deals activities of "Inline Comments on activity"
    And I click to Save button
    And I wait for 5 seconds
    And I found existing comments "Inline Comments on activity" and clicked on comments icon and fill in "Inline Comments Updated - in Deal"
    And I click to Save button
    And I wait for 5 seconds
    And I should see "Inline Comments Updated - in Deal" comment has enlisted
    And I wait for 2 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab ->  Existing Deals 4 Field Updated (Product Name, Price, Lead Source, & Sales Stage)
    And I wait for 3 seconds
    And I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$799.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Ke" & select "Kenneth Tucker, Motherboard" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I Update existing Deal with Price "$799.00" & Product & Company "Apple - Motherboard"
    And I Update Quick Deal By selecting Product "Orange" & Price "$899.00" & Lead Source "Mailing" & Sales Stage "Order/Contract"
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Add Reminder Clock Selected in Deals
    And I wait for 3 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Gold" & Price "$250.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I searched for "Ge" & select "Gerald Marshall, Quaxo" from the list
    And I select Reminder Clock
    And I click to Save button
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Specific Deal Activity has Completed
    And I wait for 3 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Gold" & Price "$133.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found deal activity with Product & Company: "Gold - Abata" and select Radio button
    And I wait for 5 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> As Deals activity Completed Status - Verification from History
    And I wait for 3 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$187.00" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "Do" & select "Dorothy George, Shufflester" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found deal activity with Product & Company: "Apple - Shufflester" and select Radio button
    And I switch to History to see Completed Items
    And I wait for 5 seconds
    And I should see Price: "$187.00" Product & Company: "Apple - Shufflester" and Sales Stage: "Won" activity has enlisted
    And I wait for 5 seconds

  #STORY BLOCK: WON/LOST

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Create a Deal as Won
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    Then I Create Quick Deal By selecting Product "Processor" & Price "$100.00" & Lead Source "Website" & Sales Stage "Won"
    And I searched for "Ge" & select "Gerald Marshall, Quaxo" from the list
    Then I click to Save button
    And I wait for 5 seconds
    Then I switch to History to see Completed Items
    Then I should see Price: "$100.00" Product & Company: "Processor - Quaxo" and Sales Stage: "Won" activity has enlisted
    And I wait for 2 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Create a Deal as Lost
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    Then I Create Quick Deal By selecting Product "Apple" & Price "$200.00" & Lead Source "Employee Referral" & Sales Stage "Lost"
    And I searched for "Jo" & select "Joe Porter, Quaxo" from the list
    Then I click to Save button
    And I wait for 5 seconds
    Then I switch to History to see Completed Items
    Then I should see Price: "$200.00" Product & Company: "Apple - Quaxo" and Sales Stage: "Lost" activity has enlisted
    And I wait for 2 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Marked Deal as Completed (Won)
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    Then I Create Quick Deal By selecting Product "Gold" & Price "$300.00" & Lead Source "Print Ad" & Sales Stage "Prospect"
    And I searched for "Dor" & select "Dorothy George, Shufflester" from the list
    Then I click to Save button
    And I wait for 5 seconds
    Then I found existing Deals : "$300.00 Gold - Shufflester" and select Radio button
    And I switch to History to see Completed Items
    Then I should see Price: "$300.00" Product & Company: "Gold - Shufflester" and Sales Stage: "Won" activity has enlisted
    And I wait for 2 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Change Sales Stage From Drop-down to Won
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    Then I Create Quick Deal By selecting Product "Diamond" & Price "$400.00" & Lead Source "Mailing" & Sales Stage "Prospect"
    And I searched for "Nic" & select "Nicole Morrison, Cogibox" from the list
    Then I click to Save button
    And I wait for 5 seconds
    And I found Price: "$400.00" & Product&Company: "Diamond - Cogibox" & Sale Stage "Prospect" & then Change Sales Stage to: "Won"
    And I wait for 2 seconds
    And I switch to History to see Completed Items
    Then I should see Price: "$400.00" Product & Company: "Diamond - Cogibox" and Sales Stage: "Won" activity has enlisted
    And I wait for 2 seconds

  Scenario: As admin user, Secondary Action List -> Deals Tab -> Change Sales Stage From Drop-down to Lost
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    Then I Create Quick Deal By selecting Product "Motherboard" & Price "$500.00" & Lead Source "Tradeshow" & Sales Stage "Negotiations"
    And I searched for "Lau" & select "Laura Taylor, Shuffledrive" from the list
    Then I click to Save button
    And I wait for 5 seconds
    And I found Price: "$500.00" & Product&Company: "Motherboard - Shuffledrive" & Sale Stage "Negotiations" & then Change Sales Stage to: "Lost"
    And I wait for 2 seconds
    And I switch to History to see Completed Items
    Then I should see Price: "$500.00" Product & Company: "Motherboard - Shuffledrive" and Sales Stage: "Lost" activity has enlisted
    And I wait for 2 seconds

# Deal won / lost added on (19th July 2016) -- End

#STORY BLOCK: EDIT/ADD COMMENTS

  @dropdown_deals_as
  Scenario: As Admin User, Action Stream -> In Deals Tab -> Dropdown menu -> Edit Deal
    Given I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$899.00" & Lead Source "Mailing" & Sales Stage "Proposal"
    And I searched for "Vic" & select "Victor Lawrence, Topicshots" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$899.00" Product & Company: "Apple - Topicshots" and Sales Stage: "Proposal" and select "Edit Deal" from drop-down menu -- In Action Stream
    And I wait for 2 seconds
    And I Update Quick Deal By selecting Product "Orange" & Price "$999.00" & Lead Source "Employee Referral" & Sales Stage "Order/Contract"
    And I click to Save button
    And I wait for 5 seconds

  @dropdown_deals_as
  Scenario: As Admin User, Action Stream -> In Deals Tab -> Dropdown menu -> Add Comment
    Given I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$879.00" & Lead Source "Print Ad" & Sales Stage "Negotiations"
    And I searched for "Joa" & select "Joan Nichols, Topicshots" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$879.00" Product & Company: "Apple - Topicshots" and Sales Stage: "Negotiations" and select "Add Comment" from drop-down menu -- In Action Stream
    And I wait for 2 seconds
    And I comments on deals and fill in "Add Comments in Deals - From Dropdown" -- From Dropdown menu
    And I wait for 5 seconds
    And I should see "Add Comments in Deals - From Dropdown" comment has enlisted

  @dropdown_deals_as
  Scenario: As Admin User, Action Stream -> In Deals Tab -> Dropdown menu -> Email Contact
    Given I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$480.00" & Lead Source "Employee Referral" & Sales Stage "Needs Analysis"
    And I searched for "Joe" & select "Joe Kennedy, Trudeo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$480.00" Product & Company: "Apple - Trudeo" and Sales Stage: "Needs Analysis" and select "Email Contact" from drop-down menu -- In Action Stream
    And I wait for 5 seconds
    And I wait until "#to" is visible

  @dropdown_deals_as
  Scenario: As Admin User, Action Stream -> In Deals Tab -> Dropdown menu -> View Contact
    Given I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$423.00" & Lead Source "Email Marketing" & Sales Stage "Order/Contract"
    And I searched for "Geor" & select "George Cox, Twitterbeat" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$423.00" Product & Company: "Apple - Twitterbeat" and Sales Stage: "Order/Contract" and select "View Contact" from drop-down menu -- In Action Stream
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @dropdown_deals_as
  Scenario: As Admin User, Action Stream -> In Deals Tab -> Dropdown menu -> Delete
    Given I wait for 5 seconds
    And I select Deals -- while in Action Stream
    And I Create Quick Deal By selecting Product "Apple" & Price "$455.00" & Lead Source "Tradeshow" & Sales Stage "Prospect"
    And I searched for "Deni" & select "Denise Robinson, Twimbo" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$455.00" Product & Company: "Apple - Twimbo" and Sales Stage: "Prospect" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

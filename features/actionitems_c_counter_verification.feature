@action_items
Feature: Action Stream/ Dashboard - Counter Verification
  Deals -- Counter Verification
  Addressbook -- Counter Verification

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Counter Updates Verification - Access to conduct the test execution
    Given Wait until the Application is ready

#Note:
#In left body, we add some demo contents in all aspects where counter changes or updates.
#Then we match in details view and right counter.

  #STORY BLOCK: Primary Action List

  @today
  Scenario: Primary Action List -> Today
    Given I am on the "/App" page
    And I create an activity with "Meeting"
    And I select activity type "Meeting"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Today activity and Today counter matched

  @starred
  Scenario: Primary Action List -> Starred
    Given I am on the "/App" page
    And I select Starred from Primary Right Panel
    And I create an activity with "Call"
    And I select activity type "Call"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Starred Activity and Starred Counter matched

  @overdue
  Scenario: Primary Action List -> Overdue
    Given I am on the "/App" page
    And I select Overdue from Primary Right Panel
    And I create activity of "Overdue" and with custom time: "12:15 PM"
    And I select activity type "Task"
    And I select Previous Date - Yesterday
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
#   And I scroll to find the content
    Then Number of Overdue Activity and Overdue Counter matched

  #STORY BLOCK: Secondary Action List

  @total
  Scenario: Secondary Action List -> Activity Tab -> All Activity (Total)
    Given I am on the "/App" page
    And I select All Activity Under Activity Tab
    And I create an activity with "All Activity Todo"
    And I select activity type "Meeting"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Total Activity and All Activity Counter matched

  @meeting
  Scenario: Secondary Action List -> Activity Tab -> Meeting
    Given I am on the "/App" page
    And I select Meeting Under Activity Tab
    And I create an activity with "Meeting From Activity Tab"
    And I select activity type "Meeting"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Meeting Activity and Meeting Counter matched

  @calls
  Scenario: Secondary Action List -> Activity Tab -> Calls
    Given I am on the "/App" page
    And I select Calls Under Activity Tab
    And I create an activity with "Call From Activity Tab"
    And I select activity type "Call"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Calls Activity and Calls Counter matched

  @tasks
  Scenario: Secondary Action List -> Activity Tab -> Tasks
    Given I am on the "/App" page
    And I select Tasks Under Activity Tab
    And I create an activity with "Task From Activity Tab"
    And I select activity type "Task"
    And I searched for "Tasa" & select "Ashley Jordan, Tasawr Interactive" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Tasks Activity and Tasks Counter matched

  @todo
  Scenario: Secondary Action List -> Activity Tab -> To Do
    Given I am on the "/App" page
    And I select Todo Under Activity Tab
    And I create activity of Todo with "Todo Demo"
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Todo Activity and Todo Counter matched

 #STORY BLOCK: SCENARIO CREATED AT FIRST -> THEN VERIFICATION OF COUNTER

 #STORY BLOCK: STATUS

  @status
    Scenario: As Admin, Status Menu - New -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select New from Status - while in Action Stream module
    Then Number of Status Activity and selected New Status Counter matched

  @status
  Scenario: As Admin, Status Menu - Attempted Contact -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select Attempted from Status - while in Action Stream module
    Then Number of Status Activity and selected Attempted Status Counter matched

  @status
  Scenario: As Admin, Status Menu - Contacted -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select Contacted from Status - while in Action Stream module
    Then Number of Status Activity and selected Contacted Status Counter matched

  @status
  Scenario: As Admin, Status Menu - Client -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select Client from Status - while in Action Stream module
    And Number of Status Activity and selected Client Status Counter matched

  @status
  Scenario: As Admin, Status Menu - Hot Prospect -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select Hot Prospect from Status - while in Action Stream module
    And Number of Status Activity and selected Hot Prospect Status Counter matched

  @status
  Scenario: As Admin, Status Menu - Disqualified -> Counter updated
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select Disqualified from Status - while in Action Stream module
    And Number of Status Activity and selected Disqualified Status Counter matched

 #STORY BLOCK: TEAM

  @team
  Scenario: Contact Action List -> Team -> Account 1
    Given I am on the "/App" page
    And I wait for 10 seconds
    And I select "Mark Andy" Under Team Menu
    And I create an activity with "From Team Drop-down to select"
    And I select activity type "Call"
    And I searched for "Kim" & select "Kimberly Sims" from the list
    And I click to Save button
    And I wait for 5 seconds
    And Number of Team Activity and selected Team Counter matched

  #STORY BLOCK: DEALS

  @deals
  Scenario: Secondary Action List -> Deals Menu -> All Deals (Total)
    Given I am on the "/App" page
    And I select All Deals Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$130" & Lead Source "Email Marketing" & Sales Stage "Prospect"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Deals and All Deals Counter matched

  @prospect
  Scenario: Secondary Action List -> Deals Menu -> Prospect
    Given I am on the "/App" page
    And I select Proposal Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$130" & Lead Source "Email Marketing" & Sales Stage "Proposal"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Proposal and Proposal Counter matched

  @needs_analysis
  Scenario: Secondary Action List -> Deals Menu -> Needs Analysis
    Given I am on the "/App" page
    And I select Needs Analysis Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$130" & Lead Source "Email Marketing" & Sales Stage "Needs Analysis"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Needs Analysis and Total counter matched

  @negotiations
  Scenario: Secondary Action List -> Deals Menu -> Negotiations
    Given I am on the "/App" page
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$130" & Lead Source "Email Marketing" & Sales Stage "Negotiations"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Negotiations and Negotiations Counter matched

  @order_contract
  Scenario: Secondary Action List -> Deals Menu -> Order/Contract
    Given I am on the "/App" page
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$130" & Lead Source "Email Marketing" & Sales Stage "Order/Contract"
    And I searched for "E" & select "Eric Harris, Abata" from the list
    And I click to Save button
    And I wait for 5 seconds
    Then Number of Order-Contract and Order-Contract Counter matched

 #STORY BLOCK: TAGS & FILTER

  #  Scenario: Secondary Action List -> Tags Tab -> Tags
  #  Scenario: Secondary Action List -> Filter Tab -> Filter

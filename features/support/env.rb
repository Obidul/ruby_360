require 'capybara'
require 'capybara/cucumber'
# require 'capybara/poltergeist'
require 'selenium-webdriver'
require 'capybara-screenshot/cucumber'

#Error screenshot saver
Capybara.save_and_open_page_path = 'features/support/screenshot'

# Capybara.ignore_hidden_elements = false

# off # Capybara.default_driver = :selenium
#pass environment variables to control which browser is used for testing. Default is HEADLESS/POLTERGEIST
#usage: firefox=true bundle exec cucumber features/test.feature

#Required to handle dirty or clean state

After do
  Capybara.current_session.driver.quit
end

#Social Module

# After do
#   #Twitter Inactive
#   find(:xpath, ".//*[@id='divTwitterChangeProfile']").click
#   find(".disableButton").click
#   sleep 0.2
#
#   #Facebook Inactive
#   find(:xpath, ".//*[@id='divFacebookChangeProfile']").click
#   find(".disableButton").click
#   sleep 0.2
#
#   #Linkedin Inactive
#   find(:xpath, ".//*[@id='divLinkedinChangeProfile']").click
#   find(".disableButton").click
#   sleep 0.2
# end
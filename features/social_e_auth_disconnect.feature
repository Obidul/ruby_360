@social
Feature: Disconnect all social networks

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Social Auth Enable & Disable - Access to conduct the test execution
    Given I am logged in with "username" and "password"

  Scenario: As Admin, I want to disable twitter social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    Given I want to disable twitter social account
    And I confirm popup
    And I wait for 3 seconds

  Scenario: As Admin, I want to disable linkedin social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    Given I want to disable linkenin social account
    And I confirm popup
    And I wait for 3 seconds

  Scenario: As Admin, I want to disable facebook social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    Given I want to disable facebook social account
    And I confirm popup
    And I wait for 3 seconds


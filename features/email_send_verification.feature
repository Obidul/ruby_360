@email
Feature: Email send verification

  @v1
  Scenario: As admin user, Email Toolbar: Send (Campaign) - Verification
    Then Goto recipient email to verify received email campaign

  @v2
  Scenario: As admin user, Email toolbar: Preview - Desktop View - Send Test Email - Verification
    Then Goto recipient email to verify Preview - Desktop View - Send Test Email

  @v3
  Scenario: As admin user, Email toolbar: Preview - Mobile View - Send Test Email - Verification
    Then Goto recipient email to verify Preview - Mobile View - Send Test Email
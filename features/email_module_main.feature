@email
Feature: Email Module

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Email Module access to conduct the test execution
    Given Wait until the Application is ready

#EMAIL MODULE START

    Scenario:
Direct Single Email	Sent direct single email to one/more contact/non contact recipient
Campaign Email	Sent direct campaign email to one/more contact/non contact recipient
Schedule Campaign	Scheduled direct campaign email to one/more contact/non contact recipient


CC, BCC	CC testing with contact & non contact recipient for single email
BCC testing with contact & non contact recipient for single email
CC & BCC testing with contact & non contact recipient for single email
CC & BCC testing with contact & non contact recipient for campaign email

Attachment	Single email
Campaign email
One/more non contact recipient with CC for single email.
One/more non contact recipient with BCC for single email.
One/more contact recipient with CC for single email.
One/more contact recipients with BCC for single email.
One/more contact recipient with CC & BCC for single email.
One/more non contact recipient with CC & BCC for single email.
Campaign to a group with one/more contact and one/more non contact recipient.


Preview	Add Merge fields and check in preview
Assign link and check in preview
Edit (change font size, change font style, bold, italic, underline, text color, block background color etc) in the email editor and check in preview

#EMAIL MODULE END
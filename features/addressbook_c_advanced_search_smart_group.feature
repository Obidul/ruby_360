@addressbook
Feature: Advanced search
  Advanced Search with

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Advanced Search activity access to conduct the test execution
  # Given Local Test User Account
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #Read Notes: Under Steps Definitions

  #STORY BLOCK: TC-SG1 - TC-SG6 (Excluding Is Blank & Is Not Blank)

  @company
  Scenario Outline: Company - Is Equal To/Begins With/Contains/Does not Contain
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input | smart_group_save | secs | number |
    | Company       | Is Equal To      | Abata      | COM - IET        | 5    | 6      |
    | Company       | Begins With      | Br         | COM - BW         | 5    | 2      |
    | Company       | Contains         | le         | COM - CON        | 5    | 4      |
    | Company       | Does not Contain | le         | COM - DNC        | 5    | 16     |

  #STORY BLOCK: TC-SG7 - TC-SG12 (Excluding Is Blank & Is Not Blank)

  @contact
  Scenario Outline: Contact - Is Equal To/Begins With/Contains/Does not Contain
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input | smart_group_save | secs | number |
    | Contact       | Is Equal To      | Ja         | CON - IET        | 5    | 2      |
    | Contact       | Begins With      | D          | CON - BW         | 5    | 4      |
    | Contact       | Contains         | Co         | CON - CON        | 5    | 3      |
    | Contact       | Does not Contain | Ja         | CON - DNC        | 5    | 18     |

  #STORY BLOCK: TC-SG13- TC-SG18 (Excluding Is Blank & Is Not Blank)

  @email
  Scenario Outline: Email - Is Equal To/Begins With/Contains/Does not Contain
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input           | smart_group_save | secs | number |
    | Email         | Is Equal To      | sakibqa+26@gmail.com | EM - IET         | 10   | 1      |
    | Email         | Begins With      | sakibqa              | EM - BW          | 10   | 13     |
    | Email         | Contains         | qa                   | EM - CON         | 10   | 13     |
    | Email         | Does not Contain | qa                   | EM - DNC         | 10   | 7      |

  #Anniversary, Birthday, Create Date, Modify Date
  #STORY BLOCK: TC-SG60 - TC-SG63

  @anniversary
  Scenario Outline: Anniversary - Is Equal To/Before/After
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | smart_group_save | secs | number |
    | Anniversary   | Is Equal To | 04/03      | ANN - IET        | 10   | 4      |
    | Anniversary   | Before      | 04/03      | ANN - BEF        | 10   | 4      |
    | Anniversary   | After       | 01/01      | ANN - AFT        | 10   | 16     |

  @birthday
  Scenario Outline: Birthday - Is Equal To/Before/After
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | smart_group_save | secs | number |
    | Birthday      | Is Equal To | 07/06      | BIR - IET        | 10   | 4      |
    | Birthday      | Before      | 06/06      | BIR - BEF        | 10   | 7      |
    | Birthday      | After       | 08/08      | BIR - AFT        | 10   | 2      |

  #Account Create Date & Modify Date

  @create_date
  Scenario Outline: Create Date - Is Equal To/Before/After
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | smart_group_save | secs | number |
    | Create Date   | Is Equal To | 05/16/2016 | CD - IET         | 10   | 20     |
    | Create Date   | Before      | 05/14/2016 | CD - BEF         | 10   | 1      |
    | Create Date   | After       | 03/08/2016 | CD - AFT         | 10   | 20     |

  @modify_date
  Scenario Outline: Modify Date - Is Equal To/Before/After
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | smart_group_save | secs | number |
    | Modify Date   | Is Equal To | 05/13/2016 | MO - IET         | 10   | 1      |
    | Modify Date   | Before      | 08/08/2016 | MO - BEF         | 10   | 1      |
    | Modify Date   | After       | 02/08/2016 | MO - AFT         | 10   | 1      |

  #STORY BLOCK: @Anniversary @Birthday @Create Date @Modify Date

  @between_coverage
  Scenario Outline: Birthday/Anniversary/Create Date/Modify Date - Between
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in Start Date "<start_mm_dd_yy>" and End Date "<end_mm_dd_yy>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | start_mm_dd_yy | end_mm_dd_yy | smart_group_save | secs | number |
    | Birthday      | Between    | 05/05          | 05/05        | BIR - BE         | 10   | 20     |
    | Anniversary   | Between    | 05/05          | 05/05        | ANN - BE         | 10   | 20     |
    | Modify Date   | Between    | 05/13/2016     | 05/14/2016   | MO - BE          | 10   | 20     |
    | Create Date   | Between    | 05/14/2016     | 05/16/2016   | CD - BE          | 10   | 20     |

  @accountmanager
  Scenario Outline: Acct.Manager - Equal/Not Equal
    Given Create Smart Group From Addressbook
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | smart_group_save | secs | number |
    | Acct. Manager | Equal      | Gen. Manager         | ACC - EQ         | 10   | 4      |
    | Acct. Manager | Not Equal  | Acc. Manager         | ACC - NE         | 10   | 15     |

  @now
  Scenario Outline: Uke
    And Go for Advanced Search
    And I wait for <secs> seconds
    And Select only Status with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | secs | conditions      | conditions_secondary | smart_group_save | number |
    | 10   | Is Equal To     | New                  | ST - EQ          | 20     |
    | 10   | Is Not Equal To | Attempted            | ST - NE          | 20     |

  #STORY BLOCK: Is Blank and Is Not Blank

  @age @lenght_of_residence @employee @passed
  Scenario Outline: Age/Length of Residence/Employee - Is Equal To/ Greater Than/ Is Not Equal To/ Less Than
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field       | conditions      | user_input | smart_group_save | secs | number |
    | Age                 | Is Equal To     | 61         | AGE - IET        | 10   | 3      |
    | Age                 | Greater Than    | 30         | AGE - GT         | 10   | 20     |
    | Length of Residence | Is Not Equal To | 12         | LOR - INET       | 10   | 19     |
    | Employees           | Less Than       | 50         | EMP - LT         | 10   | 8      |

  #STORY BLOCK: Is Blank and Is Not Blank

  @blank @is_not_blank
  Scenario Outline: Company - Is Blank/Is Not Blank
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field         | conditions   | smart_group_save | secs | number |
    | Company               | Is Blank     | COM - IB         | 10   | 1      |
    | Company               | Is not Blank | COM - INB        | 10   | 19     |
    | Contact               | Is Blank     | CON - IB         | 10   | 1      |
    | Contact               | Is not Blank | CON - INB        | 10   | 19     |
    | Email                 | Is Blank     | EM - IB          | 10   | 6      |
    | Email                 | Is not Blank | EM - INB         | 10   | 14     |
    | Fax                   | Is Blank     | FAX - IB         | 10   | 1      |
    | State                 | Is Blank     | STAT - IB        | 10   | 2      |
    | Zip                   | Is not Blank | ZIP - INB        | 10   | 17     |
    | Annual Sales          | Is Blank     | AS - IB          | 10   | 2      |
    | Year Business Started | Is not Blank | YBS - INB        | 10   | 18     |
    | Own or Rent           | Is Blank     | OR - IB          | 10   | 1      |
    | Building Structure    | Is not Blank | BS - INB         | 10   | 18     |
    | Religion              | Is Blank     | R - IB           | 10   | 3      |
    | Pool                  | Is not Blank | P - INB          | 10   | 16     |
    | Years in Business     | Is Blank     | YIB - IB         | 10   | 4      |
    | Custom 1              | Is not Blank | CU1 - INB        | 10   | 14     |
    | Status                | Is not Blank | ST - IB          | 10   | 20     |
    | Status                | Is Blank     | ST - INB         | 10   | 20     |

#  @generic_conditions #http://prntscr.com/b4m68n
  Scenario Outline: Generic Fields - Is Equal To/ Begins With/ Contains/ Does not Contain
    Given Create Smart Group From Addressbook
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I save Smart Group as "<smart_group_save>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field      | conditions       | user_input    | smart_group_save | secs | number |
    | Prefix             | Is Equal To      | Mohd          | PRE - IET        | 10   | 1      |
    | Title              | Begins With      | Mr            | TI - BW          | 10   | 6      |
    | Phone              | Contains         | (616)         | PHO - CON        | 10   | 2      |
    | Mobile             | Does not Contain | (1030)700287  | MO - DNC         | 10   | 19     |
    | Website            | Is Equal To      | alibaba.com   | WEB - IET        | 10   | 1      |
    | Address            | Begins With      | Fa            | ADD - BW         | 10   | 2      |
    | Address 2          | Contains         | Me            | ADD2 - CON       | 10   | 2      |
    | City               | Does not Contain | S             | CI - DNC         | 10   | 9      |
    | Description        | Is Equal To      | Megha River   | DES - IET        | 10   | 1      |
    | Country            | Begins With      | Uni           | COU - BW         | 10   | 16     |
    | SIC Code           | Contains         | 1070-44-10024 | SIC - CON        | 10   | 1      |
    | SIC Description    | Does not Contain | non           | SICD - DNC       | 10   | 14     |
    | Franchise          | Is Equal To      | TRUE          | FRA - IET        | 10   | 13     |
    | Location Type      | Begins With      | ma            | LOT - BW         | 10   | 2      |
    | Women Owned        | Contains         | FALSE         | WO - CON         | 10   | 5      |
    | Estimated Income   | Does not Contain | $34,063.49    | EI - DNC         | 10   | 19     |
    | Marital Status     | Is Equal To      | Married       | MS - IET         | 10   | 9      |
    | Gender             | Begins With      | Male          | GEN - BW         | 10   | 9      |
    | Children           | Contains         | 1             | CHI - CON        | 10   | 7      |
    | Ethnicity          | Does not Contain | In            | ETH - DNC        | 10   | 13     |
    | Pets               | Is Equal To      | YES           | PE -IET          | 10   | 9      |
    | Opportunity Seeker | Begins With      | NO            | OS - BW          | 10   | 10     |
    | Charity Donor      | Contains         | YES           | CD - CON         | 10   | 10     |
    | Office Location    | Does not Contain | le            | OL - DNC         | 10   | 16     |
    | Custom 2           | Is Equal To      | customteam2   | CU2 - IET        | 10   | 14     |

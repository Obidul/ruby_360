@social
Feature: Direct social posting to all possible combinations
  Text/Link/Image post to all social network with possible all combinations.

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
#   Given As Admin User, Social Module access to conduct the test execution
    Given Social Access
    Given Wait until the Application is ready
    Given I wait for 10 seconds

  #STORY BLOCK: TC-D1 = TC-D6

  @TC-D1
  Scenario: Facebook Profile (FP) = Text
    Given Go for Social Post
    And Select Facebook Profile and Fill "Somewhere in Earth - TC-D1"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D2
  Scenario: Facebook Profile (FP) = Link
    Given Go for Social Post
    And Select Facebook Profile and Fill "http://www.facebook.com/TC-D2"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D3
  Scenario: Facebook Profile (FP) = Text + Link
    Given Go for Social Post
    And Select Facebook Profile and Fill "Somewhere in Earth - TC-D3 + http://www.facebook.com/profile-post - TC-D3"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D4 @wip
  Scenario: Facebook Profile (FP) = Text + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "Somewhere in Earth - TC-D4 + http://www.facebook.com/profile-post - TC-D4"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D5 @wip
  Scenario: Facebook Profile (FP) = Link + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "Somewhere in Earth - TC-D5 http://www.facebook.com/profile-post - TC-D5"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D6 @wip
  Scenario: Facebook Profile (FP) = Text + Link + Image
    Given Go for Social Post
    And Select Facebook Profile and Fill "Somewhere in Earth - TC-D6 + http://www.facebook.com/profile-post - TC-D6"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D7 - TC-D10 -> Switch Hooks

  @TC-D7
  Scenario: Facebook Fan Page (FFP) = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Somewhere in Earth - TC-D7"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D8
  Scenario: Facebook Fan Page (FFP) = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "http://www.facebook.com/TC-D8"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D9
  Scenario: Facebook Fan Page (FFP) = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "In this world http://www.facebook.com/profile-post/TC-D9 - TC-D9"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D10 @wip
  Scenario: Facebook Fan Page (FFP) = Text + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Today, In this world + http://www.facebook.com/profile-post - TC-D10"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D11 @wip
  Scenario: Facebook Fan Page (FFP) = Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Today, more than half of the world's population lives in cities + http://www.facebook.com/profile-post - TC-D11"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D12 @wip
  Scenario: Facebook Fan Page (FFP) = Text + Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page and Fill "Facebook Fan Page (FFP) - Text + http://www.facebook.com/profile-post - TC-D12"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D13 - TC-D15

  @TC-D13
  Scenario: Linkedin Profile (LP) = Text
    Given Go for Social Post
    And Select Linkedin Profile and Fill "Linkedin Profile (LP) - Text - TC-D13"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D14
  Scenario: Linkedin Profile (LP) = Link
    Given Go for Social Post
    And Select Linkedin Profile and Fill "http://www.linkedin.com/TC-D14"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D15
  Scenario: Linkedin Profile (LP) = Text + Link
    Given Go for Social Post
    And Select Linkedin Profile and Fill "http://www.linkedin.com/ & Linkedin Profile (LP) - Text - TC-D15"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D16 - TC-D18 -> Switch Hooks

  @TC-D13 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - TC-D16"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D14 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - TC-D17"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D15 @wip
  Scenario: Linkedin Profile Company Page (LCP) = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Profile Company Page and Fill "Linkedin Profile Company Page (LCP) - Text - TC-D18"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D19 - TC-D24

  @TC-D19
  Scenario: Twitter (TW) = Text
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text - TC-D19"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D20
  Scenario: Twitter (TW) = Link
    Given Go for Social Post
    And Select Twitter and Fill "http://www.twitter.com/TC-D20"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D21
  Scenario: Twitter (TW) = Text + Link
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - TC-D21"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D22 @wip
  Scenario: Twitter (TW) = Text + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - TC-D22"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D23 @wip
  Scenario: Twitter (TW) = Link + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - TC-D23"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D24 @wip
  Scenario: Twitter (TW) = Text + Link + Image
    Given Go for Social Post
    And Select Twitter and Fill "Twitter (TW) - Text + http://www.twitter.com/profile-post - TC-D24"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D25 - TC-D30
  #WIP: WORK IN PROGRESS - FAIL TO EXECUTE

  @TC-D25
  Scenario: Facebook Profile (FP) + TW = Text
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text - TC-D25"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-D26
  Scenario: Facebook Profile (FP) + TW = Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "http://www.facebooktwitter.com(NEW)/TC-D26"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-D27
  Scenario: Facebook Profile (FP) + TW = Text + Link
    Given Go for Social Post
    And I wait for 10 seconds
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text + http://www.facebooktwitter00.com/profile-post - TC-D27"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Local Storage Session
    And Close all browser windows

  @TC-D28 @wip
  Scenario: Facebook Profile (FP) + TW = Text + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW (NEW) - Text + http://www.facebooktwitter.com/profile-post - TC-D28"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D29 @wip
  Scenario: Facebook Profile (FP) + TW = Link + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW - Text + http://www.facebooktwitter.com/profile-post - TC-D29"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D30 @wip
  Scenario: Facebook Profile (FP) + TW = Text + Link + Image
    Given Go for Social Post
    And Select Facebook Profile & Twitter and Fill "Facebook Profile (FP) + TW - Text + http://www.facebooktwitter.com/profile-post - TC-D30"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D31 - TC-D33

  @TC-D31
  Scenario: Facebook Profile (FP) + LP = Text
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "Facebook Profile (FP) + LP - Text - TC-D31"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D32
  Scenario: Facebook Profile (FP) + LP = Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "http://www.facebooklinkedin.com/TC-DD32"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D33
  Scenario: Facebook Profile (FP) + LP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Profile and Fill "Facebook Profile (FP) + LP - Text - http://www.facebooklinkedin.com/TC-DD33 - TC-D33"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D34 - TC-D36

  @TC-D34 @wip
  Scenario: Facebook Profile (FP) + LCP = Text
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "Facebook Profile (FP) + LCP - Text - TC-D34"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D35 @wip
  Scenario: Facebook Profile (FP) + LCP = Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "http://www.facebooklinkedincompanypage.com/TC-DD35"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D36 @wip
  Scenario: Facebook Profile (FP) + LCP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Linkedin Company Page and Fill "Facebook Profile (FP) + LCP - Text - http://www.facebooklinkedincompanypage.com/TC-DD32 - TC-D36"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D37 - TC-D39

  @TC-D37
  Scenario: Twitter (TW) + LP = Text
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "Twitter (TW) + LP - Text - TC-D37"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D38
  Scenario: Twitter (TW) + LP = Link
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "http://www.facebooklinkedin.com/TC-DD38"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D39
  Scenario: Twitter (TW) + LP = Text + Link
    Given Go for Social Post
    And Select Twitter & Linkedin Profile and Fill "Twitter (TW) + LP - Text - http://www.facebooklinkedin.com/TC-DD39 - TC-D39"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D40 - TC-D42 -> Switch Hook

  @TC-D40 @wip
  Scenario: Twitter (TW) + LCP = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "Twitter (TW) + LCP - Text - TC-D40"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D41 @wip
  Scenario: Twitter (TW) + LCP = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "http://www.facebooklinkedin.com/TC-D41"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D42 @wip
  Scenario: Twitter (TW) + LCP = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Twitter & Linkedin Company Page and Fill "Twitter (TW) + LCP - Text - http://www.facebooklinkedin.com/TC-D42"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D43 - TC-D48

  @TC-D43
  Scenario: Twitter (TW) + FPP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text - TC-D43"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D44
  Scenario: Twitter (TW) + FPP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "http://www.facebookTwitter.com/TC-D44"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D45 @wip
  Scenario: Twitter (TW) + FPP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - TC-D45"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D46 @wip
  Scenario: Twitter (TW) + FPP = Text + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - TC-D46"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D47 @wip
  Scenario: Twitter (TW) + FPP = Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - TC-D47"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D48 @wip
  Scenario: Twitter (TW) + FPP = Text + Link + Image
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Twitter & Facebook Fan Page and Fill "Twitter + FPP - Text + http://www.facebookTwitter.com/profile-post - TC-D48"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D49 - TC-D51

  @TC-D49
  Scenario: Facebook Fan Page (FFP) + LP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "Facebook Fan Page + LP - Text - TC-D49"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D50
  Scenario: Facebook Fan Page (FFP) + LP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "http://www.facebooklinkedin.com/TC-D50"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D51
  Scenario: Facebook Fan Page (FFP) + LP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Facebook Fan Page & Linkedin Profile and Fill "Facebook Fan Page + LP - Text - http://www.facebooklinkedin.com/TC-D51"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D52 - TC-D54

  @TC-D52 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "Linkedin Company Page + FFP - Text - TC-D52"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D53 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Link
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "http://www.facebooklinkedin.com/TC-D53"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D54 @wip
  Scenario: Linkedin Company Page (LCP) + FFP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page & Linkedin Company Page
    And Select Linkedin Company Page & Facebook Fan Page and Fill "Linkedin Company Page + FFP - Text - http://www.facebooklinkedin.com/TC-D54"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile & Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D55 - TC-D57
  @TC-D55
  Scenario: Facebook Profile (FP) + TW + LP = Text
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "Facebook Profile TW + LP - Text - TC-D55"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D56
  Scenario: Facebook Profile (FP) + TW + LP = Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "http://www.facebooklinkedin.com/TC-D56"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D57
  Scenario: Facebook Profile (FP) + TW + LP = Text + Link
    Given Go for Social Post
    And Select Facebook Profile & Twitter & Linkedin Profile and Fill "Facebook Profile TW + LP - Text - http://www.facebooklinkedin.com/TC-D57"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D58 - TC-D60 -> Switch Hook

  @TC-D52 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Text
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "Facebook Fan Page (FFP) TW + LCP - Text - TC-D58"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D53 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Link
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "http://www.facebooklinkedin.com/TC-D59"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  @TC-D54 @wip
  Scenario: Facebook Fan Page (FFP) + TW + LCP = Text + Link
    Given Go for Social Post
    And Select Facebook Fan Page & Twitter & Linkedin Company Profile and Fill "Facebook Fan Page (FFP) TW + LCP - Text - http://www.facebooklinkedin.com/TC-D60"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D61 - TC-D63
  #WIP: WORK IN PROGRESS

  @TC-D61
  Scenario: Linkedin Profile (LP) + TW + FFP = Text
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "Linkedin Profile (LP)  + TW + FFP - Text - TC-D61"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D62
  Scenario: Linkedin Profile (LP) + TW + FFP = Link
    Given Go for Social Post
    And I wait for 5 seconds
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "http://www.facebooklinkedingoogle.com/TC-D62"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D63
  Scenario: Linkedin Profile (LP) + TW + FFP = Text + Link
    Given Go for Social Post
    And I switch to Facebook Fan Page
    And Select Linkedin Profile & Twitter & Facebook Fan Page and Fill "Linkedin Profile (LP)  + TW + FFP - Text - http://www.facebooklinkedin.com/TC-D63"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Facebook Profile
    Then Clean Browser Session
    And Close all browser windows

  #STORY BLOCK: TC-D64 - TC-D66

  @TC-D64 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Text
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "Linkedin Company Profile + TW + FP- Text - TC-D64"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D65 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "http://www.facebooklinkedin.com/TC-D65"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

  @TC-D66 @wip
  Scenario: Linkedin Company Profile (LCP) + TW + FP = Text + Link
    Given Go for Social Post
    And I switch to Linkedin Company Page
    And Select Linkedin Company Profile & Twitter & Facebook Profile and Fill "Linkedin Company Profile + TW + FP- Text - http://www.facebooklinkedin.com/TC-D66"
    Then I wait for 2 seconds
    And I click Post Now button
    And Expected popup validation message should be "Successfully Posted"
    And I switch to Linkedin Profile
    Then Clean Browser Session
    And Close all browser windows

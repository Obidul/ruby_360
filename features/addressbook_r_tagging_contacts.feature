Feature: Addressbook -- Tagging Contacts

  Background: Pre-reqisties
    Given I want to maximize the windows of browser
    Given As Admin User, Tags and related activity activity access to conduct the test execution
    Given Wait until the Application is ready
    Given I wait for 10 seconds

  @tagging
  Scenario Outline: Imported contacts binding into a tags
    And I wait for <secs> seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
    And I wait for <secs> seconds
    And I switch to Contact List View
    And I select all contacts from List View
    And I quick create a Tag of "<group_name>" and save it
    And I wait for <secs> seconds
  Examples:
    | secs | group_name |
    | 5    | Tag 01     |
    | 5    | Tag 02     |
    | 5    | Tag 03     |
    | 5    | Tag 04     |
    | 5    | Tag 05     |
    | 5    | Tag 06     |
    | 5    | Tag 07     |
    | 5    | Tag 08     |
    | 5    | Tag 09     |
    | 5    | Tag 10     |
    | 5    | Tag 11     |
    | 5    | Tag 12     |
    | 5    | Tag 13     |
    | 5    | Tag 14     |
    | 5    | Tag 15     |
    | 5    | Tag 16     |
    | 5    | Tag 17     |
    | 5    | Tag 18     |
    | 5    | Tag 19     |
    | 5    | Tag 20     |
    | 5    | Tag 21     |
    | 5    | Tag 22     |
    | 5    | Tag 23     |
    | 5    | Tag 24     |
    | 5    | Tag 25     |
    | 5    | Tag 26     |
    | 5    | Tag 27     |
    | 5    | Tag 28     |
    | 5    | Tag 29     |
    | 5    | Tag 30     |
    | 5    | Tag 31     |
    | 5    | Tag 32     |
    | 5    | Tag 33     |
    | 5    | Tag 34     |
    | 5    | Tag 35     |
    | 5    | Tag 36     |
    | 5    | Tag 37     |
    | 5    | Tag 38     |
    | 5    | Tag 39     |
    | 5    | Tag 40     |
    | 5    | Tag 41     |
    | 5    | Tag 42     |
    | 5    | Tag 43     |
    | 5    | Tag 44     |
    | 5    | Tag 45     |
    | 5    | Tag 46     |
    | 5    | Tag 47     |
    | 5    | Tag 48     |
    | 5    | Tag 49     |
    | 5    | Tag 50     |
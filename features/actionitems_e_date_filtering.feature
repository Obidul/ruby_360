@action_items
Feature: Action Stream -- Date Filtering

  Background: Login to system
    Given I want to maximize the windows of browser
  # Given As Admin User, Action Items - Filter by Date Activity access to conduct the test execution
    Given Local Test User Account
    Given Wait until the Application is ready
    Given Close Support options

  #STORY BLOCK: Primary Action List (PAL)

  @today
  Scenario: PAL -> Today - Calender to Select Today
    Then I select Today from Primary Right Panel
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select Week
    Then I select Today from Primary Right Panel
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select Month
    Then I select Today from Primary Right Panel
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select All
    Then I select Today from Primary Right Panel
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Today selected
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select Date - Today
    Then I select Today from Primary Right Panel
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select - Yesterday
    Then I select Today from Primary Right Panel
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @today
  Scenario: PAL -> Today - Calender to Select - Tomorrow
    Then I select Today from Primary Right Panel
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select Today
    Then I select Starred from Primary Right Panel
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select Week
    Then I select Starred from Primary Right Panel
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select Month
    Then I select Starred from Primary Right Panel
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select All
    Then I select Starred from Primary Right Panel
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Starred selected
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select Date - Today
    Then I select Starred from Primary Right Panel
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select - Yesterday
    Then I select Starred from Primary Right Panel
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @starred
  Scenario: PAL -> Starred - Calender to Select - Tomorrow
    Then I select Starred from Primary Right Panel
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select Today
    Then I select Overdue from Primary Right Panel
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select Week
    Then I select Overdue from Primary Right Panel
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds
  @overdue
  Scenario: PAL -> Overdue - Calender to Select Month
    Then I select Overdue from Primary Right Panel
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select All
    Then I select Overdue from Primary Right Panel
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Overdue selected
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select Date - Today
    Then I select Overdue from Primary Right Panel
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select - Yesterday
    Then I select Overdue from Primary Right Panel
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @overdue
  Scenario: PAL -> Overdue - Calender to Select - Tomorrow
    Then I select Overdue from Primary Right Panel
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: Secondary Action List (SAL)

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select Today
    Then I select All Activity Under Activity Tab
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select Week
    Then I select All Activity Under Activity Tab
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select Month
    Then I select All Activity Under Activity Tab
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select All
    Then I select All Activity Under Activity Tab
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While All Activity selected
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select Date - Today
    Then I select All Activity Under Activity Tab
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select - Yesterday
    Then I select All Activity Under Activity Tab
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @all_activity
  Scenario: SCL -> Activity Tab -> All Activity - Calender to Select - Tomorrow
    Then I select All Activity Under Activity Tab
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: Meeting

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select Today
    Then I select Meeting Under Activity Tab
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select Week
    Then I select Meeting Under Activity Tab
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select Month
    Then I select Meeting Under Activity Tab
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select All
    Then I select Meeting Under Activity Tab
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Meeting selected
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select Date - Today
    Then I select Meeting Under Activity Tab
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select - Yesterday
    Then I select Meeting Under Activity Tab
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @meeting
  Scenario: SCL -> Activity Tab -> Meeting - Calender to Select - Tomorrow
    Then I select Meeting Under Activity Tab
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: Calls

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select Today
    Then I select Calls Under Activity Tab
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select Week
    Then I select Calls Under Activity Tab
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select Month
    Then I select Calls Under Activity Tab
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select All
    Then I select Calls Under Activity Tab
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Calls selected
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select Date - Today
    Then I select Calls Under Activity Tab
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select - Yesterday
    Then I select Calls Under Activity Tab
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @calls
  Scenario: SCL -> Activity Tab -> Calls - Calender to Select - Tomorrow
    Then I select Calls Under Activity Tab
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: Tasks

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select Today
    Then I select Tasks Under Activity Tab
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select Week
    Then I select Tasks Under Activity Tab
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select Month
    Then I select Tasks Under Activity Tab
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select All
    Then I select Tasks Under Activity Tab
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Tasks selected
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select Date - Today
    Then I select Tasks Under Activity Tab
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select - Yesterday
    Then I select Tasks Under Activity Tab
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @tasks
  Scenario: SCL -> Activity Tab -> Tasks - Calender to Select - Tomorrow
    Then I select Tasks Under Activity Tab
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select Today
    Then I select Todo Under Activity Tab
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select Week
    Then I select Todo Under Activity Tab
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select Month
    Then I select Todo Under Activity Tab
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select All
    Then I select Todo Under Activity Tab
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Todo selected
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select Date - Today
    Then I select Todo Under Activity Tab
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select - Yesterday
    Then I select Todo Under Activity Tab
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @todo
  Scenario: SCL -> Activity Tab -> To Do - Calender to Select - Tomorrow
    Then I select Todo Under Activity Tab
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: STATUS (SAL)

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select Today
    Then I select New from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select Week
    Then I select New from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select Month
    Then I select New from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select All
    Then I select New from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While New Status selected
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select Date - Today
    Then I select New from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select - Yesterday
    Then I select New from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_new
  Scenario: SCL -> Status -> New - Calender to Select - Tomorrow
    Then I select New from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select Today
    Then I select Attempted from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select Week
    Then I select Attempted from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select Month
    Then I select Attempted from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select All
    Then I select Attempted from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Attempted Status selected
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select Date - Today
    Then I select Attempted from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select - Yesterday
    Then I select Attempted from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_attempted
  Scenario: SCL -> Status -> Attempted - Calender to Select - Tomorrow
    Then I select Attempted from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select Today
    Then I select Contacted from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select Week
    Then I select Contacted from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select Month
    Then I select Contacted from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select All
    Then I select Contacted from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Contacted Status selected
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select Date - Today
    Then I select Contacted from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select - Yesterday
    Then I select Contacted from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_contacted
  Scenario: SCL -> Status -> Contacted - Calender to Select - Tomorrow
    Then I select Contacted from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select Today
    Then I select Hot Prospect from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select Week
    Then I select Hot Prospect from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select Month
    Then I select Hot Prospect from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select All
    Then I select Hot Prospect from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Hot Prospect Status selected
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select Date - Today
    Then I select Hot Prospect from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select - Yesterday
    Then I select Hot Prospect from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_hot_prospect
  Scenario: SCL -> Status -> Hot Prospect - Calender to Select - Tomorrow
    Then I select Hot Prospect from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select Today
    Then I select Client from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select Week
    Then I select Client from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select Month
    Then I select Client from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select All
    Then I select Client from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Client Status selected
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select Date - Today
    Then I select Client from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select - Yesterday
    Then I select Client from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_client
  Scenario: SCL -> Status -> Client - Calender to Select - Tomorrow
    Then I select Client from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select Today
    Then I select Disqualified from Status - while in Action Stream module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select Week
    Then I select Disqualified from Status - while in Action Stream module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select Month
    Then I select Disqualified from Status - while in Action Stream module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select All
    Then I select Disqualified from Status - while in Action Stream module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Disqualified Status selected
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select Date - Today
    Then I select Disqualified from Status - while in Action Stream module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select - Yesterday
    Then I select Disqualified from Status - while in Action Stream module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @status_disqualified
  Scenario: SCL -> Status -> Disqualified - Calender to Select - Tomorrow
    Then I select Disqualified from Status - while in Action Stream module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: Contact Action List (CAL)

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Today
    And I select All Deals Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Week
    And I select All Deals Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Month
    And I select All Deals Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select All
    And I select All Deals Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While All Deals selected
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Date - Today
    And I select All Deals Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select - Yesterday
    And I select All Deals Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_all_deals
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select - Tomorrow
    And I select All Deals Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Today
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Week
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Month
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select All
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Prospect selected
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select Date - Today
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select - Yesterday
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_prospect
  Scenario: CAL -> Deals Menu -> All Deals - Calender to Select - Tomorrow
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select Today
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select Week
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select Month
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select All
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Needs Analysis selected
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select Date - Today
    And I select Needs Analysis Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select - Yesterday
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_needs_analysis
  Scenario: CAL -> Deals Menu -> Needs Analysis - Calender to Select - Tomorrow
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select Today
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select Week
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select Month
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select All
    And I select Proposal Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Proposal selected
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select Date - Today
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select - Yesterday
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_proposal
  Scenario: CAL -> Deals Menu -> Proposal - Calender to Select - Tomorrow
    And I select Proposal Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##
  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select Today
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select Week
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select Month
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select All
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Negotiations selected
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select Date - Today
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select - Yesterday
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_negotiations
  Scenario: CAL -> Deals Menu -> Negotiations - Calender to Select - Tomorrow
    And I select Negotiations Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  ##

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select Today
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select Week
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select Month
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select All
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Order&Contract selected
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select Date - Today
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select - Yesterday
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @deals_order_contract
  Scenario: CAL -> Deals Menu -> Order/Contract - Calender to Select - Tomorrow
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds

  #STORY BLOCK: TAGS

  @tags_first @wip
  Scenario: CAL -> Tags -> NewTag - Create a Custom Tag -> NewTag
    And I wait for 5 seconds
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
    And In Tags Tab I select "No Tags" and select "New Tag"
    And I wait for 5 seconds
    Then To create Tags I fill in "NewTag" and save it
    And I quick search with "Anthony Harvey" -- with enter function
  #   Then I Assign few contacts in "NewTag" Group
    And To create Tags I fill in "NewTag" and save it -- while in Addressbook
    And I wait for 5 seconds

  Scenario: CAL -> Tags -> NewTag - Calender to Select Today
  Scenario: CAL -> Tags -> NewTag - Calender to Select Week
  Scenario: CAL -> Tags -> NewTag - Calender to Select Month
  Scenario: CAL -> Tags -> NewTag - Calender to Select All
  Scenario: CAL -> Tags -> NewTag - Calender to Select Date - Today
  Scenario: CAL -> Tags -> NewTag - Calender to Select - Yesterday
  Scenario: CAL -> Tags -> NewTag - Calender to Select - Tomorrow

  #STORY BLOCK: FILTER

  Scenario: CAL -> Filter -> MyFilters - Calender to Select Today
  Scenario: CAL -> Filter -> MyFilters - Calender to Select Week
  Scenario: CAL -> Filter -> MyFilters - Calender to Select Month
  Scenario: CAL -> Filter -> MyFilters - Calender to Select All
  Scenario: CAL -> Filter -> MyFilters - Calender to Select Date - Today
  Scenario: CAL -> Filter -> MyFilters - Calender to Select - Yesterday
  Scenario: CAL -> Filter -> MyFilters - Calender to Select - Tomorrow

  #STORY BLOCK: TEAM

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select Today
    Then I select Me from Team menu -- while in Action Stream
    And From Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select Week
    Then I select Me from Team menu -- while in Action Stream
    And From Calender Range - I select Week
    And Filtered by Week - Number matched
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select Month
    Then I select Me from Team menu -- while in Action Stream
    And From Calender Range - I select Month
    And Filtered by Month - Number matched
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select All
    Then I select Me from Team menu -- while in Action Stream
    And From Calender Range - I select All
    And Filtered by All - Number matched -- While Me selected
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select Date - Today
    Then I select Me from Team menu -- while in Action Stream
    And Within Calender Range - I select Today
    And Filtered by Today - Number matched
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select - Yesterday
    Then I select Me from Team menu -- while in Action Stream
    And Within Calender Range - I select Yesterday
    And Filtered by Yesterday - Number matched
    And I wait for 2 seconds

  @team_me
  Scenario: CAL -> Team -> Me - Calender to Select - Tomorrow
    Then I select Me from Team menu -- while in Action Stream
    And Within Calender Range - I select Tomorrow
    And Filtered by Tomorrow - Number matched
    And I wait for 2 seconds
  
  

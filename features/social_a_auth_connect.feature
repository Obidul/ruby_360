@social
Feature: Connect all social networks with this account

  Background: Connect to all social networks
    Given I want to maximize the windows of browser
   Given As Admin User, Social Auth Enable & Disable - Access to conduct the test execution
#    Given Local Test User Account
    Given Wait until the Application is ready

    #Note: All account must be disabled before this execution.

  @linkedin
  Scenario: As Admin, I want to enable linkedin social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    And I want to enable linkedin social account
    And I wait for 5 seconds

  @facebook
  Scenario: As Admin, I want to enable facebook social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    And I want to enable facebook social account
    And I wait for 5 seconds

  @twitter
  Scenario: As Admin, I want to enable twitter social account
    And I wait for 20 seconds
    And I click on social tab
    And I wait for 7 seconds
    And I want to enable twitter social account
    And I wait for 5 seconds

#  @allaccount
#  Scenario: Enable all social networks
#    And I wait for 10 seconds
#    And I click on social tab
#    And I wait for 20 seconds
#    And I want to enable facebook social account
#    And I want to enable twitter social account
#    And I want to enable linkedin social account
#    And I wait for 3 seconds
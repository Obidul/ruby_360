@home
Feature: Login at Splash360
  As a new user, I would like to login at splash360.com
  As a new user, I would like to login with invalid access at splash360.com
  Background: Prerequisites
    Given I want to maximize the windows of browser
  Scenario: As a Existing Admin User, Login attempt with valid data
    Given I am on the "/Account/Login" page
    When I fill in "username" with "isakib"
    When I fill in "password" with "123456"
    And I click on "Login" button
    Then I wait for 5 seconds
  Scenario: As a New user, Login attempt with invalid data
    Given I am on the "/Account/Login" page
    When I fill in "username" with "sakib"
    When I fill in "password" with "123456"
    And I click on "Login" button
    Then I should see "Please try again. We do not recognize that Username and Password combination. " on screen
    Then I wait for 3 seconds

  Scenario: As Anonymous user, Login attempt without providing no data
    And I click on "Login" button
    Then I should see "Oops! Login Failed." on screen
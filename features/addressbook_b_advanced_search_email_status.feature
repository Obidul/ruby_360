@addressbook
Feature: Email Status - Is Equal/Not Equal

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Email Status Activity access to conduct the test execution
    Given Wait until the Application is ready

  #Note: Downsizing sec will cause entire scenario failure. No other tweaks worked.

  @email_status @esf
  Scenario Outline: Email Status - Equal
    And Go for Advanced Search
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | secs | number |
    | Email Status  | Equal      | Active               | 15   | 6      |
    | Email Status  | Equal      | Bad Email Format     | 15   | 4      |
    | Email Status  | Equal      | Blocked              | 15   | 1      |
    | Email Status  | Equal      | Hard Bounce          | 15   | 2      |
    | Email Status  | Equal      | No Email Address     | 15   | 5      |
    | Email Status  | Equal      | Unsubscribed         | 15   | 2      |

  @email_status_equal @esf
  Scenario Outline: Email Status - Not Equal
    And Go for Advanced Search
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | secs | number |
    | Email Status  | Not Equal  | Active               | 15   | 14     |
    | Email Status  | Not Equal  | Bad Email Format     | 15   | 16     |
    | Email Status  | Not Equal  | Blocked              | 15   | 19     |
    | Email Status  | Not Equal  | Hard Bounce          | 15   | 18     |
    | Email Status  | Not Equal  | No Email Address     | 15   | 15     |
    | Email Status  | Not Equal  | Unsubscribed         | 15   | 18     |
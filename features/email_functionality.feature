@email
Feature: Go to Email tab and perform email functionality

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Automated Email access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  @e1
  Scenario: As admin user, Save a template to draft folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the save button
    Then I save the template as a draft
    And I wait for 3 seconds

  @d1
  Scenario: As admin user, Delete the template from the Draft
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    Then I should see "Drafts" on screen
    Then I want to delete template: "2 Text Header"
    And I wait for 2 seconds

  @e2
  Scenario: As admin user, Email Toolbar: Send (Campaign)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then In To field: I typed "Email" campaign group and found "Email me" and selected "Email me" from the list
    Then In Subject field: I fill in "Direct - Campaign Email"
    And I click on Send button
    And I wait for 5 seconds
    Then I click on the Send Now button
    And I should see Email has sent confirmation

  @e3
  Scenario: As admin user, Email Toolbar: Schedule (Campaign)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then In To field: I typed "Email" campaign group and found "Email me" and selected "Email me" from the list
    Then In Subject field: I fill in "Schedule - Campaign Email"
    And I click on Schedule button
    And I wait for 5 seconds
    And I click on the Save button to Schedule campaign
    Then I should see "Schedule - Campaign Email" on screen

  @d2
  Scenario: As admin user, Delete the Scheduled campaign
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Scheduled Campaigns"
    And I wait for 5 seconds
    Then I click on the delete button of the scheduled campaign named: "Schedule - Campaign Email"
    And I accept to proceed remaining scenario

  @e10
  Scenario: As admin user, Email Toolbar: Schedule (Campaign) from Plus button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Scheduled Campaign
    And I wait for 3 seconds
    Then In To field: I typed "Email" campaign group and found "Email me" and selected "Email me" from the list
    Then In Subject field: I fill in "Schedule - Campaign Email from Plus button"
    And I click on Schedule button
    And I wait for 5 seconds
    And I click on the Save button to Schedule campaign
    Then I should see "Schedule - Campaign Email from Plus button" on screen

  @d10
  Scenario: As admin user, Delete the Scheduled campaign
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Scheduled Campaigns"
    And I wait for 5 seconds
    Then I click on the delete button of the scheduled campaign named: "Schedule - Campaign Email from Plus button"
    And I accept to proceed remaining scenario

  @e4
  Scenario: As admin user, Email toolbar: Preview - Desktop View
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the email preview button
    And I wait for 3 seconds
    Then I should see "Send Test" on screen

  @e5
  Scenario: As admin user, Email toolbar: Preview - Mobile View
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the email preview button
    And I wait for 3 seconds
    Then I should see "Send Test" on screen
    Then I switch to mobile view
    And I wait for 3 seconds

  @e6
  Scenario: As admin user, Email toolbar: Preview - Desktop View - Send Test Email
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the email preview button
    And I wait for 3 seconds
    Then I should see "Send Test" on screen
    Then I send the test email to : "milonbsdhk1@mailinator.com"
    And I wait for 3 seconds

  @e7
  Scenario: As admin user, Email toolbar: Preview - Mobile View - Send Test Email
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the email preview button
    And I wait for 3 seconds
    Then I should see "Send Test" on screen
    Then I switch to mobile view
    And I wait for 3 seconds
    Then I send the test email to : "milonbsdhk2@mailinator.com"
    And I wait for 3 seconds

  @e8
  Scenario: As admin user, Email Toolbar: Background Color
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I change the background color to Green
    And I wait for 3 seconds

  @e9
  Scenario: As admin user, Email Toolbar: Source Code
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    And I click on the "Birthday" category
    Then I should see "Birthday" on screen
    Then I select the "2 Text Header" template
    And I wait for 3 seconds
    Then I click on the view source code button
    And I wait for 3 seconds

#  Scenario: As admin user, Email Toolbar: Attach File

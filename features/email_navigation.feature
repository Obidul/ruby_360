@email
Feature: Go to Email tab and perform Email operation

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Automated Email access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #STORY BLOCK: Email right panel navigation

  @m1
  Scenario: As admin user, Navigate to Recent Work
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Recent Work"
    And I wait for 5 seconds
    Then I should see "Recent Work" on screen

  @m1
  Scenario: As admin user, Navigate to Email Gallery
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Email Gallery"
    And I wait for 5 seconds
    Then I should see "Select a Design" on screen

  @m1
  Scenario: As admin user, Navigate to Drafts
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Drafts"
    And I wait for 5 seconds
    Then I should see "Drafts" on screen

  @m1
  Scenario: As admin user, Navigate to Shared Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Shared Folder"
    And I wait for 5 seconds
    Then I should see "Shared" on screen

  @m1
  Scenario: As admin user, Navigate to My Folders
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "My Folders"
    And I wait for 5 seconds
    Then I should see "My Folders" on screen

  Scenario: As admin user, Navigate to Scheduled Campaigns
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Scheduled Campaigns"
    And I wait for 5 seconds

  Scenario: As admin user, Navigate to Automated Emails
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I should see "Autoresponders" on screen

#  Navigation is perfectly working. Need to handle browser default popup
#  Scenario: As admin user, Navigate to Import Template
#    And I wait for 3 seconds
#    Then I click on Email Tab
#    And I wait for 5 seconds
#    Then I clicked on "Import Template"
#    And I wait for 5 seconds

  Scenario: As admin user, Navigate to New Automated Email
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen

  Scenario: As admin user, Create New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I want to create A New Folder named "Test10"
    And I wait for 2 seconds
    Then I should see "Test10" on screen

  Scenario: As admin user, Delete Folder from My Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "My Folder" from the right panel Dropdown
    Then I should see "My Folders" on screen
    Then I want to delete folder "Test10"
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  Scenario: As admin user, Select any  categories by click on the image thumbnail
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the "Birthday" category
    Then I should see "Select a Design" on screen

  #STORY BLOCK: Right panel dropdown navigation

  Scenario: As admin user, Email -> Right panel dropdown Activation
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    Then I should see "Email Gallery" on screen

  Scenario: As admin user, Navigate from right panel dropdown to -> Email Gallery
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Email Gallery" from the right panel Dropdown
    Then I should see "Select a Design" on screen

  Scenario: As admin user, Navigate from right panel dropdown to -> Recent Work
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Recent Work" from the right panel Dropdown
    Then I should see "Recent Work" on screen

  Scenario: As admin user, Navigate from right panel dropdown to -> Drafts
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    Then I should see "Drafts" on screen

  Scenario: As admin user, Navigate from right panel dropdown to -> Shared Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Shared Folder" from the right panel Dropdown
    Then I should see "Shared" on screen

  Scenario: As admin user, Navigate from right panel dropdown to -> My Folders
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "My Folders" from the right panel Dropdown
    Then I should see "My Folders" on screen

  Scenario: As admin user, Select sub category
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    And I click on the "Birthday" category
    Then I should see "Select a Design" on screen
    Then I select the sub category "Holidays & Seasonal"
    And I wait for 2 seconds

  #STORY BLOCK: Cancel (Cross button) navigation

  @c1
  Scenario: As admin user, Navigation: Recent Work -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Recent Work"
    And I wait for 5 seconds
    Then I should see "Recent Work" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Drafts -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Drafts"
    And I wait for 5 seconds
    Then I should see "Drafts" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Shared Folder -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Shared Folder"
    And I wait for 5 seconds
    Then I should see "Shared" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: My Folders -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "My Folders"
    And I wait for 5 seconds
    Then I should see "My Folders" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Scheduled Campaigns -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Scheduled Campaigns"
    And I wait for 5 seconds
    Then I should see "Scheduled Campaigns" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Automated Emails -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Automated Emails"
    And I wait for 5 seconds
    Then I should see "Autoresponders" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: New Automated Email (Round Plus Button)-> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I want to create Automated Email
    Then I should see "Create an Automated Email" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Select any categories -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the "Birthday" category
    Then I should see "Select a Design" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Right panel dropdown -> Recent Work -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Recent Work" from the right panel Dropdown
    Then I should see "Recent Work" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Right panel dropdown -> Drafts -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    Then I should see "Drafts" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Right panel dropdown -> Shared Folder -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "Shared Folder" from the right panel Dropdown
    Then I should see "Shared" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen

  @c1
  Scenario: As admin user, Navigation: Right panel dropdown -> My Folders -> Cancel (Cross button)
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 2 seconds
    Then I click on the Email right panel Dropdown Icon
    And I clicked on "My Folders" from the right panel Dropdown
    Then I should see "My Folders" on screen
    Then I clicked on the cross button to cancel
    And I should see "Select a Design" on screen
@print
Feature: Go to Print tab and perform print operation

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Print Module access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #STORY BLOCK: Print right panel naigation

  @m1
  Scenario: As admin user, Navigate to Recent Work
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Recent Work"
    And I wait for 5 seconds
    Then I should see "Recent Work" on screen

  @m1
  Scenario: As admin user, Navigate to Email Gallery
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Email Gallery"
    And I wait for 5 seconds
    Then I should see "Select a Design" on screen

  @m1
  Scenario: As admin user, Navigate to Drafts
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Drafts"
    And I wait for 5 seconds
    Then I should see "Drafts" on screen

  @m1
  Scenario: As admin user, Navigate to Shared Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Shared Folder"
    And I wait for 5 seconds
    Then I should see "Shared" on screen

  @m1
  Scenario: As admin user, Navigate to My Folders
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "My Folders"
    And I wait for 5 seconds
    Then I should see "My Folders" on screen

  Scenario:As admin user, Navigate to Pricing & Specifications
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    And I clicked on "Pricing & Specifications"
    And I should see "Postcards" on screen

  Scenario:As admin user, Navigate to Design Guidelines
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I clicked on "Design Guidelines"
    And I should see "Design Guidelines" on screen

  @new
  Scenario:As admin user, Create New Folder
    And I wait for 3 seconds
    Then I click on Print Tab
    And I wait for 2 seconds
    Then I want to create a New folder named "Test10" in Print
#    And I clicked on "New Folder" and type folder name "Test10"
    And I should see "Test10" on screen

  Scenario:As admin user, Delete folder from My Folder
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "My Folders" from the right panel Dropdown
    And I should see "My Folders" on screen
    And I want to delete folder "Test10"
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  Scenario:As admin user, Select any categories by click on the image thumbnail
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen

  #STORY BLOCK: Email right panel dropdown naigation

  Scenario:As admin user, Print -> Right panel dropdown Activation
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I should see "Print Gallery" on screen

  Scenario:As admin user, Navigate from right panel dropdown to -> Print Gallery
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Print Gallery" from the right panel Dropdown
    And I should see "Select a Product" on screen

  Scenario:As admin user, Navigate from right panel dropdown to -> Recent Work
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Recent Work" from the right panel Dropdown
    And I should see "Recent Work" on screen

  Scenario:As admin user, Navigate from right panel dropdown to -> Draft
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    And I should see "Drafts" on screen

  Scenario:As admin user, Navigate from right panel dropdown to -> Shared Folder
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Shared Folder" from the right panel Dropdown
    And I should see "Shared Folder" on screen

  Scenario:As admin user, Navigate from right panel dropdown to -> My Folders
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "My Folders" from the right panel Dropdown
    And I should see "My Folders" on screen

  Scenario:As admin user, Select sub category
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the sub category "Food & Drink"
    And I wait for 2 seconds

  #STORY BLOCK: Print order process

  Scenario:As admin user, Print order process -> Card (Send to me option)
    And I wait for 3 seconds
    And I click on Print Tab
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the "Photo Grid" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 5 seconds
    And I should see "Order Print" on screen
    And I select the quantity to "500"
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I checkout the order
    And I should see "Billing Info" on screen
    And I fill up the Card Number "4564019845654409" and CVV number "123"
    And I complete the Print order
    And I wait for 5 seconds
    And I should see "Thanks for your order!" on screen

  Scenario:As admin user, Print order process -> Card (send to list option)
    And I wait for 3 seconds
    And I click on Print Tab
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the "Photo Grid" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 5 seconds
    And I should see "Order Print" on screen
    And I checked the Send to list option
    And I should see "purchase new." on screen
    And I select the quantity to "All Contacts"
    And I select the first available mailing date
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I checkout the order
    And I should see "Billing Info" on screen
    And I fill up the Card Number "4564019845654409" and CVV number "123"
    And I complete the Print order
    And I wait for 5 seconds
    And I should see "Thanks for your order!" on screen

  Scenario:As admin user, Print order process -> Card + List
    And I wait for 3 seconds
    And I click on Print Tab
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the "Photo Grid" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 5 seconds
    And I should see "Order Print" on screen
    And I checked the Send to list option
    And I should see "purchase new." on screen
    And I click on "purchase new." link
    And I wait for 2 seconds
    And I should see "Who do you want to target?" on screen
    And I select type "Businesses" and number of contacts "500"
    And I want to switch between windows
    And I Confirm the purchase
    And I should see "How should we mail it?" on screen
    And I select the first available mailing date
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I checkout the order
    And I should see "Billing Info" on screen
    And I fill up the Card Number "4564019845654409" and CVV number "123"
    And I complete the Print order
    And I wait for 7 seconds
    And I should see "Thanks for your order!" on screen

  Scenario:As admin user, Print order process -> Multiple Cards + Multiple lists
    And I wait for 3 seconds
    And I click on Print Tab
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the "Photo Grid" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 5 seconds
    And I should see "Order Print" on screen
    And I checked the Send to list option
    And I should see "purchase new." on screen
    And I click on "purchase new." link
    And I wait for 2 seconds
    And I should see "Who do you want to target?" on screen
    And I select type "Businesses" and number of contacts "500"
    And I want to switch between windows
    And I Confirm the purchase
    And I should see "How should we mail it?" on screen
    And I select the first available mailing date
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I add another print order
    And I should see "Select a Product" on screen
    And Change mouse cursor position
    And I click on the "Oversized Postcards" category
    And I should see "Oversized Postcards" on screen
    And I select the "Headshot" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 15 seconds
    And I checked the Send to list option
    And I should see "purchase new." on screen
    And I click on "purchase new." link
    And I should see "Who do you want to target?" on screen
    And I select type "Individuals" and number of contacts "1000"
    And I want to switch between windows
    And I Confirm the purchase
    And I should see "How should we mail it?" on screen
    And I select the first available mailing date
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I checkout the order
    And I should see "Billing Info" on screen
    And I fill up the Card Number "4564019845654409" and CVV number "123"
    And I complete the Print order
    And I wait for 10 seconds

  Scenario:As admin user, Import contacts from the Thank You Page
    And I wait for 3 seconds
    And I click on Print Tab
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    And I select the "Photo Grid" template
    And I wait for 4 seconds
    And I complete the check box and continue to next step
    And I wait for 5 seconds
    And I should see "Order Print" on screen
    And I checked the Send to list option
    And I should see "purchase new." on screen
    And I click on "purchase new." link
    And I wait for 2 seconds
    And I should see "Who do you want to target?" on screen
    And I select type "Businesses" and number of contacts "500"
    And I want to switch between windows
    And I Confirm the purchase
    And I should see "How should we mail it?" on screen
    And I select the first available mailing date
    And I continue to step 3 to step 4
    And I should see "Cart" on screen
    And I checkout the order
    And I should see "Billing Info" on screen
    And I fill up the Card Number "4564019845654409" and CVV number "123"
    And I complete the Print order
    And I wait for 10 seconds
    And I should see "Thanks for your order!" on screen
    And I click on "Import Now (Recommended)" link
    And I wait for 5 seconds
    And I should see "Step 1: Select Groups" on screen
    And Import process move from step 1 to step 2
    And I wait for 5 seconds
    And I should see "Step 2: File Upload" on screen
    And Import process move from step 2 to step 3
    And I wait for 5 seconds
    And I should see "Step 3: Label Fields" on screen
    And Import process final step -> Import Now
    And I wait for 5 seconds

  #STORY BLOCK: Cancel (Cross button) navigation

  @c1
  Scenario: As admin user, Navigation: Recent Work -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    Then I clicked on "Recent Work"
    And I wait for 5 seconds
    Then I should see "Recent Work" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario: As admin user, Navigation: Drafts -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    Then I clicked on "Drafts"
    And I wait for 5 seconds
    Then I should see "Drafts" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario: As admin user, Navigation: Shared Folder -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    Then I clicked on "Shared Folder"
    And I wait for 5 seconds
    Then I should see "Shared" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario: As admin user, Navigation: My Folders -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    Then I clicked on "My Folders"
    And I wait for 5 seconds
    Then I should see "My Folders" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Pricing & Specifications -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 5 seconds
    And I clicked on "Pricing & Specifications"
    And I should see "Postcards" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Design Guidelines -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I clicked on "Design Guidelines"
    And I should see "Design Guidelines" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Select any categories -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the "Standard Postcards" category
    And I should see "Standard Postcards" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Right panel dropdown -> Recent Work -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Recent Work" from the right panel Dropdown
    And I should see "Recent Work" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Right panel dropdown -> Draft -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Drafts" from the right panel Dropdown
    And I should see "Drafts" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Right panel dropdown -> Shared Folder -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "Shared Folder" from the right panel Dropdown
    And I should see "Shared Folder" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

  @c1
  Scenario:As admin user, Navigation: Right panel dropdown -> My Folders -> Cancel (Cross button)
    And I wait for 3 seconds
    And I click on Print Tab
    And I wait for 2 seconds
    And I click on the right panel Dropdown Icon
    And I clicked on "My Folders" from the right panel Dropdown
    And I should see "My Folders" on screen
    Then I clicked on the cross button to cancel
    Then I should see "Select a Product" on screen

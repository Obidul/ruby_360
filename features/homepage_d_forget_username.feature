@splash_webpage
Feature: Retrieve at Splash360
  As registered user, I would to get my username from splash360.com system
  As anonymous user, I would to get my username from splash360.com system with invalid data

  Scenario:   As registered user, I would to retrieve my password from splash360.com system
    Given I am on the "/Account/ForgotUsername" page
    When I fill in "emailaddress" with "sakib@brainstation-23.com"
    And I click through xpath link ".//*[@id='send_btn']/span"
    Then I should see "Username Sent" on screen
    Then I wait for 2 seconds

  Scenario: As anonymous user, I would to retrive my password from splash360.com system with invalid data
    Given I am on the "/Account/ForgotUsername" page
    When I fill in "emailaddress" with "nonononoemail@gmail.com"
    And I click through xpath link ".//*[@id='send_btn']/span"
    Then I should see "Email address is invalid" on screen
    Then I wait for 2 seconds

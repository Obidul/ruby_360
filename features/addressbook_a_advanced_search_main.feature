@addressbook
Feature: Advanced search
  Advanced Search main

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Advanced Search activity access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #Read Notes: Under Steps Definitions

  #STORY BLOCK: TC-AS1 - TC-AS6 (Excluding Is Blank & Is Not Blank)

  @company
  Scenario Outline: Company - Is Equal To/Begins With/Contains/Does not Contain
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input | secs | number |
    | Company       | Is Equal To      | Abata      | 5    | 6      |
    | Company       | Begins With      | Br         | 5    | 2      |
    | Company       | Contains         | le         | 5    | 4      |
    | Company       | Does not Contain | le         | 5    | 16     |

  #STORY BLOCK: TC-AS7 - TC-AS12 (Excluding Is Blank & Is Not Blank)

  @contact
  Scenario Outline: Contact - Is Equal To/Begins With/Contains/Does not Contain
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input | secs | number |
    | Contact       | Is Equal To      | Ja         | 5    | 2      |
    | Contact       | Begins With      | D          | 5    | 4      |
    | Contact       | Contains         | Co         | 5    | 3      |
    | Contact       | Does not Contain | Ja         | 5    | 18     |

  #STORY BLOCK: TC-AS13- TC-AS18 (Excluding Is Blank & Is Not Blank)

  @email
  Scenario Outline: Email - Is Equal To/Begins With/Contains/Does not Contain
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions       | user_input           | secs | number |
    | Email         | Is Equal To      | sakibqa+26@gmail.com | 5    | 1      |
    | Email         | Begins With      | sakibqa              | 5    | 13     |
    | Email         | Contains         | qa                   | 5    | 13     |
    | Email         | Does not Contain | qa                   | 5    | 7      |

  #Anniversary, Birthday, Create Date, Modify Date
  #STORY BLOCK: TC-AS60 - TC-AS63

  @anniversary
  Scenario Outline: Anniversary - Is Equal To/Before/After
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | secs | number |
    | Anniversary   | Is Equal To | 04/03      | 10   | 4      |
    | Anniversary   | Before      | 04/03      | 10   | 4      |
    | Anniversary   | After       | 01/01      | 10   | 16     |

  @birthday
  Scenario Outline: Birthday - Is Equal To/Before/After
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | secs | number |
    | Birthday      | Is Equal To | 07/06      | 10   | 4      |
    | Birthday      | Before      | 06/06      | 10   | 7      |
    | Birthday      | After       | 08/08      | 10   | 2      |

  #Account Create Date & Modify Date

  @create_date
  Scenario Outline: Create Date - Is Equal To/Before/After
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | secs | number |
    | Create Date   | Is Equal To | 05/16/2016 | 10   | 20     |
    | Create Date   | Before      | 05/14/2016 | 10   | 1      |
    | Create Date   | After       | 03/08/2016 | 10   | 20     |

  @modify_date
  Scenario Outline: Modify Date - Is Equal To/Before/After
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions  | user_input | secs | number |
    | Modify Date   | Is Equal To | 05/13/2016 | 10   | 1      |
    | Modify Date   | Before      | 08/08/2016 | 10   | 1      |
    | Modify Date   | After       | 02/08/2016 | 10   | 1      |

  #STORY BLOCK: @Anniversary @Birthday @Create Date @Modify Date

  @between_coverage
  Scenario Outline: Birthday/Anniversary/Create Date/Modify Date - Between
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in Start Date "<start_mm_dd_yy>" and End Date "<end_mm_dd_yy>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | start_mm_dd_yy | end_mm_dd_yy | secs | number |
    | Birthday      | Between    | 05/05          | 05/05        | 10   | 20     |
    | Anniversary   | Between    | 05/05          | 05/05        | 10   | 20     |
    | Modify Date   | Between    | 05/13/2016     | 05/14/2016   | 10   | 20     |
    | Create Date   | Between    | 05/14/2016     | 05/16/2016   | 10   | 20     |

  @accountmanager
  Scenario Outline: Acct.Manager - Equal/Not Equal
    And Go for Advanced Search
    And I wait for <secs> seconds
    And Select "<contact_field>" with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field | conditions | conditions_secondary | secs | number |
    | Acct. Manager | Equal      | Gen. Manager         | 10   | 4      |
    | Acct. Manager | Not Equal  | Acc. Manager         | 10   | 15     |

  @status
  Scenario Outline: Status - Equal/Not Equal
    And Go for Advanced Search
    And I wait for <secs> seconds
    And Select only Status with the conditions of "<conditions>" and select "<conditions_secondary>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | secs | conditions      | conditions_secondary | number |
    | 10   | Is Equal To     | New                  | 20     |
    | 10   | Is Not Equal To | Attempted            | 20     |

  #STORY BLOCK: Is Blank and Is Not Blank

  @age @lenght_of_residence @employee @passed
  Scenario Outline: Age/Length of Residence/Employee - Is Equal To/ Greater Than/ Is Not Equal To/ Less Than
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field       | conditions      | user_input | secs | number |
    | Age                 | Is Equal To     | 61         | 5    | 3      |
    | Age                 | Greater Than    | 30         | 5    | 20     |
    | Length of Residence | Is Not Equal To | 12         | 5    | 19     |
    | Employees           | Less Than       | 50         | 5    | 8      |

  #STORY BLOCK: Is Blank and Is Not Blank

  @blank @is_not_blank
  Scenario Outline: Company - Is Blank/Is Not Blank
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field         | conditions   | secs | number |
    | Company               | Is Blank     | 5    | 1      |
    | Company               | Is not Blank | 5    | 19     |
    | Contact               | Is Blank     | 5    | 1      |
    | Contact               | Is not Blank | 5    | 19     |
    | Email                 | Is Blank     | 5    | 6      |
    | Email                 | Is not Blank | 5    | 14     |
    | Fax                   | Is Blank     | 5    | 1      |
    | State                 | Is Blank     | 5    | 2      |
    | Zip                   | Is not Blank | 5    | 17     |
    | Annual Sales          | Is Blank     | 5    | 2      |
    | Year Business Started | Is not Blank | 5    | 18     |
    | Own or Rent           | Is Blank     | 5    | 1      |
    | Building Structure    | Is not Blank | 5    | 18     |
    | Religion              | Is Blank     | 5    | 3      |
    | Pool                  | Is not Blank | 5    | 16     |
    | Years in Business     | Is Blank     | 5    | 4      |
    | Custom 1              | Is not Blank | 5    | 14     |
    | Status                | Is not Blank | 5    | 20     |
    | Status                | Is Blank     | 5    | 0      |

    #Note: Above status field is not working.

#  @generic_conditions #http://prntscr.com/b4m68n
  Scenario Outline: Generic Fields - Is Equal To/ Begins With/ Contains/ Does not Contain
    And Go for Advanced Search
    And Select "<contact_field>" with the conditions of "<conditions>" and fill in "<user_input>"
    And I wait for <secs> seconds
    And I should see in Details View : "<number>"
    And I wait for <secs> seconds
  Examples:
    | contact_field      | conditions       | user_input  | secs | number |
    | Prefix             | Is Equal To      | Mohd        | 5    | 1      |
    | Title              | Begins With      | Mr          | 5    | 6      |
    | Phone              | Contains         | (616)       | 5    | 2      |
    | Mobile             | Does not Contain | (530)700287 | 5    | 19     |
    | Website            | Is Equal To      | alibaba.com | 5    | 1      |
    | Address            | Begins With      | Fa          | 5    | 2      |
    | Address 2          | Contains         | Me          | 5    | 2      |
    | City               | Does not Contain | S           | 5    | 9      |
    | Description        | Is Equal To      | Megha River | 5    | 1      |
    | Country            | Begins With      | Uni         | 5    | 16     |
    | SIC Code           | Contains         | 570-44-5024 | 5    | 1      |
    | SIC Description    | Does not Contain | non         | 5    | 14     |
    | Franchise          | Is Equal To      | TRUE        | 5    | 13     |
    | Location Type      | Begins With      | ma          | 5    | 2      |
    | Women Owned        | Contains         | FALSE       | 5    | 5      |
    | Estimated Income   | Does not Contain | $34,063.49  | 5    | 19     |
    | Marital Status     | Is Equal To      | Married     | 5    | 9      |
    | Gender             | Begins With      | Male        | 5    | 9      |
    | Children           | Contains         | 1           | 5    | 7      |
    | Ethnicity          | Does not Contain | In          | 5    | 13     |
    | Pets               | Is Equal To      | YES         | 5    | 9      |
    | Opportunity Seeker | Begins With      | NO          | 5    | 10     |
    | Charity Donor      | Contains         | YES         | 5    | 10     |
    | Office Location    | Does not Contain | le          | 5    | 16     |
    | Custom 2           | Is Equal To      | customteam2 | 5    | 14     |
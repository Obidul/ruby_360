@sbe
Feature: SBE - Add To Search

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Search By Example access to conduct the test execution
    Given Wait until the Application is ready

  @sbe_add_to_search @qp1
  Scenario: SBE - Add To Search - Company
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#companyname" & Start with "Myworks"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_add_to_search @sa
  Scenario: SBE - Add To Search - Contact
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#contact" & Start with "Sa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Salutation/Prefix
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#salutation" & Start with "Sr"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "25"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Title
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#title" & Start with "Ms"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "22"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Address
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#address" & Start with "4"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "15"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Address2
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#address2" & Start with "Oak Valley"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Phone
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#phone" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "7"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Cell/Mobile
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#cell" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Fax
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#fax" & Start with "4-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "9"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Ext
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#ext" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - State
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#state" & Start with "Ohio"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "10"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - City
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#city" & Start with "New York"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "5"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Zip
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#zip" & Start with "6"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "8"
    And I wait for 3 seconds

  @sbe_new @qp @em
  Scenario: SBE - Add To Search - Email
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with Email & Start with "sakibqa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "99"
    And I wait for 3 seconds

  @sbe_new @qp5 @es
  Scenario Outline: SBE - Add To Search - Email Status
    And I quick search with "<quick_search_field>" -- with enter function
    And Go for Search By Example
    And Add to Search with - "<selector>" & Start with "<text>"
    And Add to Search for Email Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | quick_search_field | selector     | text    | select_status    | number | secs |
    | Eabox              | #companyname | Myworks | Active           | 4      | 3    |
    | Eabox              | #companyname | Myworks | No Email Address | 2      | 3    |

  @sbe_new @qp6
  Scenario Outline: SBE - Add To Search - Status
    And I quick search with "<quick_search_field>" -- with enter function
    And Go for Search By Example
    And Add to Search with - "<selector>" & Start with "<text>"
    And Add to Search for Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | quick_search_field | selector     | text      | select_status | number | secs |
    | Eabox              | #companyname | Babbleset | Attempted     | 5      | 3    |
    | Eabox              | #companyname | Aivee     | Hot Prospect  | 2      | 3    |
    | Eabox              | #companyname | Jamia     | Hot Prospect  | 3      | 3    |

  @sbe_new @wip @qp
  Scenario: SBE - Add To Search - Team Owner
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search for Team Owner and selected "Sakib Mahmud" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "119"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Profile Description
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#ProfileDescription" & Start with "viva"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_new @qp8
  Scenario: SBE - Add To Search - Birthday
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#companyname" & Start with "Babbleset"
    And I wait for 3 seconds
    And Add to search for Birthday & selected Date is "February 1"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "4"
    And I wait for 3 seconds

  @sbe_new @qp9
  Scenario: SBE - Add To Search - Anniversary
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#companyname" & Start with "Myworks"
    And I wait for 3 seconds
    And Add to search for Anniversary & selected Date is "April 3"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Add To Search - Country
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#country" & Start with "Australia"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "22"
    And I wait for 3 seconds

  @sbe_new @qp
  Scenario: SBE - Add To Search - Website
    And I quick search with "Eabox" -- with enter function
    And Go for Search By Example
    And Add to Search with - "#website" & Start with "meetup.com"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "3"
    And I wait for 3 seconds

  #TODO: BELOW TEST REQUIRES SCROLLING FUNCTIONS =>> PULL FROM m_sbe_new_search.feature rest scenario
  
  

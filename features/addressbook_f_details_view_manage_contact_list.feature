Feature: Addressbook => Details View Test Execution
  Right Panel to select and Details View to Omit Check

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - List View access to conduct the test execution
    Given Wait until the Application is ready

  #STORY BLOCK: SECONDARY ACTION LIST

  @all_activity_today @activity
  Scenario: All Activity Today --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Activity Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_activity_today @activity
  Scenario: All Activity Today --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Activity Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @meeting_today @activity
  Scenario: Meetings Today --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Meetings Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @meeting_today @activity
  Scenario: Meetings Today --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Meetings Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @calls_today @activity
  Scenario: Calls Today --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Calls Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @calls_today @activity
  Scenario: Calls Today --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Calls Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @tasks_today @activity
  Scenario: Tasks Today --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tasks Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @tasks_today @activity
  Scenario: Tasks Today --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tasks Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: PRIMARY ACTION LIST

  @all_contacts @primary
  Scenario: All Contacts --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_contacts @primary
  Scenario: All Contacts --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "117"
    And I wait for 5 seconds

  @starred @primary
  Scenario: All Contacts --> Details View -> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Starred -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @starred @primary
  Scenario: All Contacts --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Starred -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: DEALS MENU

  @all_deals @deals
  Scenario: All Deals --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Deals from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_deals @deals
  Scenario: All Deals --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Deals from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @prospect @deals
  Scenario: Prospect --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Prospect from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @prospect @deals
  Scenario: Prospect --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Prospect from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @needs_analysis @deals
  Scenario: Needs Analysis --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Need Analysis from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @needs_analysis @deals
  Scenario: Needs Analysis --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Need Analysis from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @proposal @deals
  Scenario: Proposal --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Proposal from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @proposal @deals
  Scenario: Proposal --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Proposal from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @negotiations @deals
  Scenario: Negotiations --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @negotiations @deals
  Scenario: Negotiations --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @order_contract @deals
  Scenario: Order/Contract --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @order_contract @deals
  Scenario: Order/Contract --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: STATUS MENU

  @status_new @status
  Scenario: New --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select New from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_new @status
  Scenario: New --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select New from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_attempted @status
  Scenario: Attempted --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Attempted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_attempted @status
  Scenario: Attempted --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Attempted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_contacted @status
  Scenario: Contacted --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Contacted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_contacted @status
  Scenario: Contacted --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Contacted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_client @status
  Scenario: Client --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Client from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_client @status
  Scenario: Client --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Client from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_hot_prospect @status
  Scenario: Hot Prospect --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Hot Prospect from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_hot_prospect @status
  Scenario: Hot Prospect --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Hot Prospect from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_disqualified @status
  Scenario: Disqualified --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Disqualified from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_disqualified @status
  Scenario: Disqualified --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Disqualified from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: SUBSCRIPTION STATUS

  @three @subscriber
  Scenario: Active Subscribers --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Active Subscribers from Subscriptions Status
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @four @subscriber
  Scenario: Active Subscribers --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Active Subscribers from Subscriptions Status
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "97"
    And I wait for 5 seconds

  @five @subscriber
  Scenario: Inactive Subscribers --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Inactive Subscribers from Subscriptions Status
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @six @subscriber
  Scenario: Inactive Subscribers --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Inactive Subscribers from Subscriptions Status
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

  #note: Both Filter should be pre-created and number of contact should be enlisted.

  #STORY BLOCK: TAGS

  @tagged
  Scenario: Selected Available Tags --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
  #   And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
  #   And To create Tags I fill in "MyTagsChecked" and save it
  #   And I wait for 5 seconds
    And In Tags Tab, I select "Tags Group" this tag
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @tagged
  Scenario: Selected Available Tags --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tags Under Tags Tab
  #   And I wait for 5 seconds
  #   And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
  #   And To create Tags I fill in "MyTagsOmitted" and save it
    And I wait for 5 seconds
    And In Tags Tab, I select "Tags Group" this tag
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

  #note: Both Filter should be pre-created and number of contact should be enlisted.

  #STORY BLOCK: FILTER

  @nine @filter
  Scenario: Selected Available Filter --> LV --> I select checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
  # Given In Filter Tab I select "No Filters" and select "New Filter" -- while in Addressbook
  # And To create Filter - I fill in "MyFilterChecked" and save as "MyFilterChecked" -- while in Addressbook
  # And I wait for 5 seconds
    And In Filter Tab I select "Test Filter Group" this filter
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @ten @filter
  Scenario: Selected Available Filter --> LV --> I omit checked -> DV
    And I wait for 30 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
  # Given In Filter Tab I select "No Filters" and select "New Filter" -- while in Addressbook
  # And To create Filter - I fill in "MyFilterChecked" and save as "MyFilterChecked" -- while in Addressbook
  # And I wait for 5 seconds
    And In Filter Tab I select "Test Filter Group" this filter
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

  #STORY BLOCK: TEAM MENU

  @team @me
  Scenario: Team --> Details View -- I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Me from Team menu -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @team @me
  Scenario: Team --> Details View -- I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Me from Team menu -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

    #Note: Number of 25 contacts should be available before this scenario execution

  @team @subuser @wip
  Scenario: Team --> Details View -- I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select sub-user of "Mr. Sub User" from Team Menu -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @team @subuser @wip
  Scenario: Team --> Details View -- I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select sub-user of "Mr. Sub User" from Team Menu -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    And I switch to Contact Details View
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds
@action_items
Feature: Dashboard - Right Panel Functionality

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Right Panel Functionality access to conduct the test execution
    Given Wait until the Application is ready
    Given Close Support options

  #STORY BLOCK: ACTIVITY TAB

  @all_activity_view_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> All Activity -> View Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "All Activity" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @all_activity_email_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> All Activity -> Email Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "All Activity" and select "Email Contact"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @meeting_view_contact @activity @ads
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Meeting -> View Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Meeting" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @meeting_email_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Meeting -> Email Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Meeting" and select "Email Contact"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @call_view_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Call -> View Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Calls" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @call_email_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Call -> Email Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Calls" and select "Email Contact"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @tasks_view_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Tasks -> View Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Tasks" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @tasks_email_contact @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Tasks -> Email Contact
    And I wait for 5 seconds
    Given In Activity Tab I select "Tasks" and select "Email Contact"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  #STORY BLOCK: DEALS TAB

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> All Deals -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "All Deals" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals @hamham
  Scenario: As Admin User, Right Panel -> In Deals Tab -> All Deals -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "All Deals" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals @hamham
  Scenario: As Admin User, Right Panel -> In Deals Tab -> All Deals -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "All Deals" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Prospect -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Prospect" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Prospect -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Prospect" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Prospect -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Prospect" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Needs Analysis -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Needs Analysis" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Needs Analysis -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Needs Analysis" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Needs Analysis -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Needs Analysis" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Proposal -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Proposal" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Proposal -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Proposal" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Proposal -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Proposal" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Negotiations -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Negotiations" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Negotiations -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Negotiations" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Negotiations -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Negotiations" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals @tim
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Order/Contracts -> Email Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Order/Contract" and select "Email Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals @tim
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Order/Contracts -> View Contact
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Order/Contract" and select "View Contact" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @deals @tim
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Order/Contracts -> Edit Deal Stage
    And I wait for 5 seconds
    Given I select Deals from Primary Right Panel -- while in Action Stream
    Given In Activity Tab I select "Order/Contract" and select "Edit Deal Stages" -- while in Deals Menu
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  #STORY BLOCK: TAGS TAB

  @tagtab
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Create Tags
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    Given In Tags Tab I select "No Tags" and select "New Tag"
    Then To create Tags I fill in "My Tags" and save it
    And I wait for 5 seconds

  @tagtab
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Rename Tags
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    And I wait for 3 seconds
    Given I found existing "My Tags" tag and rename to "Hello Tag"
    And I wait for 5 seconds

  @tagtab
  Scenario: As Admin User, Right Panel -> In Tags Tab -> View Contact
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    Given In Tags Tab I select "Hello Tag" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @tagtab
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Email Contact
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    Given In Tags Tab I select "Hello Tag" and select "View Contact"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @tagtab @del
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Delete Tag
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    Given In Tags Tab I select "Hello Tag" and select "Delete Tag"
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: FILTERS

  @filter1
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Create Filter
    And I wait for 5 seconds
    Given I select Filter Under Filter Tab
    Given In Filter Tab I select "No Filters" and select "New Filter"
    And To create Filter - I fill in "My Filter" and save as "My Filter"
    And I wait for 5 seconds

  @filter1
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Rename Tags
    And I wait for 5 seconds
    Given I select Filter Under Filter Tab
    And I wait for 3 seconds
    Given I found existing "My Filter" filter and rename to "My New Filter"
    And I wait for 5 seconds

  @filter1
  Scenario: As Admin User, Right Panel -> In Filter Tab -> View Contact
    And I wait for 5 seconds
    Given I select Filter Under Filter Tab
    Given In Filter Tab I select "My New Filter" and select "View Contact"
    And I wait for 5 seconds
  #    And I wait until View Contacts - Details View is visible

  @filter1
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Email Contact
    And I wait for 5 seconds
    Given I select Filter Under Filter Tab
    Given In Filter Tab I select "My New Filter" and select "View Contact"
    And I wait for 5 seconds
  #    And I wait until Email Contact - Email Compose Module is visible

  @filter1
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Delete Filter
    And I wait for 5 seconds
    Given I select Filter Under Filter Tab
    Given In Tags Tab I select "My New Filter" and select "Delete Filter"
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: STATUS

  @status_new
  Scenario: As Admin User, Right Panel -> From Status Tab -> New -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    Given From Status Tab I select "New" and select "Email Contacts"
    And I wait for 3 seconds
    And I wait until Email Contact - Email Compose Module is visible
    And I wait for 5 seconds

  @status_new
  Scenario: As Admin User, Right Panel -> From Status Tab -> New -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    Given From Status Tab I select "New" and select "View Contacts"
    And I wait for 3 seconds
    And I wait until View Contacts - Details View is visible
    And I wait for 5 seconds

  @status_new @hamham
  Scenario: As Admin User, Right Panel -> From Status Tab -> New -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    Given From Status Tab I select "New" and select "Edit Status"
    And I wait for 3 seconds
    And I wait until Edit Status -  Edit Status page is visible
    And I wait for 5 seconds

  @status_attempted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Attempted -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    Given From Status Tab I select "Attempted" and select "Email Contacts"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_attempted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Attempted -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    Given From Status Tab I select "Attempted" and select "View Contacts"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @status_attempted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Attempted -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    Given From Status Tab I select "Attempted" and select "Edit Status"
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_contacted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Contacted -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Contacted" and select "Email Contacts"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_contacted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Contacted -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Contacted" and select "View Contacts"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @status_contacted
  Scenario: As Admin User, Right Panel -> From Status Tab -> Contacted -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Contacted" and select "Edit Status"
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_client
  Scenario: As Admin User, Right Panel -> From Status Tab -> Client -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Client" and select "Email Contacts"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_client
  Scenario: As Admin User, Right Panel -> From Status Tab -> Client -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Client" and select "View Contacts"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @status_client
  Scenario: As Admin User, Right Panel -> From Status Tab -> Client -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Client" and select "Edit Status"
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_hot_prospect
  Scenario: As Admin User, Right Panel -> From Status Tab -> Hot Prospect -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Hot Prospect" and select "Email Contacts"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_hot_prospect
  Scenario: As Admin User, Right Panel -> From Status Tab -> Hot Prospect -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Hot Prospect" and select "View Contacts"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @status_hot_prospect
  Scenario: As Admin User, Right Panel -> From Status Tab -> Hot Prospect -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Hot Prospect" and select "Edit Status"
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_disqualified
  Scenario: As Admin User, Right Panel -> From Status Tab -> Disqualified -> Email Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Disqualified" and select "Email Contacts"
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_disqualified
  Scenario: As Admin User, Right Panel -> From Status Tab -> Disqualified -> View Contacts
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Disqualified" and select "View Contacts"
    And I wait for 5 seconds
    And I wait until View Contacts - Details View is visible

  @status_disqualified
  Scenario: As Admin User, Right Panel -> From Status Tab -> Disqualified -> Edit Status
    And I wait for 5 seconds
    Given I select Status Tab - while in Action Stream module
    And I wait for 3 seconds
    And From Status Tab I select "Disqualified" and select "Edit Status"
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  #STORY BLOCK: TEAM

  Scenario: Team related scenario

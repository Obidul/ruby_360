@addressbook
Feature: Go for Quick search Test Execution - List View
  Quick Search with -- click functions
  Quick Search with -- enter functions
  Quick Search without -- company name and contact field and others field empty
  Quick Search with - 15 Field Data Verification
  Quick search results to select checked

#Note: (LV) = List View & (DV) = Details View

  Background: Prerequisites
     Given I want to maximize the windows of browser
     Given As Admin User, Addressbook - Quick Search Activity access to conduct the test execution
#     Given Local Quick Search
     Given Wait until the Application is ready
     Given I wait for 10 seconds

  @test1
  Scenario: (LV) Quick Search with -- click functions
    And Go for Contact Module
    And I switch to Contact List View
    And I quick search with "An" -- with click function
    And I select "Anne Andrews, Eabox" from the list of autocomplete -- with click selection
    And I wait for 3 seconds

  @test2
  Scenario: (LV) Quick Search with -- enter functions
    And Go for Contact Module
    And I switch to Contact List View
    And I quick search with "An" -- with enter function
    And I wait for 3 seconds
    And I should see in Search Results : "4"

  @test3
  Scenario: (LV) Quick Search with -- enter functions (No Company Name & Contact Name & Empty Others Field)
    And Go for Contact Module
    And I switch to Contact List View
    And I quick search with "sakibqa+38@gmail.com" -- with enter function and no company and contact info not found
    And I should see in Search Results : "1"
    And I wait for 3 seconds

  @test4
  Scenario Outline: (LV) Quick Search with - 15 fields verification
    And Go for Contact Module
    And I switch to Contact List View
    And I wait for <secs> seconds
    And I quick search with "<quick_search_field>" -- with enter function
    And I should see in Search Results : "<number>"
    And I wait for <secs> seconds
  Examples:
    | quick_search_field  | number | secs |
    | Eabox               | 2      | 5    |
    | An                  | 4      | 5    |
    | Sr                  | 10     | 5    |
    | Mrs                 | 7      | 5    |
    | sakibqa+22@gmail.com| 1      | 5    |
    | (404) 825-3925      | 1      | 5    |
#    | 49321               | 1      | 5    |
    | (937) 942-8625      | 1      | 5    |
    | (309) 750-0031      | 1      | 5    |
    | Attempted           | 2      | 5    |
    | North Carbon Road   | 1      | 5    |
    | Corben              | 1      | 5    |
    | New York            | 4      | 5    |
    | Michigan            | 1      | 5    |
    | 20503-79950         | 1      | 5    |

  @test5
  Scenario: (LV) Quick Search with -- Partially adding/deleting contact name and notice change in quick search result
    And Go for Contact Module
    And I switch to Contact List View
    And I quick search with "Wayne" -- In Drop-down Updates Information
    And In quick search I should see "Wayne Spencer, Centizu"
    And I quick search with "Wa" -- In Drop-down Updates Information
    And I wait for 3 seconds
    And In quick search I should see "Doris Watson, Devpulse"
    And In quick search I should see "Wayne Spencer, Centizu"
    And In quick search I should see "Willie Williams, Eabox"
    And In quick search I should see "Dennis Ellis, Eazzy"
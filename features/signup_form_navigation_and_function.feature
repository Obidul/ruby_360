@signup
Feature: Go to Signup form navigations and functionality

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Signup form access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

  #STORY BLOCK: Signup form Navigations : Folder View

  @s1
  Scenario: As admin user, Click on the Red Plus Sign
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen

  @s2
  Scenario: As admin user, Click on the Done button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Done button on the Signup Form page
    Then I should see "Select a Design" on screen

  @s3
  Scenario: As admin user, Create a Form
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I create Name Webform: "My Second Form" and click on the start button
    And I should see "Form Completed" on the screen
    Then I click on the cross button
    Then I should see "Signup Forms" on the screen

  @s4
  Scenario: As admin user, Inline click on the form to edit
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the form name: "My Second Form"
    Then I should see "Signup Forms" on screen

  @s5
  Scenario: As admin user, Click on the thumbnail to edit
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the thumbnail of the form name: "My Second Form"
    Then I should see "Signup Forms" on screen

  @s6 @folder_edit
  Scenario: As admin user, Activate the Folder edit dropdown
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I wait for 3 seconds

  @s7 @folder_edit
  Scenario: As admin user, Navigate to Folder edit dropdown -> New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "New Folder" from the dropdown
    And I wait for 3 seconds

  @s8 @folder_edit
  Scenario: As admin user, Navigate to Folder edit dropdown -> Rename Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "Rename Folder" from the dropdown
    And I wait for 3 seconds

  @s9 @folder_edit
  Scenario: As admin user, Navigate to Folder edit dropdown -> Delete Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "Delete Folder" from the dropdown
    And I wait for 3 seconds

  @s10 @form_edit
  Scenario: As admin user, Activate the Form edit dropdown
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I wait for 3 seconds

  @s11 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Edit Design
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Edit Design" from the dropdown
    Then I should see "Signup Forms" on screen

  @s12 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Edit Settings
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Edit Settings" from the dropdown
    Then I should see "Display Settings" on the screen

  @s13 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Edit Group
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Edit Group" from the dropdown
    And I should see "Assign Group" on the screen

  @s14 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Preview
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Preview" from the dropdown
    Then The page should have selector: ".full-page"

  @s15 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Get Code
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Get Code" from the dropdown
    And I should see "Form Completed" on the screen

  @s16 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Active Form
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Pause Form" from the dropdown
    Then I should see the status changed from Active to Paused

  @s17 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Paused
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Activate Form" from the dropdown
    Then I should see the status changed from Paused to Active

  @s18 @form_edit
  Scenario: As admin user, Navigate to Form edit dropdown > Delete
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My Second Form"
    And I select "Delete" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  #STORY BLOCK: Signup form Functionality : Folder View

  @s19 @form_delete
  Scenario: As admin user, Delete a form
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I quickly create a Form
    Then I click on the Edit button of the form name "Sample Form"
    And I select "Delete" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  @s20 @folder_edit
  Scenario: As admin user, Create a New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "New Folder" from the dropdown
    Then I create a New Folder : "My New Folder"
    Then I should see "My New Folder" on screen

  @s20-1 @folder_edit @duplicate
  Scenario: As admin user, Create new folder with Duplicate folder name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "New Folder" from the dropdown
    Then I create a New Folder : "Test"
    Then I should see popup message : "A folder with same name already exists." on the screen

  @s21 @folder_edit @folder_edit
  Scenario: As admin user, Rename a New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "My New Folder" on Grid list and I clicked the edit dropdown
    And I select "Rename Folder" from the dropdown
    Then I rename a New Folder to : "My New Folder renamed"
    Then I should see "My New Folder renamed" on screen
    Then I delete the renamed folder

  @s21-1 @folder_edit @duplicate
  Scenario: As admin user, Rename a New Folder with Duplicate folder name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "Rename Folder" from the dropdown
    Then I create a New Folder : "Test"
    Then I should see popup message : "A folder with same name already exists." on the screen

  @s22 @folder_edit
  Scenario: As admin user, Delete a New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I quickly create a Folder
    Then I found folder: "Folder for Delete" on Grid list and I clicked the edit dropdown
    And I select "Delete Folder" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  #STORY BLOCK: Signup form Navigations : Category view page

  @s23 @category_view
  Scenario: As admin user, Go to the Category view page
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen

  @s24 @category_view
  Scenario: As admin user, Click on the Cross button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I click on the cross button
    Then I should see "Select a Design" on screen

  @s25 @category_view
  Scenario: As admin user, Click a Category
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen

  #STORY BLOCK: Signup form Navigations : Template view page

  @s26 @template_view
  Scenario: As admin user, Go to the Template view page
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen

  @s27 @template_view
  Scenario: As admin user, Click on the Cross button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I click on the cross button
    Then I should see "Select a Design" on screen

  @s28 @template_view
  Scenario: As admin user, Select a template
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I wait for 3 seconds

  #STORY BLOCK: Signup form Navigations : Edit form page

  @s29 @form_editor
  Scenario: As admin user, Go to Edit form page
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I wait for 3 seconds

  @s30 @form_editor
  Scenario: As admin user, Click on the Save Draft button and save the template
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Save Draft button on the signup
    Then I save the form as a draft named : "My Draft Form"
    And I click on the Cancel button on the signup
    Then I should see "Select a Design" on screen
    Then I clicked on "Signup Forms"
    And I should see the status changed to Draft Version
    Then I click on the Edit button of the form name "My Draft Form"
    And I select "Delete" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  @s30-1 @form_editor @duplicate
  Scenario: As admin user, Click on the Save Draft button and save the template with Duplicate form name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Save Draft button on the signup
    Then I save the form as a draft named : "My First Form"
    Then I should see popup message : "A form with same name already exists. Do you want to replace it?" on the screen


  @s31 @form_editor
  Scenario: As admin user, Click on the Save Draft button and Cancel the form
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Save Draft button on the signup
    Then I click on the Cancel button of the save popup
    And I wait for 3 seconds

  @s32 @form_editor
  Scenario: As admin user, Click on the Preview button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Preview button on the signup
    Then I should see "Desktop - Tablet" on screen

  @s33 @form_editor
  Scenario: As admin user, Click on the Cancel button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Cancel button on the signup
    Then I should see "Select a Design" on screen

  @s34 @form_editor
  Scenario: As admin user, Click on the Source button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Source button on the signup

  @s35 @form_editor
  Scenario: As admin user, Click on the next button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen

  #STORY BLOCK: Signup form Navigations : Preview page

  @s36 @preview
  Scenario: As admin user, Change view from Desktop Preview to Mobile Preview
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Preview button on the signup
    Then I should see "Desktop - Tablet" on screen
    Then I select the Mobile view button
    And I should see "Mobile Preview" on screen

  @s37 @preview
  Scenario: As admin user, Change view from Mobile Preview to Desktop Preview
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Preview button on the signup
    Then I select the Mobile view button
    And I should see "Mobile Preview" on screen
    Then I select the Desktop view button
    And I should see "Desktop - Tablet" on screen

  @s38 @preview
  Scenario: As admin user, Click on the cross button from Mobile Preview page
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Preview button on the signup
    Then I select the Mobile view button
    And I should see "Mobile Preview" on screen
    Then I click on the Cross button of the preview page
    And I should see "Signup Forms" on screen

  @s39 @preview
  Scenario: As admin user, Click on the cross button from Desktop Preview page
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I click on the Preview button on the signup
    Then I should see "Desktop - Tablet" on screen
    Then I click on the Cross button of the preview page
    And I should see "Signup Forms" on screen

  #STORY BLOCK: Signup form Navigations : Display Settings

  @s40 @display_settings
  Scenario: As admin user, Click on the Cancel button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    And I click on the Cancel button of the signup
    Then I should see "Select a Design" on screen

  @s41 @display_settings
  Scenario: As admin user, Click on the Next button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen

  @s42 @display_settings @behavior
  Scenario: As admin user, Select the Check box of Behavior : Seconds Before Showing
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    And I select the checkbox for : "Seconds Before Showing"
    And I wait for 3 seconds

  @s43 @display_settings @behavior
  Scenario: As admin user, Select the Check box of Behavior : Show After Scrolling
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    And I select the checkbox for : "Show After Scrolling"
    And I wait for 3 seconds

  @s44 @display_settings @behavior
  Scenario: As admin user, Select the Check box of Behavior : Show On Page Exit
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    And I select the checkbox for : "Show On Page Exit"
    And I wait for 3 seconds

  @s45 @display_settings @behavior
  Scenario: As admin user, Change the value of Behavior : Seconds Before Showing
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I change the data to : "50" for : "Seconds Before Showing"
    And I wait for 3 seconds

  @s46 @display_settings @behavior
  Scenario: As admin user, Change the value of Behavior : Show After Scrolling
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I change the data to : "10" for : "Show After Scrolling"
    And I wait for 3 seconds

  @s47 @display_settings @frequency
  Scenario: As admin user, Change the value of Frequency : Input Field
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I change the value to : "5" for Frequency
    And I wait for 3 seconds

  @s48 @display_settings @frequency
  Scenario: As admin user, Change the value of Frequency : Select from drop-down
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I change the type value to : "always show" for Frequency
    And I wait for 3 seconds

  @s49 @display_settings @page_visibility
  Scenario: As admin user, Select the Radio button of Page Visibility : Show Always
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the radio button of : "Show Always" for Page Visibility
    And I wait for 3 seconds

  @s50 @display_settings @page_visibility
  Scenario: As admin user, Select the Radio button of Page Visibility : Hide on specific pages
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the radio button of : "Hide on specific pages" for Page Visibility
    And I wait for 3 seconds

  @s51 @display_settings @page_visibility
  Scenario: As admin user, Select the Radio button of Page Visibility : Only Show on Specific Pages
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the radio button of : "Only Show on Specific Pages" for Page Visibility
    And I wait for 3 seconds

  @s52 @display_settings @page_visibility
  Scenario: As admin user, Select and add contents of the Radio button of Page Visibility : Hide on specific pages
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the radio button of : "Hide on specific pages" for Page Visibility
    Then I include URLs:
      """
      www.test.com
      www.junk.com
      """
    And I wait for 3 seconds

  @s53 @display_settings @page_visibility
  Scenario: As admin user, Select and add contents of the Radio button of Page Visibility : Only Show on Specific Pages
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the radio button of : "Only Show on Specific Pages" for Page Visibility
    Then I include URLs:
      """
      www.test.com
      www.junk.com
      """
    And I wait for 3 seconds

  @s54 @display_settings @page_visibility
  Scenario: As admin user, Select the Check box of Page Visibility : Hide on Mobile
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the check box of : "Hide on Mobile" for Page Visibility
    And I wait for 3 seconds

  @s55 @display_settings @page_visibility
  Scenario: As admin user, Select the Check box of Page Visibility :  Hide on Tablet
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I select the check box of : "Hide on Tablet" for Page Visibility
    And I wait for 3 seconds

  #STORY BLOCK: Signup form Navigations : Assign group page

  @s56 @group_assign
  Scenario: As admin user, Click on the Cancel button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the Cancel button of the signup
    And I should see "Select a Design" on screen

  @s57 @group_assign
  Scenario: As admin user, Click on the Save button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I wait for 3 seconds

  @s58 @group_assign
  Scenario: As admin user, Select a group from the groups list
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I select "Import" group from the group list of singup form
    And I wait for 3 seconds

  @s59 @group_assign
  Scenario: As admin user, Save popup: Enter a form name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I enter a new Webform Name: "My Fourth Form"
    And I wait for 3 seconds

  @s60 @group_assign
  Scenario: As admin user, Save popup: Click on the Cancel button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    Then I click on the Cancel button of the save popup
    And I wait for 3 seconds

  @s61 @group_assign
  Scenario: As admin user, Save popup: Click on the Start button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    Then I click on the Start button of the save popup
    And I wait for 3 seconds

  @s62 @group_assign
  Scenario: As admin user, Save popup: Save a form
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I create Name Webform: "My Third Form" and click on the start button
    Then Goto the Signup form folder view and Delete the form

  @s62-1 @group_assign @duplicate
  Scenario: As admin user, Save popup: Save a form with Duplicate name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I create Name Webform: "My First Form" and click on the start button
    Then I should see popup message : "A form with same name already exists. Do you want to replace it?" on the screen


  @s63 @group_assign
  Scenario: As admin user, Save popup: Click on the New folder icon
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I wait for 3 seconds

  @s64 @group_assign
  Scenario: As admin user, Save popup: Change different Folder from the dropdown
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I select the folder : "Earth" from the dropdown
    Then I wait for 3 seconds

  @s65 @group_assign
  Scenario: As admin user, Save popup -> New Folder popup: Enter folder name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I enter a new folder name: "My Office Work"
    And I wait for 3 seconds

  @s66 @group_assign
  Scenario: As admin user, Save popup -> New Folder popup: Click on the OK button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I click on the OK button of the new folder popup
    And I wait for 3 seconds

  @s67 @group_assign
  Scenario: As admin user, Save popup -> New Folder popup: Click on the Cancel button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I select on the Cancel button of the new folder popup
    And I wait for 3 seconds

  @s68 @group_assign
  Scenario: As admin user, Save popup -> New Folder popup: Create a New Folder
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I enter a folder name : "Temp Folder" and press Ok button
    Then Goto the Signup form folder view and Delete the folder

  @s68-1 @group_assign @duplicate
  Scenario: As admin user, Save popup -> New Folder popup: Enter folder name with Duplicate name
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I click on the New Folder Icon
    Then I enter a folder name : "Test" and press Ok button
    Then I should see popup message : "A folder with same name already exists." on the screen

      #STORY BLOCK: Signup form Navigations : Form completed page

  @s69 @form_completed
  Scenario: As admin user, Click on the Video Tutorial Link
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My First Form"
    And I select "Get Code" from the dropdown
    Then I should see "Form Completed" on the screen
    And I click on the link : "Video Tutorial"
    Then I wait for 3 seconds

  @s70 @form_completed
  Scenario: As admin user, Click on the Copy Code Link
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My First Form"
    And I select "Get Code" from the dropdown
    Then I should see "Form Completed" on the screen
    And I click on the link : "Copy Code"
    Then I wait for 3 seconds

  @s71 @form_completed
  Scenario: As admin user, Folder view -> Form edit dropdown -> Get Code -> Click on the Cross button
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My First Form"
    And I select "Get Code" from the dropdown
    Then I should see "Form Completed" on the screen
    And I click on the cross button
    Then I should see "Signup Forms" on the screen

  @s72 @form_completed
  Scenario: As admin user, Click on the Cross button - Whole form creation process
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I create Name Webform: "My Third Form" and click on the start button
    And I should see "Form Completed" on the screen
    Then I click on the cross button
    Then I should see "Signup Forms" on the screen
    Then Now goto the Signup form folder view and Delete the form

  @count
  Scenario: As admin user, Webform Count Check
    And I wait for 3 seconds
    Then I click on Email Tab
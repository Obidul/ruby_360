@addressbook
Feature: Addressbook Counter Verification

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook Counter Updates Verification - Access to conduct the test execution
    Given Wait until the Application is ready
    Given Close Support options
    And I wait for 10 seconds

#Note:
#In left body, we add some demo contents in all aspects where counter changes or updates.
#Then we match in details view and right counter.

#STORY BLOCK: Right Side Counter Verification

#Primary Action List

  Scenario: As Admin, All Contacts -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select All Contacts -- while in Addressbook
    And Number of All Contacts and Total counter matched

  Scenario: As Admin, Starred -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Starred -- while in Addressbook
    And Number of Starred and Total counter matched

  Scenario: As Admin, New Leads -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select New Leads -- while in Addressbook
    And Number of New Leads and Total counter matched

#Secondary Action List Tab

  Scenario: As Admin, Activity Tab -> All Activity Today -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select All Activity Today from Activity Tab -- while in Addressbook
    And Number of All Activity Today and Total counter matched

  Scenario: As Admin, Activity Tab -> Meetings Today -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Meetings Today from Activity Tab -- while in Addressbook
    And Number of Meetings Today and Total counter matched

  Scenario: As Admin, Activity Tab -> Calls Today -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Calls Today from Activity Tab -- while in Addressbook
    And Number of Calls Today and Total counter matched

#Secondary Action List Tab

  Scenario: As Admin, Deals Tab -> All Deals -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select All Deals from Deals -- while in Addressbook
    And Number of All Deals and Total counter matched

  Scenario: As Admin, Deals Tab -> Prospect -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Proposal from Deals -- while in Addressbook
    And Number of Prospect and Total counter matched

  Scenario: As Admin, Deals Tab -> Needs Analysis -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Need Analysis from Deals -- while in Addressbook
    And Number of Needs Analysis and Total counter matched

  Scenario: As Admin, Deals Tab -> Proposal -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Proposal from Deals -- while in Addressbook
    And Number of Prospect and Total counter matched

  Scenario: As Admin, Deals Tab -> Negotiations -> Counter Verification
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Negotiations from Deals -- while in Addressbook
    Then Number of Negotiations and Total counter matched

  Scenario: As Admin, Deals Tab -> Order/Contract -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Order&Contract from Deals -- while in Addressbook
    Then Number of Order&Contract and Total counter matched

#STORY BLOCK: STATUS TAB

  Scenario: As Admin, Status Menu - New -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select New from Status - while in Addressbook module
    Then Number of New Opportunity and Total counter matched

  Scenario: As Admin, Status Menu - Attempted -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Attempted from Status - while in Addressbook module
    Then Number of Attempted Contact and Total counter matched

  Scenario: As Admin, Status Menu - Contacted -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Contacted from Status - while in Addressbook module
    Then Number of Contacted and Total counter matched

  Scenario: As Admin, Status Menu - Hot Prospect -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Hot Prospect from Status - while in Addressbook module
    And Number of Hot Prospect and Total counter matched

  Scenario: As Admin, Status Menu - Client -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Client from Status - while in Addressbook module
    And Number of Client and Total counter matched

  Scenario: As Admin, Status Menu - Disqualified -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Disqualified from Status - while in Addressbook module
    And Number of Disqualified and Total counter matched

#STORY BLOCK: SUBSCRIPTION STATUS

  Scenario: As Admin, Subscription Status -> Active Subscribers -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Active Subscribers from Subscriptions Status
    Then Number of Active Subscribers and Total counter matched

  Scenario: As Admin, Subscription Status -> Inactive Subscribers -> Counter Verification
    Given I wait for 5 seconds
    Given I click on Contacts Tab
    And I wait for 5 seconds
    And I select Inactive Subscribers from Subscriptions Status
    And Number of Inactive Subscribers and Total counter matched

 #STORY BLOCK: TAGS
    #  Scenario: Secondary Action List -> Tags Tab -> Tags
  Scenario: As Admin, Tags


  #STORY BLOCK: FILTER

  #  Scenario: Secondary Action List -> Filter Tab -> Filter

  #STORY BLOCK: TEAM

Feature: Dashboard Activity - Timezone Verification

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Timezone Verification to access to conduct the test execution
    Given Wait until the Application is ready

  ##STORY BLOCK: GMT+ 6 Time.

  @gmt
  Scenario: As Admin User, Time Set Default - 12:00 PM
    Given I am on the "/App" page
    And I create an activity with "Time Set - Default Twelve PM"
    And I select activity type "Meeting"
    And I searched for "Mat" & select "Matthew Williamson, Buzzdog" from the list
    Then I click to Save button
    Then I should see "12:00 PM" & "Meeting - Buzzdog" & "Time Set - Default Twelve PM" has enlisted
    And I wait for 3 seconds

  @gmt @match
  Scenario: As Admin User, Time Set 09:00 AM
    Given I am on the "/App" page
    And I create activity of "Time Set - Nine AM" and with custom time: "09:00 AM"
    And I select activity type "Call"
    And I searched for "Ant" & select "Anthony Harvey, Abata" from the list
    Then I click to Save button
    And I wait for 5 seconds
    Then I should see "09:00 AM" & "Call - Abata" & "Time Set - Nine AM" has enlisted
    And I wait for 3 seconds

  @gmt
  Scenario: As Admin User, Time Set 03:00 PM
    Given I am on the "/App" page
    And I create activity of "Time Set - Three AM" and with custom time: "03:00 PM"
    And I select activity type "Call"
    And I searched for "Larr" & select "Larry Cooper, Kaymbo" from the list
    Then I click to Save button
    Then I should see "03:00 PM" & "Call - Abata" & "Time Set - Three AM" has enlisted
    And I wait for 3 seconds

  @gmt
  Scenario: As Admin User, Time Set 09:00 PM
    Given I am on the "/App" page
    And I create activity of "Time Set - Nine PM" and with custom time: "09:00 PM"
    And I select activity type "Task"
    And I searched for "Mil" & select "Mildred Perez, Kwideo" from the list
    Then I click to Save button
    Then I should see "09:00 PM" & "Task - Kwideo" & "Time Set - Nine PM" has enlisted
    And I wait for 3 seconds

  @gmt @today_gmt @tomorrow
  Scenario: As Admin User, Time Set 12:15 PM (GMT +6 Time)
    Given I am on the "/App" page
    And I create activity of "Time Set - Twelve Fifteen PM Next Day" and set custom time: "12:15 PM"
    And Within Calender Range - I select Tomorrow
    And I select activity type "Meeting"
    And I searched for "Pame" & select "Pamela Phillips, Leexo" from the list
    Then I click to Save button
    And I wait for 5 seconds
#    And I select Next Date from Calender
#    And I wait for 3 seconds
    Then I should see "12:15 PM" & "Meeting - Leexo" & "Time Set - Twelve Fifteen PM Next Day" has enlisted
    And I wait for 3 seconds

  @gmt
  Scenario: As Admin User, Time Set 12:00 AM
    Given I am on the "/App" page
    And I create activity of "Time Set - Twelve AM" and with custom time: "12:00 AM"
    And I select activity type "Meeting"
    And I searched for "Jen" & select "Jennifer Hunter, Gigazoom" from the list
    Then I click to Save button
    Then I should see "Anytime" & "Meeting - Gigazoom" & "Time Set - Twelve AM" has enlisted
    And I wait for 3 seconds

  ##STORY BLOCK: UTC +0

  #manual switch requires to "UTC"

  @utc
  Scenario: As Admin User, Time Match : Set Default - 12:00 PM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    Then I should see "01:00 AM" & "Meeting - Buzzdog" & "Time Set - Default Twelve PM" has enlisted
    And I wait for 1 seconds

  @utc @yesterday @match
  Scenario: As Admin User, Time Match : 09:00 AM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    And Within Calender Range - I select Yesterday
    And I wait for 5 seconds
    Then I should see "10:00 PM" & "Call - Abata" & "Time Set - Nine AM" has enlisted
    And I wait for 1 seconds

  @utc
  Scenario: As Admin User, Time Match 03:00 PM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    Then I should see "04:00 AM" & "Call - Kaymbo" & "Time Set - Three AM" has enlisted
    And I wait for 1 seconds

  @utc
  Scenario: As Admin User, Time Match 09:00 PM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    Then I should see "10:00 AM" & "Task - Kwideo" & "Time Set - Nine PM" has enlisted
    And I wait for 1 seconds

  @utc @today_gmt @tomorrow
  Scenario: As Admin User, Time Set 12:15 PM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    And Within Calender Range - I select Tomorrow
    And I wait for 5 seconds
    Then I should see "01:15 AM" & "Meeting - Leexo" & "Time Set - Twelve Fifteen PM Next Day" has enlisted
    And I wait for 3 seconds

  @utc @done @yesterday
  Scenario: As Admin User, Time Match 12:00 AM (GMT +6 Time)
    Given I am on the "/App" page
    And I wait for 5 seconds
    And Within Calender Range - I select Yesterday
    And I wait for 5 seconds
    Then I should see "Anytime" & "Meeting - Gigazoom" & "Time Set - Twelve AM" has enlisted
    And I wait for 1 seconds

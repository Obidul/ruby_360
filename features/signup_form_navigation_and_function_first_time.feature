@signup
Feature: Go to Signup form navigate for the first time

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, First time Signup form access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

    #STORY BLOCK: Signup form For the first time

  @s1
  Scenario: As admin user, Empty Folder View
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I should see "No Signup Forms Active" on screen

  @s2
  Scenario: As admin user, Empty Folder View -> Lets Get Started
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on Lets get started button
    And I should see "Create a Signup Form" on the screen

  @s3
  Scenario: As admin user, Create a form for the first time
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on Lets get started button
    And I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Basic" form template
    Then I click on the signup form Next button
    And I should see "Display Settings" on the screen
    Then I click on the signup form Next button
    And I should see "Assign Group" on the screen
    Then I click on the signup form Save button
    And I create a Name Folder: "Test" and Name Webform: "My First Form"
    Then I click on the Start Webform button
    And I should see "Form Completed" on the screen
    And I click on the cross button
    Then I should see "Signup Forms" on the screen

  @s4
  Scenario: As admin user, Delete the form that was created for the first time
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Edit button of the form name "My First Form"
    And I select "Delete" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds

  @s5
  Scenario: As admin user, Delete the folder that was created for the first time
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I found folder: "Test" on Grid list and I clicked the edit dropdown
    And I select "Delete Folder" from the dropdown
    Then I accept to proceed remaining scenario
    And I wait for 3 seconds



@sbe
Feature: SBE - Substract from Search Results

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - Search By Example access to conduct the test execution
    Given Wait until the Application is ready
    Given Go for Search By Example
    Given I wait for 2 seconds
    Given I select Subtract From Results

  @sbe_add_to_search @qp1
  Scenario: SBE - Subtract From Result - Company
    And Subtract From Results with - "#companyname" & Start with "Myworks"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "118"
    And I wait for 3 seconds

  @sbe_add_to_search @sa
  Scenario: SBE - Subtract From Result - Contact
    And Subtract From Results with - "#contact" & Start with "Sa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "118"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Salutation/Prefix
    And Subtract From Results with - "#salutation" & Start with "Sr"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "97"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Title
    And Subtract From Results with - "#title" & Start with "Ms"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "100"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Address
    And Subtract From Results with - "#address" & Start with "4"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "107"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Address2
    And Subtract From Results with - "#address2" & Start with "Oak Valley"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "118"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Phone
    And Subtract From Results with - "#phone" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "115"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Cell/Mobile
    And Subtract From Results with - "#cell" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "118"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Fax
    And Subtract From Results with - "#fax" & Start with "4-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "113"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Ext
    And Subtract From Results with - "#ext" & Start with "1-"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "119"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - State
    And Subtract From Results with - "#state" & Start with "Ohio"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "112"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - City
    And Subtract From Results with - "#city" & Start with "New York"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "117"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Zip
    And Subtract From Results with - "#zip" & Start with "6"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "114"
    And I wait for 3 seconds

  @sbe_new @qp @em
  Scenario: SBE - Subtract From Result - Email
    And Subtract From Results with Email & Start with "sakibqa"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "21"
    And I wait for 3 seconds

  @sbe_new @qp5 @es
  Scenario Outline: SBE - Subtract From Result - Email Status
    And Subtract From Results with Email Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status    | number | secs |
    | Active           | 20     | 3    |
    | No Email Address | 100    | 3    |

  @sbe_new @qp6
  Scenario Outline: SBE - Subtract From Result - Status
    And Subtract From Results with Status and selected "<select_status>" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "<number>"
    And I wait for <secs> seconds
  Examples:
    | select_status | number | secs |
    | Attempted     | 115    | 3    |
    | Hot Prospect  | 115    | 3    |

  @sbe_new @wip @qp
  Scenario: SBE - Subtract From Result - Team Owner
    And I quick search with "Abata" -- with enter function
    And Go for Search By Example
    And I select Subtract From Results
    And Subtract From Results of Team Owner and selected "Sakib Mahmud" from drop-down menu
    Then I click to Search Button -- Search By Example
    And I should see "No Records Found" on screen
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Profile Description
    And Subtract From Results with - "#ProfileDescription" & Start with "viva"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "118"
    And I wait for 3 seconds

  @sbe_new @bi
  Scenario: SBE - Subtract From Result - Birthday
    And Subtract From Results of Birthday & selected Date is "February 1"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "97"
    And I wait for 3 seconds

  @sbe_new @an
  Scenario: SBE - Subtract From Result - Anniversary
    And Subtract From Results of Anniversary & selected Date is "April 3"
    And I wait for 2 seconds
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "88"
    And I wait for 3 seconds

  @sbe_add_to_search @qp
  Scenario: SBE - Subtract From Result - Country
    And Subtract From Results with - "#country" & Start with "Australia"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "100"
    And I wait for 3 seconds

  @sbe_new @qp
  Scenario: SBE - Subtract From Result - Website
    And Subtract From Results with - "#website" & Start with "meetup.com"
    Then I click to Search Button -- Search By Example
    And I should see in Current Lookup : "1"
    And I wait for 3 seconds

  #TODO: BELOW TEST REQUIRES SCROLLING FUNCTIONS =>> PULL FROM m_sbe_new_search.feature rest scenario
  

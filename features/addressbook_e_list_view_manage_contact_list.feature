@addressbook
Feature: Addressbook => List View Test Execution
  Right Panel to select and List view to Omit Checked

  Background: Prerequisites
    Given I want to maximize the windows of browser
    Given As Admin User, Addressbook - List View access to conduct the test execution
    Given Wait until the Application is ready
    Given Close Support options

  #STORY BLOCK: SECONDARY ACTION LIST

  @create_activity @create
  Scenario Outline: Create Activity
    And I quick create activity with "<user_input_subject>" and type "<activity_type>" and searched for contact of "<contact_find>" & select "<select_contact>"
    And I wait for <secs> seconds
  Examples:
    | user_input_subject | activity_type | contact_find | select_contact                 | secs |
    | Meeting - Activity | Meeting       | Virg         | Virginia Rodriguez, Browseblab | 5    |
    | Meeting - Activity | Meeting       | Mild         | Mildred Hayes, Babbleset       | 5    |
    | Meeting - Activity | Meeting       | Ryan         | Ryan Lee, Babbleset            | 5    |
    | Meeting - Activity | Meeting       | Russ         | Russell West, Babbleset        | 5    |
    | Meeting - Activity | Meeting       | Shar         | Sharon Montgomery, Browsezoom  | 5    |
    | Call - Call        | Call          | Davi         | David Gilbert, Centimia        | 5    |
    | Call - Call        | Call          | Wayn         | Wayne Spencer, Centizu         | 5    |
    | Call - Call        | Call          | Lois         | Lois Medina, Dazzlesphere      | 5    |
    | Call - Call        | Call          | Paul         | Paul Martin, Demimbu           | 5    |
    | Call - Call        | Call          | Chr          | Christopher Mcdonald, Devcast  | 5    |
    | Task - Task        | Task          | Dor          | Doris Watson, Devpulse         | 5    |
    | Task - Task        | Task          | Dian         | Diana Bailey, Digitube         | 5    |
    | Task - Task        | Task          | Amy          | Amy Ray, Dynava                | 5    |
    | Task - Task        | Task          | Ann          | Anne Andrews, Eabox            | 5    |
    | Task - Task        | Task          | Will         | Willie Williams, Eabox         | 5    |

  @all_activity_today @activity
  Scenario: All Activity Today --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Activity Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_activity_today @activity
  Scenario: All Activity Today --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Activity Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @meeting_today @activity
  Scenario: Meetings Today --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Meetings Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @meeting_today @activity
  Scenario: Meetings Today --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Meetings Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @calls_today @activity
  Scenario: Calls Today --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Calls Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @calls_today @activity
  Scenario: Calls Today --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Calls Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @tasks_today @activity
  Scenario: Tasks Today --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tasks Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @tasks_today @activity
  Scenario: Tasks Today --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tasks Today from Activity Tab -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: PRIMARY ACTION LIST

  @all_contacts @primary
  Scenario: All Contacts --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_contacts @primary
  Scenario: All Contacts --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Contacts -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "117"
    And I wait for 5 seconds

  @starred @primary
  Scenario: All Contacts --> List View -> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Starred -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @starred @primary
  Scenario: All Contacts --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Starred -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: DEALS MENU

  @create_deals @deals @create
  Scenario Outline: Create Deals
    And I select All Deals Under Deals options -- while in Action Stream Module
    And I quick create deals Product "<product>" & Price "<price>" & Lead Source "<lead_source>" & Sales Stage "<sales_stage>" and searched for contact of "<contact_find>" & select "<select_contact>"
    And I wait for <secs> seconds
  Examples:
    | product   | price   | lead_source       | sales_stage    | contact_find | select_contact              | secs |
    | iPhone 1S | $399.99 | Email Marketing   | Prospect       | Nor          | Norma West, Fanoodle        | 5    |
    | iPhone 1S | $399.99 | Email Marketing   | Prospect       | Patr         | Patricia Cruz, Feedmix      | 5    |
    | iPhone 1S | $399.99 | Email Marketing   | Prospect       | Car          | Carol Ford, Flipbug         | 5    |
    | iPhone 1S | $399.99 | Email Marketing   | Prospect       | Timo         | Timothy Little, Flipstorm   | 5    |
    | iPhone 1S | $399.99 | Email Marketing   | Prospect       | Juli         | Julia Ray, Gigashots        | 5    |
    | iPhone 4  | $499.99 | Employee Referral | Needs Analysis | Stev         | Steve Henderson, Jabbertype | 5    |
    | iPhone 4  | $499.99 | Employee Referral | Needs Analysis | Ray          | Raymond Willis, Jaloo       | 5    |
    | iPhone 4  | $499.99 | Employee Referral | Needs Analysis | Ear          | Earl Rivera, Jamia          | 5    |
    | iPhone 4  | $499.99 | Employee Referral | Needs Analysis | Step         | Stephen Vasquez, Jaxbean    | 5    |
    | iPhone 4  | $499.99 | Employee Referral | Needs Analysis | Rebe         | Rebecca Chavez, Jaxnation   | 5    |
    | iPhone 7S | $599.99 | Mailing           | Proposal       | Rebe         | Rebecca Gutierrez, Jetwire  | 5    |
    | iPhone 7S | $599.99 | Mailing           | Proposal       | Chri         | Christopher Wells, Kanoodle | 5    |
    | iPhone 7S | $599.99 | Mailing           | Proposal       | Meli         | Melissa Bailey, Katz        | 5    |
    | iPhone 7S | $599.99 | Mailing           | Proposal       | Debo         | Deborah Sanchez, Kazu       | 5    |
    | iPhone 7S | $599.99 | Mailing           | Proposal       | Mar          | Martha Reed, Kwinu          | 5    |
    | iPhone 5  | $599.99 | Print Ad          | Negotiations   | Jaso         | Jason Rivera, Lajo          | 5    |
    | iPhone 5  | $599.99 | Print Ad          | Negotiations   | Tamm         | Tammy Alexander, Linkbridge | 5    |
    | iPhone 5  | $599.99 | Print Ad          | Negotiations   | Mari         | Marilyn Black, Livetube     | 5    |
    | iPhone 5  | $599.99 | Print Ad          | Negotiations   | Phy          | Phyllis Henderson, Midel    | 5    |
    | iPhone 5  | $599.99 | Print Ad          | Negotiations   | Emil         | Emily Bowman, Minyx         | 5    |
    | iPhone 6  | $699.99 | Tradeshow         | Order/Contract | Char         | Charles Hill, Muxo          | 5    |
    | iPhone 6  | $699.99 | Tradeshow         | Order/Contract | Chri         | Christine Bryant, Myworks   | 5    |
    | iPhone 6  | $699.99 | Tradeshow         | Order/Contract | Eug          | Eugene Cooper, Npath        | 5    |
    | iPhone 6  | $699.99 | Tradeshow         | Order/Contract | Amy          | Amy Payne, Ooba             | 5    |
    | iPhone 6  | $699.99 | Tradeshow         | Order/Contract | Mich         | Michelle Evans, Oyoba       | 5    |

  @all_deals @deals
  Scenario: All Deals --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Deals from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @all_deals @deals
  Scenario: All Deals --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select All Deals from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @prospect @deals
  Scenario: Prospect --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Prospect from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @prospect @deals
  Scenario: Prospect --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Prospect from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @needs_analysis @deals
  Scenario: Needs Analysis --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Need Analysis from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @needs_analysis @deals
  Scenario: Needs Analysis --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Need Analysis from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @proposal @deals
  Scenario: Proposal --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Proposal from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @proposal @deals
  Scenario: Proposal --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Proposal from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @negotiations @deals
  Scenario: Negotiations --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @negotiations @deals
  Scenario: Negotiations --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @order_contract @deals
  Scenario: Order/Contract --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @order_contract @deals
  Scenario: Order/Contract --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Negotiations from Deals -- while in Addressbook
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: STATUS MENU

  #Note: Preset of number of items should be updated on counter

  @status_new @status
  Scenario: New --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select New from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_new @status
  Scenario: New --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select New from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_attempted @status
  Scenario: Attempted --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Attempted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_attempted @status
  Scenario: Attempted --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Attempted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_contacted @status
  Scenario: Contacted --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Contacted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_contacted @status
  Scenario: Contacted --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Contacted from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_client @status
  Scenario: Client --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Client from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_client @status
  Scenario: Client --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Client from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_hot_prospect @status
  Scenario: Hot Prospect --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Hot Prospect from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_hot_prospect @status
  Scenario: Hot Prospect --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Hot Prospect from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  @status_disqualified @status
  Scenario: Disqualified --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Disqualified from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @status_disqualified @status
  Scenario: Disqualified --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Disqualified from Status - while in Addressbook module
    And I switch to Contact List View
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "2"
    And I wait for 5 seconds

  #STORY BLOCK: SUBSCRIPTION STATUS

  @three @subscriber
  Scenario: Active Subscribers --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Active Subscribers from Subscriptions Status
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @four @subscriber
  Scenario: Active Subscribers --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Active Subscribers from Subscriptions Status
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "97"
    And I wait for 5 seconds

  @five @subscriber
  Scenario: Inactive Subscribers --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Inactive Subscribers from Subscriptions Status
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @six @subscriber
  Scenario: Inactive Subscribers --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Inactive Subscribers from Subscriptions Status
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

  #note: Both Filter should be pre-created and number of contact should be enlisted.

  #STORY BLOCK: TAGS

  @seven @tagged
  Scenario: Selected Available Tags --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
  #   And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
  #   And To create Tags I fill in "MyTagsChecked" and save it
  #   And I wait for 5 seconds
    And In Tags Tab, I select "Tags Group" this tag
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @eight @tagged
  Scenario: Selected Available Tags --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I select Tags Under Tags Tab
  #   And I wait for 5 seconds
  #   And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
  #   And To create Tags I fill in "MyTagsOmitted" and save it
    And I wait for 5 seconds
    And In Tags Tab, I select "Tags Group" this tag
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

  #note: Both Filter should be pre-created and number of contact should be enlisted.

  #STORY BLOCK: FILTER

  @nine @filter
  Scenario: Selected Available Filter --> List View --> I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
  #   Given In Filter Tab I select "No Filters" and select "New Filter" -- while in Addressbook
  #   And To create Filter - I fill in "MyFilterChecked" and save as "MyFilterChecked" -- while in Addressbook
  #   And I wait for 5 seconds
    And In Filter Tab I select "Test Filter Group" this filter
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I select checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

  @ten @filter
  Scenario: Selected Available Filter --> List View --> I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
  #   Given In Filter Tab I select "No Filters" and select "New Filter" -- while in Addressbook
  #   And To create Filter - I fill in "MyFilterChecked" and save as "MyFilterChecked" -- while in Addressbook
  #   And I wait for 5 seconds
    And In Filter Tab I select "Test Filter Group" this filter
    And I wait for 5 seconds
    And I switch to Contact List View
    And I wait for 5 seconds
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "17"
    And I wait for 5 seconds

    #STORY BLOCK: TEAM MENU

  @team @me @wip
  Scenario: Team --> List View -- I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Me from Team menu -- while in Addressbook
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @team @me @wip
  Scenario: Team --> List View -- I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select Me from Team menu -- while in Addressbook
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds

    #Note: Number of 25 contacts should be available before this scenario execution

  @team @subuser @wip
  Scenario: Team --> List View -- I select checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select sub-user of "Mr. Sub User" from Team Menu -- while in Addressbook
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "22"
    And I wait for 5 seconds

  @team @subuser @wip
  Scenario: Team --> List View -- I omit checked
    And I wait for 30 seconds
    And I click on Contacts Tab
    And I switch to Contact List View
    And I select sub-user of "Mr. Sub User" from Team Menu -- while in Addressbook
    And I wait until ".content-header" is visible
    And I select three row from List View
    And I omit checked
    Then I should see in Current Lookup : "3"
    And I wait for 5 seconds


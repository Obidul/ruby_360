@signup
Feature: Signup forms validations

  Background: Maximize the browser and Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Signup form access to conduct the test execution
    Given Wait until the Application is ready
    And I wait for 10 seconds

    #STORY BLOCK: Signup form Navigations : Folder View

  Scenario: As admin user, write text to the block
    And I wait for 3 seconds
    Then I click on Email Tab
    And I wait for 5 seconds
    Then I clicked on "Signup Forms"
    And I wait for 5 seconds
    Then I click on the Red Cross button on the Signup Form page
    Then I should see "Create a Signup Form" on the screen
    Then I select the "Popup" category
    And I should see "Popup" on the screen
    Then I select the "1 Popup From" form template
    And I delete a block

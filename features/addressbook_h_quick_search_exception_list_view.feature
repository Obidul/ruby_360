@addressbook
Feature: Go for Quick search Test Execution - List View

#Note: (LV) = List View & (DV) = Details View

  Background: Prerequisites
     Given I want to maximize the windows of browser
#    Given As Admin User, Addressbook - Quick Search Activity access to conduct the test execution
     Given Local Quick Search
     Given Wait until the Application is ready
     Given I wait for 10 seconds

  @test6
  Scenario Outline: (LV) Quick Search with Combined Data - #1563
    And Go for Contact Module
    And I switch to Contact List View
    And I wait for <secs> seconds
    And I quick search with "<quick_search_field>" -- with enter function
    And I should see in Search Results : "<number>"
    And I wait for <secs> seconds
    Examples:
      | quick_search_field  | number | secs |
      | Eabox               | 2      | 5    |
      | An                  | 4      | 5    |
      | Sr                  | 10     | 5    |
      | Mrs                 | 8      | 5    |
      | sakibqa+22@gmail.com| 1      | 5    |
      | (404) 825-3925      | 1      | 5    |
      | 49321               | 1      | 5    |
      | (937) 942-8625      | 1      | 5    |
      | (309) 750-0031      | 1      | 5    |
      | On Hold             | 9      | 5    |
      | North Carbon Road   | 1      | 5    |
      | Corben              | 1      | 5    |
      | New York            | 5      | 5    |
      | Michigan            | 1      | 5    |
      | 20503-79950         | 1      | 5    |
@splash_webpage
Feature: Free trial - Account registration

  Background: Account registration to system
    Given I want to maximize the windows of browser
    Given Wait until the Application is ready

  @admintrial
  Scenario:New admin registration (Free trial)
    When I am on the "/" page
    Then I click on the FREE TRIAL button
    And I wait for 2 seconds
    Then I fill in "EmailAddress" with "robinhoodbs23@mailinator.com"
    Then I fill in "UserName" with "robinhoodbs23"
    Then I fill in "Password" with "123456"
    Then I click on the Create Account button
    And I wait for 5 seconds
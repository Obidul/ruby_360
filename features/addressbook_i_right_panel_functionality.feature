@addressbook
Feature: Dashboard - right panel

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - Right Panel Functionality access to conduct the test execution
    Given Wait until the Application is ready
    Given Close Support options
    And I wait for 10 seconds

  #STORY BLOCK: ACTIVITY TAB

  @activity
  Scenario: As Admin User, AB -> In Activity Tab -> All Activity Today -> Email Contacts
    And I click on Contacts Tab
    And I wait for 5 seconds
    Then I select All Activity Today from Activity Tab -- while in Addressbook
    And In Activity Tab I select "All Activity Today" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @meeting @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Meetings Today -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Then I select All Activity Today from Activity Tab -- while in Addressbook
    And In Activity Tab I select "Meetings Today" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @calls @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Calls Today -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Then I select All Activity Today from Activity Tab -- while in Addressbook
    And In Activity Tab I select "Calls Today" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @tasks @activity
  Scenario: As Admin User, Right Panel -> In Activity Tab -> Calls Today -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Then I select All Activity Today from Activity Tab -- while in Addressbook
    And In Activity Tab I select "Tasks Today" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  #STORY BLOCK: DEALS MENU

  @deals_all_deals @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> All Deals -> Email Contact
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "All Deals" and select "Email Contact" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_all_deals @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> All Deals -> Edit Deal Stage
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "All Deals" and select "Edit Deal Stages" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals_prospect @deals @bug
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Prospect -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Prospect" and select "Email Contact" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_prospect @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Prospect -> Edit Deal Stages
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Prospect" and select "Edit Deal Stages" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals_needs_analysis @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Needs Analysis -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Needs Analysis" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_needs_analysis @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Needs Analysis -> Edit Deal Stages
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Needs Analysis" and select "Edit Deal Stages" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals_proposal @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Proposal -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Proposal" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_proposal @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Proposal -> Edit Deal Stage
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Proposal" and select "Edit Deal Stages" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals_negotiations @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Negotiations -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Negotiations" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_negotiations @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Negotiations -> Edit Deal Stage
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Negotiations" and select "Edit Deal Stages" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  @deals_order_contracts @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Order/Contracts -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Order/Contract" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @deals_order_contracts @deals
  Scenario: As Admin User, Right Panel -> In Deals Tab -> Order/Contracts -> Edit Deal Stage
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Deals -- while in Addressbook
    And I wait for 5 seconds
    And In Deals Tab I select "Order/Contract" and select "Edit Deal Stage" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Deal Stages - Edit Deal Stages is visible

  #STORY BLOCK: TAGS TAB (BLOCK WISE SCENARIO)

  @tags
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Create Tags
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    And I select Tags Under Tags Tab
    And I wait for 5 seconds
    And In Tags Tab I select "No Tags" and select "New Tag" -- while in Addressbook
    Then To create Tags I fill in "My Tags" and save it
    And I wait for 5 seconds

  @tags
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Rename Tags
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    And I wait for 5 seconds
    Given I found existing "My Tags" tag and rename to "Hello Tag" -- while in Addressbook
    And I wait for 5 seconds

  @tags
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Email Contact
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    And I wait for 5 seconds
    Given In Tags Tab I select "Hello Tag" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @tags
  Scenario: As Admin User, Right Panel -> In Tags Tab -> Delete Tag
    And I wait for 5 seconds
    And I click on Contacts Tab
    And I wait for 5 seconds
    Given I select Tags Under Tags Tab
    And I wait for 5 seconds
    Given In Tags Tab I select "Hello Tag" and select "Delete Tag" -- while in Addressbook
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: FILTERS (BLOCK WISE SCENARIO)

  @filter
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Create Filter
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
    Given In Filter Tab I select "No Filters" and select "New Filter" -- while in Addressbook
    And To create Filter - I fill in "My Filter" and save as "My Filter" -- while in Addressbook
    And I wait for 5 seconds

  @filter
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Rename Tags
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 3 seconds
    Given I found existing "My Filter" filter and rename to "My New Filter" -- while in Addressbook
    And I wait for 5 seconds

  @filter
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Email Contact
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
    Given In Filter Tab I select "My New Filter" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @filter
  Scenario: As Admin User, Right Panel -> In Filter Tab -> Delete Filter
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Filter Under Filter Tab
    And I wait for 5 seconds
    Given In Tags Tab I select "My New Filter" and select "Delete Filter" -- while in Addressbook
    And I accept to proceed remaining scenario
    And I wait for 5 seconds

  #STORY BLOCK: STATUS TAB (BLOCK WISE SCENARIO)

  @status_new @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> New -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    Given In Status menu I select "New" and select "Email Contacts" -- while in Addressbook
    And I wait for 3 seconds
    And I wait until Email Contact - Email Compose Module is visible
    And I wait for 5 seconds

  @status_new @status2
  Scenario: As Admin User, Right Panel -> In Status Menu -> New -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    Given In Status menu I select "New" and select "Edit Status" -- while in Addressbook
    And I wait for 3 seconds
    And I wait until Edit Status -  Edit Status page is visible
    And I wait for 5 seconds

  @status_attempted @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Attempted Contact -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    Given In Status menu I select "Attempted" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_attempted @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Attempted Contact -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    Given In Status menu I select "Attempted" and select "Edit Status" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_contacted @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Contacted -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Contacted" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_contacted @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Contacted -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Contacted" and select "Edit Status" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_hot_prospect @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Hot Prospect -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Hot Prospect" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_hot_prospect @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Hot Prospect -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Hot Prospect" and select "Edit Status" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_client @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Client -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Client" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_client @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Client -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Client" and select "Edit Status" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  @status_disqualified @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Disqualified -> Email Contacts
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Disqualified" and select "Email Contacts" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Email Contact - Email Compose Module is visible

  @status_disqualified @status
  Scenario: As Admin User, Right Panel -> In Status Menu -> Disqualified -> Edit Status
    And I wait for 5 seconds
    And I click on Contacts Tab
    Given I select Status Tab - while in Addressbook module
    And I wait for 3 seconds
    And In Status menu I select "Disqualified" and select "Edit Status" -- while in Addressbook
    And I wait for 5 seconds
    And I wait until Edit Status -  Edit Status page is visible

  #STORY BLOCK: TEAM
   Scenario: Team related scenario

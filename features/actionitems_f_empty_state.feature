@action_items
Feature: Action Stream - New user - All Nevigations

  Background: Login to system
    Given I want to maximize the windows of browser
    Given As Admin User, Action Items - In empty state navigation access to conduct the test execution
    Given Wait until the Application is ready
    Given Close Support options
    And I wait for 10 seconds

  Scenario: Action Stream Navigation Move to - Today
    And I select Today from Primary Right Panel
    Then I should see "Today" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Starred
    And I select Starred from Primary Right Panel
    Then I should see "Starred" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Overdue
    And I select Overdue from Primary Right Panel
    Then I should see "Overdue" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Activity - All Activity
    And I select All Activity Under Activity Tab
    Then I should see "All Activity" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Activity - Meetings
    And I select Meeting Under Activity Tab
    Then I should see "Meetings" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Activity - Calls
    And I select Calls Under Activity Tab
    Then I should see "Calls" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Activity - Tasks
    And I select Tasks Under Activity Tab
    Then I should see "Tasks" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Activity - To Do
    And I select Todo Under Activity Tab
    Then I should see "To Do" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status
    And I select Status Tab - while in Action Stream module
    Then I should see "New" on screen
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - New
    And I select New from Status - while in Action Stream module
    Then I should see "New" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - Attempted
    And I select Attempted from Status - while in Action Stream module
    Then I should see "Attempted" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - Contacted
    And I select Contacted from Status - while in Action Stream module
    Then I should see "Contacted" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - Hot Prospect
    And I select Hot Prospect from Status - while in Action Stream module
    Then I should see "Hot Prospect" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - Client
    And I select Client from Status - while in Action Stream module
    Then I should see "Client" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Status - Disqualified
    And I select Disqualified from Status - while in Action Stream module
    Then I should see "Disqualified" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Tags
    And I select Tags Under Tags Tab
    Then I should see "No Tags" on screen
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Filter
    And I select Filter Under Filter Tab
    Then I should see "No Filters" on screen
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals
    And I select Deals from Primary Right Panel -- while in Action Stream
    Then I should see "All Deals" on screen
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - All Deals
    And I select All Deals Under Deals options -- while in Action Stream Module
    Then I should see "All Deals" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - Prospect
    And I select Prospect Under Deals options -- while in Action Stream Module
    Then I should see "Prospect" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - Needs Analysis
    And I select Needs Analysis Under Deals options -- while in Action Stream Module
    Then I should see "Needs Analysis" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - Proposal
    And I select Proposal Under Deals options -- while in Action Stream Module
    Then I should see "Proposal" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - Negotiations
    And I select Negotiations Under Deals options -- while in Action Stream Module
    Then I should see "Negotiations" on the left header title
    And I wait for 3 seconds

  Scenario: Action Stream Navigation Move to - Deals - Order/Contract
    And I select Order&Contact Under Deals options -- while in Action Stream Module
    Then I should see "Order/Contract" on the left header title
    And I wait for 3 seconds

  @t1
  Scenario: Action Stream Navigation Move to - Team
    And I select Team from Primary Right Panel -- while in Action Stream
    Then I should see "Me" on screen
    And I wait for 3 seconds

  @t2
  Scenario: Action Stream Navigation Move to - Team - Me
    And I select Me from Team menu -- while in Action Stream
    Then I should see Client Name on the left header title
    And I wait for 3 seconds

  # Add Activity
  @a1
  Scenario: Add an activity while there is no activity in Today
    And I wait for 5 seconds
    And I click on "Add Activity" link
    Then I create an activity with subject: "New activity Meeting - One (Today)"
    Then I searched for "Da" & select "Daniel Baker, Aba" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Meeting - One (Today)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Meeting - One (Today)" activity has enlisted
    And I wait for 3 seconds

  @a1
  Scenario: Add an activity while there is no activity in Starred
    And I wait for 5 seconds
    And I select Starred from Primary Right Panel
    And I click on "Add Activity" link
    Then I create an activity with subject: "New activity Call - Two (Starred)"
    Then I select activity type "Call"
    Then I searched for "Ga" & select "Gary Cook, Abata" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Call - Two (Starred)" and select Star button
    And I wait for 1 seconds
    Then I found activity of "New activity Call - Two (Starred)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Call - Two (Starred)" activity has enlisted
    And I wait for 3 seconds

  @a1
  Scenario: Add an activity while there is no activity in Overdue
    And I wait for 5 seconds
    And I select Overdue from Primary Right Panel
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Task - Three (Overdue)"
    And I select Previous Date - Yesterday
    Then I select activity type "Task"
    Then I searched for "Er" & select "Eric Harris, Abata" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Task - Three (Overdue)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I select Today from Primary Right Panel
    Then I switch to History to see Completed Items
    And I should see "New activity Task - Three (Overdue)" activity has enlisted
    And I wait for 3 seconds

  @a1 @all
  Scenario: Add an activity while there is no activity in All Activity
    And I wait for 5 seconds
    Then I select All Activity Under Activity Tab
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Meeting - One (All Activity)"
    And I select Previous Date - Yesterday
    Then I searched for "Ern" & select "Ernest Hall, Flipstorm" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Meeting - One (All Activity)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I select All Activity Under Activity Tab
    Then I switch to History to see Completed Items
    And I should see "New activity Meeting - One (All Activity)" activity has enlisted
    And I wait for 3 seconds

  @a1
  Scenario: Add an activity while there is no activity in Meetings
    And I wait for 5 seconds
    Then I select Meeting Under Activity Tab
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Meeting - Two (Meeting)"
    Then I searched for "Ja" & select "James Bennett, Gabcube" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Meeting - Two (Meeting)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Meeting - Two (Meeting)" activity has enlisted
    And I wait for 3 seconds

  @a1
  Scenario: Add an activity while there is no activity in Calls
    And I wait for 5 seconds
    Then I select Calls Under Activity Tab
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Call - Three (Calls)"
    Then I searched for "De" & select "Debra Shaw, Izio" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Call - Three (Calls)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Call - Three (Calls)" activity has enlisted
    And I wait for 3 seconds

  @a1
  Scenario: Add an activity while there is no activity in Tasks
    And I wait for 5 seconds
    Then I select Tasks Under Activity Tab
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Task - Four (Tasks)"
    Then I searched for "Chr" & select "Chris Harvey, Jamia" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Task - Four (Tasks)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Task - Four (Tasks)" activity has enlisted
    And I wait for 3 seconds

  @a1 @todo
  Scenario: Add an activity while there is no activity in To Do
    And I wait for 5 seconds
    Then I select Todo Under Activity Tab
    Then I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create activity of Todo with "New activity ToDo - Five (ToDo)"
    Then I click to Save button
    And I wait for 5 seconds
    Then I found To Do of "New activity ToDo - Five (ToDo)" and select Radio button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity ToDo - Five (ToDo)" Todo has enlisted
    And I wait for 3 seconds

  # Add Activity
  @t1
  Scenario: Add an activity while there is no activity in Status - New
    And I wait for 5 seconds
    Then I select New from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Meeting - One (New)"
    Then I searched for "La" & select "Larry Cooper, Kaymbo" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Meeting - One (New)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Meeting - One (New)" activity has enlisted
    And I wait for 3 seconds

  @t1
  Scenario: Add an activity while there is no activity in Status -  Attempted
    And I wait for 5 seconds
    Then I select Attempted from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Call - Two (Attempted)"
    Then I searched for "And" & select "Andrea Crawford, Kazio" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Call - Two (Attempted)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Call - Two (Attempted)" activity has enlisted
    And I wait for 3 seconds

  @t1
  Scenario: Add an activity while there is no activity in Status - Contacted
    And I wait for 5 seconds
    Then I select Contacted from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Task - Three (Contacted)"
    Then I searched for "Jud" & select "Judith Hanson, Kazio" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Task - Three (Contacted)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Task - Three (Contacted)" activity has enlisted
    And I wait for 3 seconds

  @t1
  Scenario: Add an activity while there is no activity in Status - Hot Prospect
    And I wait for 5 seconds
    Then I select Hot Prospect from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Meeting - Four (Hot Prospect)"
    Then I searched for "Fr" & select "Fred Coleman, Motherboard" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Meeting - Four (Hot Prospect)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Meeting - Four (Hot Prospect)" activity has enlisted
    And I wait for 3 seconds

  @t1
  Scenario: Add an activity while there is no activity in Status - Client
    And I wait for 5 seconds
    Then I select Client from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Call - Five (Client)"
    Then I searched for "Mil" & select "Mildred Perez, Kwideo" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Call - Five (Client)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Call - Five (Client)" activity has enlisted
    And I wait for 3 seconds

  @t1
  Scenario: Add an activity while there is no activity in Status - Disqualified
    And I wait for 5 seconds
    Then I select Contacted from Status - while in Action Stream module
    And I click on "Add Activity" link
    And I wait for 5 seconds
    Then I create an activity with subject: "New activity Task - Six (Disqualified)"
    Then I searched for "Pam" & select "Pamela Phillips, Leexo" from the list
    Then I click to Save button
    And I wait for 2 seconds
    Then I found activity of "New activity Task - Six (Disqualified)" and select Radio button
    Then I click to Cancel button
    And I wait for 2 seconds
    Then I switch to History to see Completed Items
    And I should see "New activity Task - Six (Disqualified)" activity has enlisted
    And I wait for 3 seconds


  # Add Deal
  @d1
  Scenario: Add a Deal while there is no Deal in All Deals
    And I wait for 5 seconds
    Then I select All Deals Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Processor" & Price "$100.00" & Lead Source "Website" & Sales Stage "Prospect"
    And I searched for "Jen" & select "Jennifer Hunter, Gigazoom" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$100.00" Product & Company: "Processor - Gigazoom" and Sales Stage: "Prospect" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @d1 @c2
  Scenario: Add a Deal while there is no Deal in Prospect
    And I wait for 5 seconds
    Then I select Prospect Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Apple" & Price "$200.00" & Lead Source "Employee Referral" & Sales Stage "Prospect"
    And I searched for "Cat" & select "Catherine Gilbert, Motherboard" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$200.00" Product & Company: "Apple - Motherboard" and Sales Stage: "Prospect" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @d1
  Scenario: Add a Deal while there is no Deal in Needs Analysis
    And I wait for 5 seconds
    Then I select Needs Analysis Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Gold" & Price "$300.00" & Lead Source "Mailing" & Sales Stage "Needs Analysis"
    And I searched for "Ke" & select "Kenneth Tucker, Motherboard" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$300.00" Product & Company: "Gold - Motherboard" and Sales Stage: "Needs Analysis" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @d1
  Scenario: Add a Deal while there is no Deal in Proposal
    And I wait for 5 seconds
    Then I select Proposal Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Diamond" & Price "$400.00" & Lead Source "Print Ad" & Sales Stage "Proposal"
    And I searched for "Hen" & select "Henry Bailey, Mynte" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$400.00" Product & Company: "Diamond - Mynte" and Sales Stage: "Proposal" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @d1
  Scenario: Add a Deal while there is no Deal in Negotiations
    And I wait for 5 seconds
    Then I select Negotiations Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Motherboard" & Price "$500.00" & Lead Source "Tradeshow" & Sales Stage "Negotiations"
    And I searched for "Ho" & select "Howard Rivera, Photofeed" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$500.00" Product & Company: "Motherboard - Photofeed" and Sales Stage: "Negotiations" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds

  @d1
  Scenario: Add a Deal while there is no Deal in Order/Contract
    And I wait for 5 seconds
    Then I select Order&Contact Under Deals options -- while in Action Stream Module
    And I Create Quick Deal By selecting Product "Processor" & Price "$600.00" & Lead Source "Website" & Sales Stage "Order/Contract"
    And I searched for "Eli" & select "Elizabeth Stone, Photolist" from the list
    And I click to Save button
    And I wait for 5 seconds
    And I found existing Deals Price: "$600.00" Product & Company: "Processor - Photolist" and Sales Stage: "Order/Contract" and select "Delete" from drop-down menu -- In Action Stream
    And I accept to proceed remaining scenario
    And I wait for 2 seconds